﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;

namespace uTools
{
    public partial class uKnowledgeView : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            /*if (User.Identity.IsAuthenticated)
            {
                bool test = User.IsInRole("user");                
 
            }
            else
            {
                Response.Redirect("~/Login.aspx?urlredirect=uKnowledgeView.aspx");
            }*/

        }

        protected void OnTreeNodePopulate(object sender, TreeNodeEventArgs e)
        {
            if (e.Node.ChildNodes.Count == 0)
            {
                /*switch (e.Node.Depth)
                {
                    case 0:
                        PopulateCategories(e.Node);
                        break;
                    case 1:
                        PopulateCategories(e.Node);
                        break;
                }*/
                PopulateCategories(e.Node);

            }
            //e.Node.Target = "iframe_link";

        }

        void PopulateCategories(TreeNode node)
        {
            string strsql;
            SqlCommand sqlQuery = new SqlCommand();
            DataSet resultSet = new DataSet();

            if (node.Value.Trim() == "-1")
            {
                strsql = "Select id, Descricao, NULL as URL From Classificacao where ParentId is null";
            }
            else
            {
                strsql = "SELECT Categoria.id as ID, Categoria.Descricao as Descricao, NULL as URL FROM Categoria WHERE Categoria.Parent_Cat_ID = " + node.Value.Trim() +
                         " UNION " +
                         " SELECT Conteudo.Id, Conteudo.Descricao, Conteudo.URL FROM Conteudo WHERE Conteudo.Categoria_Id = " + node.Value.Trim();
            }

            sqlQuery.CommandText = strsql;
            resultSet = RunQuery(sqlQuery);

            if (resultSet.Tables.Count > 0)
            {
                foreach (DataRow row in resultSet.Tables[0].Rows)
                {

                    TreeNode NewNode = new TreeNode();

                    if (row["URL"].ToString() == "")
                    {
                        NewNode.Text = row["Descricao"].ToString();
                        NewNode.Value = row["ID"].ToString();
                        NewNode.SelectAction = TreeNodeSelectAction.Expand;
                        NewNode.PopulateOnDemand = true;
                        NewNode.Expanded = false;
                        //NewNode.Target = "iframe_link";

                    }
                    else
                    {
                        NewNode.Text = row["Descricao"].ToString();
                        NewNode.Value = "-1";
                        NewNode.NavigateUrl = row["URL"].ToString();
                        NewNode.SelectAction = TreeNodeSelectAction.Select;
                        NewNode.PopulateOnDemand = false;
                        //NewNode.;

                    }
                    node.ChildNodes.Add(NewNode);
                    //node.Target = "iframe_link";
                }
            }

        }

        private DataSet RunQuery(SqlCommand sqlQuery)
        {
            string connectionString = ConfigurationManager.ConnectionStrings
                ["uToolsDB"].ConnectionString;
            SqlConnection DBConnection = new SqlConnection(connectionString);
            SqlDataAdapter dbAdapter = new SqlDataAdapter();
            dbAdapter.SelectCommand = sqlQuery;
            sqlQuery.Connection = DBConnection;
            DataSet resultsDataSet = new DataSet();
            try
            {
                dbAdapter.Fill(resultsDataSet);
            }
            catch
            {
                //System.Windows.Forms.MessageBox.Show("Unable to connect to SQL Server.");
            }
            return resultsDataSet;
        }
    }
}