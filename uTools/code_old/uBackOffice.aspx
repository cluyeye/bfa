﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="uBackOffice.aspx.cs" Inherits="uTools.uBackOffice" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <table fullscreen>
            <tr height="100%">
                <td style="width:75%">
                    <iframe src="uKnowledgeView.aspx" width="100%" height="100%" frameborder="1" autoing="no"></iframe>
                </td>
                <td style="width:25%">
                    <iframe src="uCRMView.aspx" width="100%" height="100%" frameborder="1" autoing="no"></iframe>
                </td>
            </tr>            
        </table>
    </form>
</body>
</html>

