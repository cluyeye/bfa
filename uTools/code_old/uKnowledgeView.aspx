﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="uKnowledgeView.aspx.cs" Inherits="uTools.uKnowledgeView" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
</head>
<body>
    <form id="form1" runat="server">
        
            <span class = "tl_page" style = "font-size: 18px">Conteúdos</span>

            <asp:TreeView ID = "TreeView" runat = "server" Target = "iframe_link" OnTreeNodePopulate = "OnTreeNodePopulate" ImageSet = "WindowsHelp">
                <HoverNodeStyle Font-Underline = "True" ForeColor = "#6666AA" />
                <Nodes>
                    <asp:TreeNode PopulateOnDemand= "True" Text = "Menu" Value = "-1"></asp:TreeNode>
                </Nodes>
                <NodeStyle Font-Names = "Tahoma" Font-Size = "8pt" ForeColor = "Black" HorizontalPadding= "5px" NodeSpacing= "0px" VerticalPadding= "1px" />
                <ParentNodeStyle Font-Bold= "False" />
                <SelectedNodeStyle Font-Underline = "False" HorizontalPadding= "0px" VerticalPadding= "0px" BackColor = "#B5B5B5" />
            </asp:TreeView>

            <iframe src= "http://www.ucall.co.ao" name = "iframe_link">
    </form>
</body>
</html>
