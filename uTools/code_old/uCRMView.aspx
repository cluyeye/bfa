﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="uCRMView.aspx.cs" Inherits="uTools.uCRMView" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script>
        function OnLoad_Page() {
            container_dadoscliente.disabled = true;
            container_button_save.style.visibility = 'hidden';
        }

        function OnClick_Editar() {
            container_button_edit.style.visibility = 'hidden';
            container_button_save.style.visibility = 'visible';
            container_dadoscliente.disabled = false;
            document.getElementById("btn_action").value = "Gravar";
        }

        function OnClick_Search() {
            container_button_edit.style.visibility = 'hidden';
            container_button_save.style.visibility = 'visible';
            container_dadoscliente.disabled = false;
            document.getElementById("btn_action").value = "Pesquisar";
            document.getElementById("txtbox_nome").value = "";
            document.getElementById("txtbox_email").value = "";
            document.getElementById("txtbox_telefone1").value = "";
            document.getElementById("txtbox_telefone2").value = "";
            document.getElementById("txtbox_numero_conta").value = "";
        }
    </script>
</head>
<body onload="OnLoad_Page()">
    <form id="uCrm" runat="server">
        <div id="container_dadoscliente">
            <asp:HiddenField ID="hidden_cliente_id" runat="server" />
            <table>
                <tr>
                    <td>Tipo de Cliente:               
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:DropDownList ID="dropbox_tipo_cliente" runat="server"></asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>Nome:               
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="txtbox_nome" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Email:
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="txtbox_email" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Telefone 1:
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="txtbox_telefone1" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Telefone 2:
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="txtbox_telefone2" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Numero de Conta :
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="txtbox_numero_conta" runat="server"></asp:TextBox>
                    </td>
                </tr>

                <tr>
                    <td>Agência:
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:DropDownList ID="dropbox_agencia" runat="server"></asp:DropDownList>
                    </td>
                </tr>
            </table>
        </div>
        <div id="container_message">
            <asp:Label ID="message_status" runat="server"></asp:Label>
        </div>
        <div id="container_button_save">
            <asp:Button ID="btn_action" runat="server" Text="Guardar" OnClick="btn_action_Click" />
            <asp:Button ID="btn_cancel" runat="server" Text="Cancelar" OnClick="btn_cancel_Click" />
        </div>
        <div id="container_button_edit">
            <button id="btn_edit" onclick="OnClick_Editar();return false;">Editar</button>
            <button id="btn_search" onclick="OnClick_Search();return false;">Pesquisar</button>
        </div>
        <div id="container_interaccoes">
            <asp:GridView ID="gridview_interaccoes" runat="server" Width="400px" AllowSorting="True" AutoGenerateSelectButton="True" OnRowCommand="gridview_interaccoes_RowCommand" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black" GridLines="Horizontal">
                


                <AlternatingRowStyle BackColor="#CCCCCC" />
                <FooterStyle BackColor="#CCCCCC" />
                <HeaderStyle HorizontalAlign="Left" BackColor="Black" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#808080" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                <SortedAscendingHeaderStyle BackColor="#808080" />
                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                <SortedDescendingHeaderStyle BackColor="#383838" />
            </asp:GridView>
        </div>
    </form>
</body>

</html>
