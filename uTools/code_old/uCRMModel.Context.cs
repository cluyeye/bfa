﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace uTools.code_old
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class uCRMEntities : DbContext
    {
        public uCRMEntities()
            : base("name=uCRMEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Interacao> Interacaos { get; set; }
        public virtual DbSet<Tipo_Cliente> Tipo_Cliente { get; set; }
        public virtual DbSet<Tipo_Interacao> Tipo_Interacao { get; set; }
        public virtual DbSet<Dados_Cliente> Dados_Cliente { get; set; }
        public virtual DbSet<Agencia> Agencias { get; set; }
    
        public virtual int novoCliente(string nome, string email, string telefone1, string telefone2, string conta, Nullable<int> agenciaID, Nullable<int> tipoID)
        {
            var nomeParameter = nome != null ?
                new ObjectParameter("Nome", nome) :
                new ObjectParameter("Nome", typeof(string));
    
            var emailParameter = email != null ?
                new ObjectParameter("Email", email) :
                new ObjectParameter("Email", typeof(string));
    
            var telefone1Parameter = telefone1 != null ?
                new ObjectParameter("Telefone1", telefone1) :
                new ObjectParameter("Telefone1", typeof(string));
    
            var telefone2Parameter = telefone2 != null ?
                new ObjectParameter("Telefone2", telefone2) :
                new ObjectParameter("Telefone2", typeof(string));
    
            var contaParameter = conta != null ?
                new ObjectParameter("Conta", conta) :
                new ObjectParameter("Conta", typeof(string));
    
            var agenciaIDParameter = agenciaID.HasValue ?
                new ObjectParameter("AgenciaID", agenciaID) :
                new ObjectParameter("AgenciaID", typeof(int));
    
            var tipoIDParameter = tipoID.HasValue ?
                new ObjectParameter("TipoID", tipoID) :
                new ObjectParameter("TipoID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("novoCliente", nomeParameter, emailParameter, telefone1Parameter, telefone2Parameter, contaParameter, agenciaIDParameter, tipoIDParameter);
        }
    
        public virtual int RemoverDadosCliente(Nullable<int> id)
        {
            var idParameter = id.HasValue ?
                new ObjectParameter("id", id) :
                new ObjectParameter("id", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("RemoverDadosCliente", idParameter);
        }
    }
}
