﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="uTools.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br">
<head runat="server">

    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />

    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta http-equiv="content-language" content="pt-br" />

    <meta name="description" content="Ferramenta de Ticketing, que vai melhorar a
        Experiência de Cliente dando resposta às situações colocadas nos vários canais
        de interacção da sua organização (agências, pontos de venda, contact center, etc).
        A partir de qualquer área da sua empresa, a resposta é dada dentro do prazo expectável,
        aumentando a Qualidade na relação com o Cliente. Os pedidos de informação, as reclamações
        ou sugestões são direccionadas a quem decide e resolvem-se com maior rapidez, eficiência
        e controlo da interacção" />

    <meta name="locale" content="AO" />
    <meta name="language" content="pt" />
    <meta name="country" content="Angola" />

    <link rel="shortcut icon" href="images/image_icon.png" />

    <title>uTools | Autenticação</title>

    <link rel="stylesheet" type="text/css" href="css/style_login.css" />
    <link rel="stylesheet" type="text/css" href="css/geral.css" />
    <link type="text/css" rel="stylesheet" href="css/style_modal.css" />

    <link rel="stylesheet" type="text/css" href="css/style_upd_progress.css">

    <script type="text/javascript">

        var clicked = false;
        function CheckBrowser() {
            if (clicked == false) {
                //Browser closed   
            } else {
                //redirected
                clicked = false;
            }
        }
        function bodyUnload() {
            if (clicked == false)//browser is closed  
            {
                var request = GetRequest();
                request.open("POST", "../LogOut.aspx", false);
                request.send();
            }
        }

        function GetRequest() {
            var xmlhttp;
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            return xmlhttp;
        }

    </script>

</head>
<body>
    <form id="form1" runat="server">

        <asp:PlaceHolder runat="server" ID="LoginForm" Visible="false">

            <asp:ScriptManager ID="manager" runat="server">
            </asp:ScriptManager>

            <div id="container_login" align="center">

                <div id="user_login_components">
                    <%--<img id="img_login" src="images/Asset 37.png" />--%>

                    <div id="content_user_login" align="justify">

                        <label for="UserName" class="lb_login">Utilizador*</label><br />
                        <asp:TextBox CssClass="txtInput" placeholder="UserName" ID="UserName" runat="server"></asp:TextBox>



                        <br />
                        <br />

                        <label for="Password" class="lb_login">Palavra-Passe*</label><br />
                        <asp:TextBox CssClass="txtInput" type="password" placeholder="Password" ID="Password" runat="server"></asp:TextBox>

                        <br />
                        <br />
                        <br />

                        <div class="botoes_div" align="justify">
                            <asp:ImageButton ID="bt_Login" runat="server" ImageUrl="~/images/entrar.png" OnClick="SignIn" title="Entrar" CssClass="img_bt_rect" />

                            <%--                            <asp:UpdatePanel ID="upd_modal" runat="server">
                                <ContentTemplate>

                                    <asp:ScriptManager ID = "scp_modal" runat="server">

                                    </asp:ScriptManager>
                                    <!-- The Modal -->
                                    <div id="myModal" class="modal">

                                        <!-- Modal content -->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h2 id="modal_text_header" runat="server">Modal Header</h2>
                                            </div>
                                            <div class="modal-body">
                                                <span id="modal_text_1" runat="server"></span>
                                                <br />
                                                <span id="modal_text_2" runat="server"></span>
                                                <br />
                                                <span id="modal_text_3" runat="server"></span>
                                                <br />
                                                <span id="modal_text_4" runat="server"></span>
                                            </div>
                                            <div class="modal-footer" runat="server">
                                                <span class="close">
                                                    <img id="modal_img_close" src="images/ok.png" /></span>
                                                <h3 id="modal_text_footer" runat="server">Modal Footer</h3>
                                            </div>
                                        </div>

                                    </div>

                                    <script>
                                        // Get the modal
                                        var modal = document.getElementById('myModal');

                                        // Get the button that opens the modal
                                        var btn = document.getElementById("bt_Login");

                                        // Get the <span> element that closes the modal
                                        var span = document.getElementsByClassName("close")[0];

                                        // When the user clicks the button, open the modal 
                                        btn.onclick = function () {
                                            modal.style.display = "block";
                                        }

                                        // When the user clicks on <span> (x), close the modal
                                        span.onclick = function () {
                                            modal.style.display = "none";
                                        }

                                        // When the user clicks anywhere outside of the modal, close it
                                        window.onclick = function (event) {
                                            //if (event.target == modal) {
                                            //    modal.style.display = "none";
                                            //}
                                        }
                                    </script>

                                </ContentTemplate>
                            </asp:UpdatePanel>--%>
                        </div>

                        <asp:PlaceHolder runat="server" ID="LoginStatus" Visible="false">
                            <p>
                                <asp:Literal runat="server" ID="StatusText" />
                            </p>
                        </asp:PlaceHolder>

                    </div>

                </div>


            </div>




        </asp:PlaceHolder>

    </form>


    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <div id="blur" />
            <div id="progress">
                <img src="images/loading-small.gif" width="50px" height="50px" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>


</body>
</html>
