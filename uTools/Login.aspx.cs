﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;

namespace uTools
{
    public partial class Login : System.Web.UI.Page
    {
        string id;
        string name_user;

        protected void Page_Load(object sender, EventArgs e)
        {
            Session.Abandon();

            if (!IsPostBack)
            {
                if (User.Identity.IsAuthenticated)
                {
                    string url = String.Empty;
                    url=Request.QueryString.Get("urlredirect");
                    if (url != "") Response.Redirect(url);
                    bool test = User.IsInRole("user");  
                    LoginForm.Visible = true;
                }
                else
                {
                    LoginForm.Visible = true;
                }
            }
        }


        public string Username_Agent
        {
            get
            {
                return UserName.Text;
            }
        }


        public string Password_Agent
        {
            get
            {
                return Password.Text;
            }
        }

        
        protected void SignIn(object sender, EventArgs e)
        {

            System.Threading.Thread.Sleep(1000);
            
            var userStore = new UserStore<IdentityUser>();
            var userManager = new UserManager<IdentityUser>(userStore);
            var user = userManager.Find(UserName.Text, Password.Text);
            
            if (user != null)
            {
                //if (Tools.session_status(UserName.Text) == false)
                //{
                    var authenticationManager = HttpContext.Current.GetOwinContext().Authentication;
                    var userIdentity = userManager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);
                    authenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = false }, userIdentity);

                string id = Context.User.Identity.GetUserId();
                string name_user = Tools.Load_AspNetUser(id);
                       
                //Load_user_info(id, name_user);

                    name_user = Tools.Load_AspNetUser(id);

                    //Tools.change_session_value_IN(UserName.Text, 1);

                    registar_Log();

                    string url = Request.QueryString.Get("urlredirect");

                    //modal_text_header.InnerText = "Informação | Autenticação";
                    //modal_text_1.InnerText = "Dados correctos.";
                    //modal_text_2.InnerText = "Acesso permitido";
                    //modal_text_footer.InnerText = "Login";

                    Response.Redirect("~/Login.aspx?urlredirect=" + url);
                //}
                //else
                //{
                //    StatusText.Text = "Já tem uma sessão iniciada, contacte o administrador.";
                //    LoginStatus.Visible = true;
                //    //Logout();
                //}


            }
            else
            {

                //modal_text_header.InnerText = "Informação | Autenticação";
                //modal_text_1.InnerText = "Dados incorrectos.";
                //modal_text_2.InnerText = "Acesso não permitido";
                //modal_text_footer.InnerText = "Login"; 
                StatusText.Text = "Nome de Usário ou palavra-passe inválida.";
                LoginStatus.Visible = true;
            }
        }

        //private void Load_user_info(string id, string name_user)
        //{
        //    id = Context.User.Identity.GetUserId();
        //    name_user = Tools.Load_AspNetUser(id);
        //}

        //private void Logout()
        //{
        //    //Load_user_info(id, name_user);
        //    Tools.change_session_value(Context.User.Identity.GetUserId(), 1);
        //    var authenticationManager = HttpContext.Current.GetOwinContext().Authentication;

        //    authenticationManager.SignOut();

        //}

        private void registar_Log()
        {         

            String[] log = new String[15];

            log[0] = UserName.Text;
            log[1] = "" + DateTime.Now.ToString();
            log[2] = "uAuth.dbo.AspNetUsers";
            log[3] = "" + 5;
            log[4] = "" + 1;
            //log[5] = UserName.Text;

            Tools.registar_Log(log);
        }

    }
}