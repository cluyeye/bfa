﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="utools_tasks.aspx.cs" Inherits="uTools.utools_tasks" %>

<%@ Register Src="~/Controls/uTasksView_script.ascx" TagName="uTasksView2" TagPrefix="utv2" %>
<%@ Register Src="~/Controls/uKnowledgeView.ascx" TagName="uKnowledgeView2" TagPrefix="ukv2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" >
<head runat="server">

    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>

    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta http-equiv="content-language" content="pt-br" />

    <meta name="description" content="Ferramenta de Ticketing, que vai melhorar a
        Experiência de Cliente dando resposta às situações colocadas nos vários canais
        de interacção da sua organização (agências, pontos de venda, contact center, etc).
        A partir de qualquer área da sua empresa, a resposta é dada dentro do prazo expectável,
        aumentando a Qualidade na relação com o Cliente. Os pedidos de informação, as reclamações
        ou sugestões são direccionadas a quem decide e resolvem-se com maior rapidez, eficiência
        e controlo da interacção" />

    <meta name="locale" content="AO"/>
    <meta name="language" content="pt"/>
    <meta name="country" content="Angola"/>

    <link rel="shortcut icon" href="images/image_icon.png" />


    <title>UTools Tasks</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="stylesheet" type="text/css" href="css/tabbed-panels.css" />
    <link rel="stylesheet" type="text/css" href="css/geral.css" />

    <link rel="stylesheet" type="text/css" href="css/style_email.css" />
    <link rel="stylesheet" type="text/css" href="css/style_kb.css" />
    <link rel="stylesheet" type="text/css" href="css/style_utask.css" />

    <link rel="stylesheet" type="text/css" href="css/style_upd_progress.css">


</head>

<body>
    <form id="form1" runat="server">

        <div class="container">
<%--            <div id="container_manage" align="justify">

                <span id="agent_info" class="i_manage" runat="server">
                    <asp:Label runat="server" style="background-color: #58595b;" ID="lbl_info_agent"></asp:Label>
                </span>
               
                <span id="job_role_info" class="i_manage" style="background-color: #58595b;" runat="server">
                    <asp:Label runat="server" ID="lbl_job_role_agent"></asp:Label>
                </span>

            </div>--%>


            <div class="tabbed">
                <input name="tabbed" id="tabbed1" type="radio" checked>

                <section>
                    <h1>
                        <label for="tabbed1" class="txt_Titulo">uKNOWLEDGEBASE</label>
                    </h1>
                    <div>
                <ukv2:uKnowledgeView2 ID="uKnowledgeView2" runat="server"></ukv2:uKnowledgeView2>
       
                    </div>
                </section>

                <input name="tabbed" id="tabbed2" type="radio">
                
                <section>
                    <h1>
                        <label for="tabbed2" class="txt_Titulo">uTASKS</label>
                    </h1>
                    <div style="background-color:  #e6e7e8;">
                <utv2:uTasksView2 ID="uTasksView2" runat="server"></utv2:uTasksView2>
                               
                    </div>
                </section>
                
            </div>

        </div>
    
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <div id="blur" />
            <div id="progress">
                <img src="images/loading-small.gif" width="50px" height="50px" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    </form>
</body>
</html>
