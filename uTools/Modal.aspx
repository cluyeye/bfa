﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Modal.aspx.cs" Inherits="uTools.Modal" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <link type="text/css" rel="stylesheet" href="css/geral.css" />
    <link type="text/css" rel="stylesheet" href="css/style_modal.css" />

</head>
<body>


    <h2>Bottom Modal</h2>

    <!-- Trigger/Open The Modal -->
    <button id="myBtn" runat="server">Open Modal</button>

    <!-- The Modal -->
    <div id="myModal" class="modal">

        <!-- Modal content -->
        <div class="modal-content">
            <div class="modal-header">
                <h2 id="modal_text_header">Modal Header</h2>
            </div>
            <div class="modal-body">
                <span id="modal_text_1" runat="server">Teste utools</span><br />
                <span id="modal_text_2" runat="server">Teste utools</span><br />
                <span id="modal_text_3" runat="server">Teste utools</span><br />
                <span id="modal_text_4" runat="server">Teste utools</span>
            </div>
            <div class="modal-footer">
                <span class="close"><img id="modal_img_close" src="images/ok.png" /></span>
                <h3 id="modal_text_footer">Modal Footer</h3>
            </div>
        </div>

    </div>

    <script>
        // Get the modal
        var modal = document.getElementById('myModal');

        // Get the button that opens the modal
        var btn = document.getElementById("myBtn");

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];

        // When the user clicks the button, open the modal 
        btn.onclick = function () {
            modal.style.display = "block";
        }

        // When the user clicks on <span> (x), close the modal
        span.onclick = function () {
            modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function (event) {
            //if (event.target == modal) {
            //    modal.style.display = "none";
            //}
        }
    </script>
    

</body>
</html>
