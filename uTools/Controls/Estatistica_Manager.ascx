﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Estatistica_Manager.ascx.cs" Inherits="uTools.Controls.Estatistica_Manager" %>




<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br">
<head runat="server">

    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />

    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta http-equiv="content-language" content="pt-br" />

    <meta name="description" content="Ferramenta de Ticketing, que vai melhorar a
        Experiência de Cliente dando resposta às situações colocadas nos vários canais
        de interacção da sua organização (agências, pontos de venda, contact center, etc).
        A partir de qualquer área da sua empresa, a resposta é dada dentro do prazo expectável,
        aumentando a Qualidade na relação com o Cliente. Os pedidos de informação, as reclamações
        ou sugestões são direccionadas a quem decide e resolvem-se com maior rapidez, eficiência
        e controlo da interacção" />

    <meta name="locale" content="AO" />
    <meta name="language" content="pt" />
    <meta name="country" content="Angola" />

    <link rel="shortcut icon" href="../images/icone_utools.png" />

    <title>uTools</title>

    <link rel="stylesheet" type="text/css" href="../css/style_userchangepwd.css" />
    <link rel="stylesheet" type="text/css" href="../css/style_utask.css" />
    <link rel="stylesheet" type="text/css" href="../css/geral.css" />

    <style>
        #content_user_register {
            margin-top: 10px;
            /*position: relative;*/
        }


            #content_user_register .txtInput {
                border: 1px solid #e6e7e8;
                font-family: 'ephismere';
                border: 1px solid #e6e7e8;
                width: 310px;
                border-radius: 4px;
                height: 25px;
            }

        #corpo_tasks #content_tb_parent {
            float: left;
            width: 85%;
            /*border: 3px solid red;*/
        }

        #corpo_tasks #content_detail_parent {
            /*width: 29%;*/
            /*border: 3px solid orange;*/
            border-radius: 6px;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server" enctype="multipart/form-data">

        <asp:ScriptManager ID="ScriptManagerTarefas" runat="server">
        </asp:ScriptManager>


        <div id="corpo_tasks">

            <asp:UpdatePanel ID="updpanel_lista" runat="server">
                <ContentTemplate>

                    <div id="content_parent_view">

                        <div id="content_tb_parent">
                            <table>
                                <tr>
                                    <td>
                                        <asp:TextBox CssClass="txtInput" ID="txt_username" runat="server" placeholder="Login"></asp:TextBox>
                                    </td>

                                    <td>
                                        <asp:TextBox CssClass="txtInput" ID="data_find_parent_1" runat="server" placeholder="Data Filtro Inicio"></asp:TextBox>
                                        <ajaxToolkit:CalendarExtender ID="cal_data_find_parent_1" runat="server" Format="yyyy-MM-dd" TargetControlID="data_find_parent_1" />
                                    </td>

                                    <td>
                                        <div id="container_task_count" align="center" style="width: 90px;">

                                            <asp:Label runat="server" ID="literal_count"></asp:Label>

                                        </div>
                                    </td>


                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox CssClass="txtInput" ID="txt_name_user" runat="server" placeholder="Nome"></asp:TextBox>
                                    </td>

                                    <td>
                                        <asp:TextBox CssClass="txtInput" ID="data_find_parent_2" runat="server" placeholder="Data Filtro Fim"></asp:TextBox>
                                        <ajaxToolkit:CalendarExtender ID="cal_data_find_parent_2" runat="server" Format="yyyy-MM-dd" TargetControlID="data_find_parent_2" />
                                    </td>
                                    <td>
                                        <div style="padding-left: 60px;">
                                            <asp:ImageButton ID="btn_filtrar" runat="server" ImageUrl="~/images/search_01.png" CssClass="img_bt" title="Filtrar dados" OnClick="btn_filtrar_Click" />
                                            <asp:ImageButton ID="btn_limpar_filtro" runat="server" ImageUrl="~/images/reply_all.png" CssClass="img_bt" title="Limpar filtro" OnClick="btn_limpar_filtro_Click" />
                                        </div>
                                    </td>

                                </tr>

                            </table>

                            <br />
                            <div id="content_gridview">
                                <asp:GridView ID="gridview_tickets" runat="server" BackColor="White" BorderColor="#999999" Width="100%" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black" GridLines="Horizontal" OnSelectedIndexChanged="gridview_agente_tickets_SelectedIndexChanged">
                                    <AlternatingRowStyle BackColor="#e6e7e8" />
                                    <Columns>
                                        <asp:CommandField ButtonType="Image" ControlStyle-CssClass="img_bt" SelectImageUrl="~/images/plus_button_01.png" ShowSelectButton="True" ControlStyle-BackColor="Transparent">
                                            <ControlStyle CssClass="img_bt" Width="11px" />
                                        </asp:CommandField>
                                    </Columns>
                                    <FooterStyle BackColor="#CCCCCC" />
                                    <HeaderStyle HorizontalAlign="Left" BackColor="#58595b" CssClass="head_lista" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="#808080" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                    <SortedAscendingHeaderStyle BackColor="#808080" />
                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                    <SortedDescendingHeaderStyle BackColor="#383838" />
                                </asp:GridView>
                            </div>

                        </div>

                        <nav id="content_detail_parent">

                            <div id="content_view_detail" style="padding-left: 5px;">
                                <label id="tl_parent_detail" class="tl_page" style="font-size: 16px; text-shadow: 10px 10px 5px #888888;">Detalhes do Usuário</label><br />
                                <hr />

                                <div id="content_detail_parent_view">

                                    <div>
                                        <label for="lbl_nome_detail">Nome</label><br />
                                        <asp:Label ID="lbl_nome_detail" runat="server" class="tl_page" Style="font-size: 14px; font-weight: bold;"></asp:Label><br />
                                    </div>

                                    <div>
                                        <label for="lbl_username_detail">Login</label><br />
                                        <asp:Label ID="lbl_username_detail" runat="server" class="tl_page" Style="font-size: 14px; font-weight: bold;"></asp:Label><br />
                                    </div>

                                    <table>
                                        <tr>
                                            <td>
                                                <label class="go_up" for="lbl_aberto_detail">Tickets Atribuídos</label><br />
                                                <asp:Label ID="lbl_atribuido_detail" runat="server" class="tl_page" Style="font-size: 14px; font-weight: bold;"></asp:Label><br />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="go_up" for="lbl_aberto_detail">Tickets Abertos</label><br />
                                                <asp:Label ID="lbl_aberto_detail" runat="server" class="tl_page" Style="font-size: 14px; font-weight: bold;"></asp:Label><br />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="go_up" for="lbl_fechado_detail">Tickets Fechados</label><br />
                                                <asp:Label ID="lbl_fechado_detail" runat="server" class="tl_page" Style="font-size: 14px; font-weight: bold;"></asp:Label><br />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label for="lbl_ticket_detail">Total de Tickets</label><br />
                                                <asp:Label ID="lbl_total_detail" runat="server" class="tl_page" Style="font-size: 14px; font-weight: bold;"></asp:Label><br />
                                                <%--<asp:TextBox CssClass="txtInput" ID="" runat="server"></asp:TextBox><br />--%>
                                            </td>
                                        </tr>

                                    </table>

                                </div>
                            </div>
                        </nav>

                    </div>

                </ContentTemplate>
            </asp:UpdatePanel>


        </div>
    </form>

</body>
</html>

