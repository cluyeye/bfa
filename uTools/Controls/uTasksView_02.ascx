﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="uTasksView_02.ascx.cs" Inherits="uTools.Controls.uTasksView_02" %>



<style>
    #corpo_tasks {
        width: 100%;
        height: 510px;
        overflow-y: auto;
        position: relative;
    }

    button.accordion {
        background-color: #ddd;
        color: #fff;
        cursor: pointer;
        padding: 18px;
        width: 100%;
        border: none;
        text-align: left;
        outline: none;
        font-size: 15px;
        transition: 0.4s;
    }

        button.accordion:hover {
            background-color: #aaa;
        }

    .updatepanel {
        /*background: linear-gradient( grey, white);*/
        /*background-color #efefef;*/
    }

    input {
        padding: 12px;
        border: solid 0px;
        background-color: #fff;
    }

        input.editdescricao {
            padding: 10px;
            width: 100%;
            border: 0px;
            background-color: #fff;
            outline: none;
        }

    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable auto if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    }

    /* Modal Content */
    .modal-content {
        position: relative;
        background-color: #eee;
        /*background-color: none;*/
        margin: auto;
        padding: 0;
        /*border: 1px solid #888;*/
        width: 200px;
        /*box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);*/
        -webkit-animation-name: animatetop;
        -webkit-animation-duration: 0.4s;
        animation-name: animatetop;
        animation-duration: 0.4s;
    }

    /* The Close Button */
    .close {
        color: white;
        float: left;
        font-size: 32px;
        font-weight: bold;
    }

    .sg_close:hover,
    .sg_close:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
    }

    .modal-header {
        padding: 2px 16px;
        background-color: #eee;
        /*background-image: url('ucall-img/ClickToHeader_01.png');*/
        background-repeat: no-repeat;
        width: 200px;
        height: 50px;
        color: white;
    }

    .modal-body {
        padding: 2px 16px;
        background-color: #eee;
        /*background-color: none;*/
        /*background-image: url('ucall-img/ClickTo_background.png');*/
        background-repeat: no-repeat;
        width: 200px;
        height: 200px;
    }

    .modal-footer {
        padding: 2px 16px;
        background-color: #eee;
        /*background-image: url("ucall-img/ClickTo_footer.png");*/
        background-repeat: no-repeat;
        width: 200px;
        height: 50px;
        color: white;
    }

    /* Add Animation */
    @-webkit-keyframes animatetop {
        from {
            top: -300px;
            opacity: 0;
        }

        to {
            top: 0;
            opacity: 1;
        }
    }

    @keyframes animatetop {
        from {
            top: -300px;
            opacity: 0;
        }

        to {
            top: 0;
            opacity: 1;
        }
    }

    .head_detalhe {
        width: 150px;
        text-align: left;
        vertical-align: top;
        font-weight: bold;
    }

    .head_lista {
        text-align: left;
        background-color: gray;
        color: black;
    }

    .selected_row {
        text-align: justify;
        background-color: lightgray;
    }



    div {
        margin: 0;
        padding: 0;
    }

    #principal_Tasks {
        width: 580px;
        position: relative;
        height: 300px;
    }


    #lateral_Tasks {
        width: 490px;
        height: 300px;
        overflow-y: auto;
        border-radius: 6px;
        margin-left: 10px;
        margin-top: -300px;
        float: right;
        /*top: -156px;*/
    }


    #gridview_historico {
        overflow-y: auto;
    }


    #corpo_item {
        width: 150px;
        margin-left: 1%;
    }

    .img_bt {
        width: 40px;
        padding: 0;

    }


    .modal-body .txtInput {
        border: 1px solid #e6e7e8;
        width: 230px;
        border-radius: 4px;
        height: 4px;
    }


    .modal-body .selectInput {
        border: 1px solid #e6e7e8;
        width: 260px;
        border-radius: 4px;
        height: 25px;
    }


    .img_bt {
        width: 40px;
        padding: 0;
    }
</style>



<div id="corpo_tasks">

    <asp:ScriptManager ID="ScriptManagerTarefas" runat="server">
    </asp:ScriptManager>


    <ajaxToolkit:CollapsiblePanelExtender ID="collpanel_Detalhe" runat="server"
        ExpandControlID="btn_detalhe" CollapseControlID="btn_detalhe" TargetControlID="panel_detalhe" SuppressPostBack="true" Collapsed="true" />
    <button id="btn_detalhe" class="accordion">DETALHE</button>
    <asp:Panel ID="panel_detalhe" runat="server">
        <asp:UpdatePanel ID="updpanel_detalhe" runat="server" class="updatepanel">
            <ContentTemplate>
                <br />

                <div>

                    <div id="principal_Tasks">
                        <table style="width: 100%;">
                            <tr>
                                <td class="corpo_item">
                                    <label for="lbl_tipo" class="head_detalhe">Tipo</label><br />
                                    <asp:Label ID="lbl_tipo" runat="server" Style="font-family: 'Courier New, Courier, monospace'"></asp:Label>
                                </td>

                                <td class="corpo_item">
                                    <label for="lbl_tipificacao" class="head_detalhe">Tipificação</label><br />
                                    <asp:Label ID="lbl_tipificacao" runat="server" Style="font-family: 'Courier New, Courier, monospace'"></asp:Label>
                                </td>

                                <td class="corpo_item">
                                    <label for="lbl_ID" class="head_detalhe">ID</label><br />
                                    <asp:Label ID="lbl_ID" runat="server" Style="font-family: 'Courier New, Courier, monospace'"></asp:Label>
                                </td>

                            </tr>

                            <tr>
                                <td></td>

                                <td>
                                    <label for="dropbox_detalhe_departamento" class="head_detalhe">Departamento</label><br />
                                    <asp:DropDownList ID="dropbox_detalhe_departamento" runat="server" Style="font-family: 'Courier New, Courier, monospace'">
                                        <asp:ListItem Selected="True" Text="Departamento" Value="-1"></asp:ListItem>
                                    </asp:DropDownList>

                                </td>

                                <td>

                                    <label for="dropbox_detalhe_estado" class="head_detalhe">Estado</label><br />
                                    <asp:DropDownList ID="dropbox_detalhe_estado" runat="server">
                                        <asp:ListItem Selected="True" Text="Estado" Value="-1"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>

                            </tr>

                        </table>

                        <br />

                        <table>
                            <tr>
                                <td>
                                    <label for="lbl_descricao" class="head_detalhe">Descrição</label><br />
                                    <asp:TextBox ID="lbl_descricao" runat="server" TextMode="MultiLine" Rows="6" Width="100%" Columns="70" CssClass="editdescricao"></asp:TextBox>

                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <label for="lbl_resumo" class="head_detalhe">Resolução</label><br />
                                    <asp:TextBox ID="lbl_resumo" runat="server" Columns="70" CssClass="editdescricao" Rows="3" Width="100%" TextMode="MultiLine"></asp:TextBox>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="lbl_resolucao" class="head_detalhe">Resumo</label><br />
                                    <asp:TextBox ID="lbl_resolucao" runat="server" TextMode="MultiLine" Rows="3" Width="100%" Columns="70" CssClass="editdescricao"></asp:TextBox>
                                </td>

                            </tr>

                        </table>

                    </div>


                    <div id="lateral_Tasks" align="center">

                        <table style="margin-left: 20px;">

                            <tr>
                                <td class="corpo_item">
                                    <label for="lbl_registo" class="head_detalhe">Registo</label><br />
                                    <asp:Label ID="lbl_registo" runat="server"></asp:Label>
                                </td>

                                <td>
                                    <button id="modal_add_comentario" onclick="ClickOnBtnComentario();return false;">
                                        add comentário
                                    </button>

                                </td>

                            </tr>

                            <tr>
                                <td class="corpo_item">
                                    <label for="lbl_fecho" class="head_detalhe">Fecho</label><br />
                                    <asp:Label ID="lbl_fecho" runat="server"></asp:Label>
                                </td>

                                <td>
                                    <button id="btn_modal_estado" onclick="ClickOnBtnEstado();return false;">Alterar Estado</button>
                                </td>
                            </tr>

                            <tr>
                                <td class="corpo_item">
                                    <label for="lbl_sla" class="head_detalhe">SLA</label><br />
                                    <asp:Label ID="lbl_sla" runat="server"></asp:Label>

                                </td>

                                <td>
                                    <button id="btn_modal_departamento" onclick="ClickOnBtnDepartamento();return false;">Atribuir a Departamento</button>
                                </td>
                            </tr>
                        </table>

                        <asp:GridView ID="gridview_historico" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black" GridLines="Horizontal">
                            <FooterStyle BackColor="#e6e7e8" />
                            <HeaderStyle CssClass="head_lista" BackColor="Black" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                            <RowStyle />
                            <SelectedRowStyle CssClass="selected_row" BackColor="#808080" Font-Bold="True" ForeColor="White" />
                            <AlternatingRowStyle BackColor="#e6e7e8" />
                            <Columns>
                                <asp:BoundField DataField="DataRegisto" HeaderText="Data" />
                                <asp:BoundField DataField="Departamento" HeaderText="Departamento" />
                                <asp:BoundField DataField="Estado" HeaderText="Estado" />
                                <asp:BoundField DataField="UserID" HeaderText="Utilizador" />
                            </Columns>
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#808080" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#383838" />
                        </asp:GridView>

                        <br />
                        <br />

                        <div runat="server" style="height: 320px; overflow-y: auto;">
                            <asp:GridView ID="gridview_comentario" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black" GridLines="Horizontal">
                                <FooterStyle BackColor="#e6e7e8" />
                                <HeaderStyle CssClass="head_lista" BackColor="Black" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                <RowStyle />
                                <SelectedRowStyle CssClass="selected_row" BackColor="#808080" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="#e6e7e8" />
                                <Columns>
                                    <asp:BoundField DataField="DataRegisto" HeaderText="Data" />
                                    <asp:BoundField DataField="Comentario" HeaderText="Comentário" />
                                    <asp:BoundField DataField="UserID" HeaderText="Utilizador" />
                                </Columns>
                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#808080" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#383838" />
                            </asp:GridView>
                        </div>
                    </div>

                </div>


                <!-- The Modal -->
                <div id="myModal" class="modal">
                    <!-- Modal content -->
                    <div class="modal-content">
                        <div id="clickto_modal_header" class="modal-header">
                            <span class="close">&times</span>
                        </div>
                        <div class="modal-body">
                            <table>
                                <tr>
                                    <asp:HiddenField ID="hidden_modal_action" runat="server" />
                                    <td>
                                        <label for="dropbox_estado_edit" class="head_detalhe">Estado</label><br />
                                        <asp:DropDownList ID="dropbox_estado_edit" CssClass="selectInput" runat="server" Style="visibility: hidden; border: 1px solid #e6e7e8;">
                                            <asp:ListItem Selected="True" Text="Estado" Value="-1"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>

                                    <td>
                                        <label for="dropbox_departamento_edit" class="head_detalhe">Departamento</label><br />
                                        <asp:DropDownList ID="dropbox_departamento_edit" CssClass="selectInput" runat="server" Style="visibility: hidden; border: 1px solid #e6e7e8;">
                                            <asp:ListItem Selected="True" Text="Departamento" Value="-1"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="txtbox_comentario_edit" class="head_detalhe">Comentário</label><br />
                                        <asp:TextBox ID="txtbox_comentario_edit" CssClass="txtInput" runat="server" Style="visibility: hidden; border: 1px solid #e6e7e8;"></asp:TextBox>
                                    </td>

                                    <td>
                                        <asp:ImageButton ID="btn_modal_action" ImageUrl="~/imagens/search_01.png" CssClass="img_bt" runat="server" OnClick="btn_modal_action_Click" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>

    <ajaxToolkit:CollapsiblePanelExtender ID="collpanel_lista" runat="server"
        ExpandControlID="btn_lista" CollapseControlID="btn_lista" TargetControlID="panel_lista" SuppressPostBack="true" />
    <button id="btn_lista" class="accordion">LISTA</button>
    <asp:Panel ID="panel_lista" runat="server">
        <asp:UpdatePanel ID="updpanel_lista" runat="server">
            <ContentTemplate>
                <div id="div_filtro">

                    <asp:TextBox ID="txtbox_id" runat="server" CssClass="txtInput" placeholder="ID"></asp:TextBox>
                    <asp:TextBox ID="txtbox_descricao" runat="server" CssClass="txtInput" placeholder="Descricao"></asp:TextBox>
                    <asp:DropDownList ID="dropbox_tipo" runat="server">
                        <asp:ListItem Selected="True" Text="Tipo Tarefa" Value="-1"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:DropDownList ID="dropbox_departamento" CssClass="selectInput" runat="server">
                        <asp:ListItem Selected="True" Text="Departamento" Value="-1"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:DropDownList ID="dropbox_estado" CssClass="selectInput" runat="server">
                        <asp:ListItem Selected="True" Text="Estado" Value="-1"></asp:ListItem>
                    </asp:DropDownList>
                    <p></p>
                    <asp:TextBox ID="txtbox_dataregisto_inicio" placeholder="Data Registo Inicio" CssClass="selectInput" runat="server"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="cal_dataregisto_inicio" Format="dd-MM-yyyy" TargetControlID="txtbox_dataregisto_inicio" runat="server" />
                    <b>-</b>
                    <asp:TextBox ID="txtbox_dataregisto_fim" CssClass="txtInput" placeholder="Data Registo Fim" runat="server"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="cal_dataregisto_fim" Format="dd-MM-yyyy" TargetControlID="txtbox_dataregisto_fim" runat="server" />
                    <asp:TextBox ID="txtbox_datafecho_inicio" CssClass="txtInput" placeholder="Data Fecho Inicio" runat="server"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="cal_datafecho_inicio" Format="dd-MM-yyyy" TargetControlID="txtbox_datafecho_inicio" runat="server" />
                    <b>-</b>
                    <asp:TextBox ID="txtbox_datafecho_fim" CssClass="txtInput" placeholder="Data Fecho Fim" runat="server"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="cal_datafecho_fim" Format="dd-MM-yyyy" TargetControlID="txtbox_datafecho_fim" runat="server" />
                    <asp:ImageButton ID="btn_filtro" runat="server" OnClick="btn_filtro_Click" ImageUrl="~/imagens/search_01.png" class="img_bt" />
                    <asp:ImageButton ID="btn_clearfiltro" runat="server" OnClick="btn_clearfiltro_Click" ImageUrl="~/imagens/search_01.png" class="img_bt" />

                </div>
                <p></p>
                <asp:GridView ID="gridview_tarefas" runat="server" AllowSorting="True" AutoGenerateColumns="False" Width="100%"
                    OnSorting="gridview_tarefas_Sorting" OnSelectedIndexChanged="gridview_tarefas_SelectedIndexChanged" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black" GridLines="Horizontal">

                    <FooterStyle BackColor="#e6e7e8" />
                    <HeaderStyle CssClass="head_lista" BackColor="Black" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <RowStyle />
                    <SelectedRowStyle CssClass="selected_row" BackColor="#808080" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="#e6e7e8" />
                    <Columns>
                        <asp:CommandField ButtonType="Image" ControlStyle-CssClass="img_bt" SelectImageUrl="~/imagens/search_01.png" ShowSelectButton="True">
                            <ControlStyle CssClass="img_bt" />
                        </asp:CommandField>
                        <%--<asp:ImageButton ID="ImageButton1" ImageUrl="~/imagens/search_01.png" ButtonType="Image" CssClass="img_bt" ShowSelectButton="True" runat="server" />--%>
                        <asp:BoundField DataField="ID" HeaderText="ID" SortExpression="id" />
                        <asp:BoundField DataField="Tipo" HeaderText="Tipo Tarefa" SortExpression="Tipo" />
                        <asp:BoundField DataField="Descricao" HeaderText="Descrição" SortExpression="Descricao" />
                        <asp:BoundField DataField="DataRegisto" HeaderText="Data de Registo" SortExpression="DataRegisto" />
                        <asp:BoundField DataField="DataFecho" HeaderText="Data de Fecho" SortExpression="DataFecho" />
                        <asp:BoundField DataField="Departamento" HeaderText="Departamento" SortExpression="Departamento" />
                        <asp:BoundField DataField="Estado" HeaderText="Estado" SortExpression="Estado" />
                        <asp:BoundField DataField="diferenca_SLA" HeaderText="SLA To" SortExpression="diferenca_SLA" />
                    </Columns>
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#808080" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#383838" />


                </asp:GridView>

            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>


</div>

<script>
    // Get the modal
    var modal = document.getElementById('myModal');
    var modalheader = document.getElementById("clickto_modal_header");

    // Get the button action
    var btn = document.getElementById("hidden_modal_action");

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks the button, open the modal
    function ClickOnBtnEstado() {
        modalheader.innerText = 'ALTERAR ESTADO';
        document.getElementById("hidden_modal_action").value = 'estado';
        //document.getElementById("dropbox_estado_edit").style.display = "block";
        modal.style.display = "block";
    }

    function ClickOnBtnDepartamento() {
        modalheader.innerText = 'ALTERAR DEPARTAMENTO';
        document.getElementById("hidden_modal_action").value = 'departamento';
        document.getElementById("dropbox_departamento_edit").style.visibility = "visible";
        modal.style.display = "block";
    }

    function ClickOnBtnComentario() {
        modalheader.innerText = 'ALTERAR COMENTÁRIO';
        document.getElementById("hidden_modal_action").value = 'comentario';
        //document.getElementById("txtbox_comentario_edit").style.visibility = "visible";
        modal.style.display = "block";
    }

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }

</script>