﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="userChange_Pwd.ascx.cs" Inherits="uTools.Controls.userChange_Pwd" %>



<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>uTools | Autenticação</title>

    <link rel="stylesheet" type="text/css" href="../css/style_userchangepwd.css" />
    <link rel="stylesheet" type="text/css" href="../css/geral.css" />

</head>

<body>

        <asp:PlaceHolder runat="server" ID="LoginForm">

            <div id="container_changepwd" align="center">

                <div>
                    <img title="" id="img_changepwd" src="../imagens/userchange_img.png" />

                    <div id="content_user_changepwd" align="justify">

                        <p>
                            <asp:Literal runat="server" ID="StatusMessage" />
                        </p>
                        <div style="margin-bottom: 10px">
                            <asp:Label runat="server" AssociatedControlID="UserName" class="lb_changepwd">Nome do Usuario*</asp:Label>
                            <div>
                                <asp:TextBox CssClass="txtInput" runat="server" ID="UserName" />
                            </div>
                        </div>
                        <div style="margin-bottom: 10px">
                            <asp:Label runat="server" AssociatedControlID="CurrentPwd" class="lb_changepwd">Palavra-Passe Atual*</asp:Label>
                            <div>
                                <asp:TextBox CssClass="txtInput" runat="server" ID="CurrentPwd" TextMode="Password" />
                            </div>
                        </div>
                        <div style="margin-bottom: 10px">
                            <asp:Label runat="server" AssociatedControlID="Password" class="lb_changepwd">Palavra-Passe Nova*</asp:Label>
                            <div>
                                <asp:TextBox CssClass="txtInput" runat="server" ID="Password" TextMode="Password" />
                            </div>
                        </div>
                        <div style="margin-bottom: 10px">
                            <asp:Label runat="server" AssociatedControlID="ConfirmPassword" class="lb_changepwd">Confirmar Palavra-Passe*</asp:Label>
                            <div>
                                <asp:TextBox CssClass="txtInput" runat="server" ID="ConfirmPassword" TextMode="Password" />
                            </div>
                        </div>
                        <div>
                            <div>
                                <asp:ImageButton ID="btn_alterar" runat="server" ImageUrl="~/imagens/ok.png" OnClick="CreateUser_Click" CssClass="img_bt" title="Alterar Palavra-passe" />
                                <%--<asp:Button runat="server" OnClick="CreateUser_Click" Text="Register" />--%>
                            </div>
                        </div>


                    </div>

                </div>


            </div>

        </asp:PlaceHolder>

</body>

</html>
