﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Servicos.ascx.cs" Inherits="uTools.Controls.Servicos" %>



<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />

    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta http-equiv="content-language" content="pt-br" />

    <meta name="description" content="Ferramenta de Ticketing, que vai melhorar a
        Experiência de Cliente dando resposta às situações colocadas nos vários canais
        de interacção da sua organização (agências, pontos de venda, contact center, etc).
        A partir de qualquer área da sua empresa, a resposta é dada dentro do prazo expectável,
        aumentando a Qualidade na relação com o Cliente. Os pedidos de informação, as reclamações
        ou sugestões são direccionadas a quem decide e resolvem-se com maior rapidez, eficiência
        e controlo da interacção" />

    <meta name="locale" content="AO" />
    <meta name="language" content="pt" />
    <meta name="country" content="Angola" />

    <link rel="shortcut icon" href="../images/icone_utools.png" />

    <title>uTOOLs</title>

<style>
    
    @charset 'utf-8';

    @font-face {
        font-family: ephismere;
        src: url('../fontes/Styling W00 Regular.ttf');
    }


    body, button {
        /*background-color: #e6e7e8;*/
        font-family: 'ephismere';
        /*font-size: 12px;*/
        color: #58595b;
        max-height: none;
    }
    
        
    .sidenav_servicos {
        margin-top: -30px;
        height: 65%;
        width: 0;
        /*position: fixed;*/
        z-index: 1;
        top: 185px;
        left: 50px;
        border-radius: 4px;
        background-color: #58595b;
        overflow-x: hidden;
        transition: 0.5s;
        padding-top: 60px;
    }

            .sidenav_servicos a {
                padding: 8px 8px 8px 32px;
                text-decoration: none;
                font-size: 20px;
                color: white;
                display: block;
                transition: 0.3s;
            }

                .sidenav_servicos a:hover, .offcanvas a:focus {
                    color: #f1f1f1;
                    background-color: #808080;
                }

                .sidenav_servicos a:checked
                {
                    font-weight: bold;
                    box-shadow: 10px 10px 5px #888888;
                }

            .sidenav_servicos .closebtn {
                position: absolute;
                top: 0;
                right: 25px;
                font-size: 36px;
                margin-left: 50px;
            }

        #main_servicos {
            transition: margin-left .5s;
            padding: 16px;
        }

        @media screen and (max-height: 450px) {
            .sidenav_servicos {
                padding-top: 15px;
            }

                .sidenav_servicos a {
                    font-size: 18px;
                }
        }


        #content_link_op_servicos
        {
            margin: 0;
            padding: 0;
        }


        #link_op_servicos {
            border: 2px solid white;
            width: 100%;
            /*position: relative;*/
            height: 750px;
            border-radius: 6px;
            margin: 0;
            padding: 0;
            top: -140px;
            position: relative;
            float: left;
            /*margin-top: -100px;*/
        }

        #container_titulo_servicos
        {
            
            top: -155px;
            left: 10px;
            margin: 0;
            padding: 0;
            /*border: 3px solid yellow;*/
            position: relative;
            margin: 0;
            padding: 0;
        }

        #menu_servicos
        {
            padding: 10px;
            margin-top: 5px;
        }

</style>


</head>
<body onfocus="openNav_Count()">

    <br />
        <span id="menu_servicos" style="font-size: 20px; cursor: pointer" onclick="openNav_Count()">&#9776; menu</span>

    <div id="mysidenav_servicos" class="sidenav_servicos">


        <a href="javascript:void(0)" target="link_op_servicos" style="color: red; font-size: 18px; padding: 0; position: relative; top: -50px;" class="closebtn" onclick="closeNav()">&times;</a>
        <a href="Call_Estatistica_Manager.aspx" target="link_op_servicos" onclick="change_title_servicos('ESTATÍSTICAS')">Estatísticas</a>
        <%--<a href="../UserChangePwd.aspx" target="link_op_servicos">Palavra-passe</a>--%>
    </div>

    <div id="main_servicos">

        <div id="container_titulo_servicos">
                <label id="txt_tl_servicos" class="txt_Titulo" style="font-size: 24px; text-shadow: 10px 10px 5px #888888;" ></label>
        </div>

        <div id="content_link_op_servicos" runat="server">
            <iframe id="link_op_servicos" name="link_op_servicos"></iframe>
        </div>


    </div>

    <script>
        function openNav_Count() {
            document.getElementById("mysidenav_servicos").style.visibility = true;
            document.getElementById("mysidenav_servicos").style.width = "180px";

            document.getElementById("main_servicos").style.marginLeft = "180px";

            document.getElementById("container_titulo_servicos").setAttribute('align', 'right');
            document.getElementById("menu_servicos").style.visibility = false;
        }

        function closeNav() {
            document.getElementById("mysidenav_servicos").style.width = "0";
            document.getElementById("mysidenav_servicos").style.visibility = false;

            document.getElementById("main_servicos").style.marginLeft = "0";

            document.getElementById("container_titulo_servicos").setAttribute('align', 'right');
            document.getElementById("menu_servicos").style.visibility = true;

        }

        function change_title_servicos(text) {

            document.getElementById("txt_tl_servicos").innerHTML = text;
        }
    </script>

</body>
</html>
