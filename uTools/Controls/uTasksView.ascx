﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="uTasksView.ascx.cs" Inherits="uTools.Controls.uTasksView" %>

<%@ Register Src="~/Controls/email_enviar.ascx" TagName="Email_Detalhe" TagPrefix="email" %>
<%@ Register Src="~/Controls/uTaskCreate.ascx" TagName="Task_Create" TagPrefix="taskcreate" %>


<!DOCTYPE html>

<html>
<head>
    <title>uTasks</title>
</head>
<body>

    <asp:ScriptManager ID="ScriptManagerTarefas" runat="server">
    </asp:ScriptManager>
    <div id="corpo_tasks">

        <asp:UpdatePanel ID="updpanel_lista" runat="server">
            <ContentTemplate>

                <ajaxToolkit:CollapsiblePanelExtender ID="collpanel_Detalhe" runat="server"
                    ExpandControlID="btn_detalhe" CollapseControlID="btn_detalhe" TargetControlID="panel_detalhe" SuppressPostBack="true" />
                <button id="btn_detalhe" title="" class="accordion">
                    <asp:Label ID="lxt_head_main_tasks" runat="server"></asp:Label>
                </button>


                <div id="detalhe_principal_Tasks_all">



                    <asp:Panel ID="panel_detalhe" runat="server">
                        <asp:UpdatePanel ID="updpanel_detalhe" runat="server" class="updatepanel">
                            <ContentTemplate>

                                <asp:MultiView ID="mv_tarefa" runat="server" ActiveViewIndex="0" Visible="false">
                                    <asp:View ID="view_detalhe" runat="server">
                                        <div id="content_accao">
                                            <asp:UpdatePanel ID="pnlupd_accao" runat="server">
                                                <ContentTemplate>
                                                    <br />
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:DropDownList ID="dropbox_accao" CssClass="selectInput" runat="server" OnSelectedIndexChanged="dropbox_accao_SelectedIndexChanged" AutoPostBack="true">
                                                                    <asp:ListItem Value="1">ATRIBUIR</asp:ListItem>
                                                                    <asp:ListItem Value="2">ALTERAR ESTADO</asp:ListItem>
                                                                    <asp:ListItem Value="3">ALTERAR TIPO</asp:ListItem>
                                                                    <asp:ListItem Value="4">ADICIONAR COMENTÁRIO</asp:ListItem>
                                                                    <asp:ListItem Value="5">GERIR CONTEÚDOS</asp:ListItem>
                                                                    <asp:ListItem Value="6">ENVIAR TICKET</asp:ListItem>
                                                                </asp:DropDownList>

                                                            </td>

                                                            <td>
                                                                <%--<asp:ImageButton ID="" runat="server" CssClass="img_bt" ImageUrl="~/images/ok.png" title="Filtar Busca" />
                                                                <asp:Button ID="" CssClass="btn_form" Visible="false" runat="server" Text="  " OnClick="btn_apagar_contactos_cc_Click" />--%>
                                                                <asp:Button ID="btn_accao_ok" CssClass="btn_form" runat="server" title="Executar Acção" Text="Executar Acção" />
                                                            </td>
                                                        </tr>

                                                    </table>

                                                    <ajaxToolkit:ModalPopupExtender ID="modal_accao" runat="server"
                                                        TargetControlID="btn_accao_ok" PopupControlID="pnl_accao" CancelControlID="btn_accao_cancel"
                                                        BackgroundCssClass="fundo_modal" DropShadow="true"
                                                        OnPreRender="dropbox_accao_SelectedIndexChanged">
                                                    </ajaxToolkit:ModalPopupExtender>

                                                    <asp:Panel ID="pnl_accao" runat="server" CssClass="modal" align="center" Style="color: #fff">
                                                        <asp:MultiView ID="mv_accao" runat="server" ActiveViewIndex="3">
                                                            <asp:View ID="view_dropbox" runat="server">
                                                                <div class="content_modal">

                                                                    <span id="sp_alterar" runat="server" class="txt_Titulo" style="text-shadow: 10px 10px 5px #888888; color: #fff; margin-left: 100px;"></span>
                                                                    <br />

                                                                    <hr class="space_bottom" />
                                                                    <label id="lbl_alterar" runat="server" for="txtbox_editar_comentario" class="head_detalhe"></label>
                                                                    <br />
                                                                    <asp:DropDownList ID="dropbox_alterar" CssClass="selectInput" runat="server"></asp:DropDownList>
                                                                </div>
                                                            </asp:View>
                                                            <asp:View ID="view_comentario" runat="server">
                                                                <div class="content_modal">
                                                                    <span class="txt_Titulo" style="text-shadow: 10px 10px 5px #888888; color: #fff; margin-left: 100px;">ADIÇÃO DE COMENTÁRIO</span><br />
                                                                    <hr class="space_bottom" />
                                                                    <label for="txtbox_editar_comentario" class="head_detalhe">Comentário</label><br />
                                                                    <asp:TextBox ID="txtbox_editar_comentario" runat="server" TextMode="MultiLine" Rows="6" Width="99%" Columns="70" CssClass="editdescricao"></asp:TextBox>
                                                                </div>
                                                            </asp:View>
                                                            <asp:View ID="view_descricao" runat="server">
                                                                <div class="content_modal">
                                                                    <span class="txt_Titulo" style="text-shadow: 10px 10px 5px #888888; color: #fff; margin-left: 100px;">GESTÃO DE CONTEÚDOS</span><br />
                                                                    <hr class="space_bottom" />
                                                                    <label for="txtbox_editar_descricao" class="head_detalhe">Descricao</label><br />
                                                                    <asp:TextBox ID="txtbox_editar_descricao" runat="server" TextMode="MultiLine" Rows="6" Width="99%" Columns="70" CssClass="editdescricao"></asp:TextBox>
                                                                    <br />
                                                                    <label for="txtbox_editar_resumo" class="head_detalhe">Resumo</label><br />
                                                                    <asp:TextBox ID="txtbox_editar_resumo" runat="server" TextMode="MultiLine" Rows="6" Width="99%" Columns="70" CssClass="editdescricao"></asp:TextBox>
                                                                    <br />
                                                                    <label for="txtbox_editar_resolucao" class="head_detalhe">Resolução</label><br />
                                                                    <asp:TextBox ID="txtbox_editar_resolucao" runat="server" TextMode="MultiLine" Rows="6" Width="99%" Columns="70" CssClass="editdescricao"></asp:TextBox>
                                                                    <br />

                                                                </div>
                                                            </asp:View>
                                                            <asp:View ID="view_email_send" runat="server">
                                                                <div id="panel_detalhe_bottom">
                                                                    <%--<div class="content_modal">--%>
                                                                    <br />
                                                                    <span class="txt_Titulo" style="text-shadow: 10px 10px 5px #888888; color: #fff; margin-left: 100px;">ENVIAR INFO POR EMAIL</span><br />
                                                                    <hr class="space_bottom" />
                                                                    <email:Email_Detalhe ID="email_enviar_info" runat="server" />
                                                                    <br />
                                                                </div>
                                                            </asp:View>
                                                        </asp:MultiView>
                                                        <div id="content_modal_bt" align="justify">
                                                            <%--<asp:ImageButton ID="" runat="server" CssClass="img_bt" ImageUrl="~/images/ok.png" title="" />
                                                            <asp:ImageButton ID="" runat="server" CssClass="img_bt" ImageUrl="~/images/close_01.png" title="" />--%>

                                                            <asp:Button ID="btn_accao_confirm" CssClass="btn_form" runat="server" title="Confirmar" Text="      Ok      " OnClick="btn_accao_confirm_Click" />
                                                            <asp:Button ID="btn_accao_cancel" CssClass="btn_form" runat="server" title="Cancelar" Text="Cancelar" />
                                                        </div>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="btn_accao_confirm" EventName="Click" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </div>
                                        <asp:UpdatePanel ID="updpnl_detalhe_tarefa" runat="server">
                                            <ContentTemplate>
                                                <div id="panel_detalhe_section">
                                                    <div id="detalhe_principal_Tasks">
                                                        <table style="width: 100%;">
                                                            <tr>
                                                                <td class="corpo_item">
                                                                    <label for="lbl_ID" class="head_detalhe">ID Tarefa</label><br />
                                                                    <asp:Label ID="lbl_ID" runat="server"></asp:Label>

                                                                </td>

                                                                <td class="corpo_item" colspan="2">
                                                                    <label for="lbl_tipificacao" class="head_detalhe">Tipificação</label><br />
                                                                    <asp:Label ID="lbl_tipificacao" runat="server"></asp:Label>
                                                                </td>

                                                                <td class="corpo_item">
                                                                    <label for="lbl_tipo" class="head_detalhe">Tipo</label><br />
                                                                    <asp:Label ID="lbl_tipo" runat="server"></asp:Label>


                                                                </td>

                                                            </tr>


                                                            <tr>

                                                                <td>
                                                                    <label for="lbl_canal_contacto" class="head_detalhe">ID Canal</label><br />
                                                                    <asp:Label ID="lbl_canal_contacto_id" runat="server"></asp:Label>

                                                                    <br />

                                                                    <label for="lbl_canal_contacto_id" class="head_detalhe">Descrição Canal</label><br />
                                                                    <asp:Label ID="lbl_canal_contacto" runat="server"></asp:Label><br />
                                                                </td>

                                                                <td>
                                                                    <label for="lbl_AspUser" class="head_detalhe">Usuário</label><br />
                                                                    <asp:Label ID="lbl_AspUser" runat="server"></asp:Label>

                                                                    <br />

                                                                    <label for="lbl_feito_por" class="head_detalhe">Nome de Usuário</label><br />
                                                                    <asp:Label ID="lbl_feito_por" runat="server"></asp:Label>

                                                                </td>

                                                                <td>
                                                                    <label for="lbl_cliente" class="head_detalhe">ID Cliente</label><br />
                                                                    <asp:Label ID="lbl_cliente" runat="server"></asp:Label>
                                                                    <br />

                                                                    <label for="lbl_feito_por" class="head_detalhe">Nome do Cliente</label><br />
                                                                    <asp:Label ID="lbl_cliente_nome" runat="server"></asp:Label>

                                                                </td>

                                                                <td>
                                                                    <label for="lbl_diferenca_SLA" class="head_detalhe">Diferença SLA</label><br />
                                                                    <asp:Label ID="lbl_diferenca_SLA" runat="server"></asp:Label>

                                                                    <br />

                                                                    <label for="lbl_sla" class="head_detalhe">SLA</label><br />
                                                                    <asp:Label ID="lbl_sla" runat="server"></asp:Label>
                                                                </td>


                                                            </tr>



                                                            <tr>

                                                                <br />
                                                                <td>
                                                                    <label for="lbl_detalhe_departamento" class="head_detalhe">Departamento</label><br />
                                                                    <asp:Label ID="lbl_detalhe_departamento" runat="server"></asp:Label>
                                                                    <asp:HiddenField ID="hf_departamentoID" runat="server" />
                                                                </td>

                                                                <td>
                                                                    <label for="lbl_detalhe_estado" class="head_detalhe">Estado</label><br />
                                                                    <asp:Label ID="lbl_detalhe_estado" runat="server"></asp:Label>
                                                                    <asp:HiddenField ID="hf_estadoID" runat="server" />

                                                                    <br />

                                                                </td>

                                                                <td>
                                                                    <label for="lbl_registo" class="head_detalhe">Registo</label><br />
                                                                    <asp:Label ID="lbl_registo" runat="server"></asp:Label>
                                                                </td>

                                                                <td>
                                                                    <label for="lbl_fecho" class="head_detalhe">Fecho</label><br />
                                                                    <asp:Label ID="lbl_fecho" runat="server"></asp:Label>

                                                                </td>
                                                            </tr>

                                                        </table>

                                                        <br />

                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <label for="lbl_descricao" class="head_detalhe">Descrição</label><br />
                                                                    <asp:Label ID="lbl_descricao" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td>
                                                                    <label for="lbl_resumo" class="head_detalhe">Resumo</label><br />
                                                                    <asp:Label ID="lbl_resumo" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <label for="lbl_resolucao" class="head_detalhe">Resolução</label><br />
                                                                    <asp:Label ID="lbl_resolucao" runat="server"></asp:Label>
                                                                </td>

                                                            </tr>
                                                        </table>
                                                    </div>

                                                    <div id="detalhe_lateral_Tasks">
                                                        <span class="txt_Titulo">LISTA DE HISTÓRICO</span><br />
                                                        <hr />
                                                        <div id="tasks_detalhe_historico">
                                                            <asp:GridView ID="gridview_historico" runat="server" AutoGenerateColumns="False" BackColor="White" Width="100%" BorderColor="#999999" BorderStyle="Solid" BorderWidth="0px" CellPadding="3" ForeColor="Black" GridLines="Horizontal">
                                                                <FooterStyle BackColor="#e6e7e8" />
                                                                <HeaderStyle HorizontalAlign="Left" CssClass="head_lista" BackColor="#58595b" Font-Bold="True" ForeColor="White" />
                                                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                                                <RowStyle />
                                                                <SelectedRowStyle CssClass="selected_row" BackColor="#808080" Font-Bold="True" ForeColor="White" />
                                                                <AlternatingRowStyle BackColor="#e6e7e8" />
                                                                <Columns>
                                                                    <asp:BoundField DataField="DataRegisto" HeaderText="Data" />
                                                                    <asp:BoundField DataField="Departamento" HeaderText="Departamento" />
                                                                    <asp:BoundField DataField="Estado" HeaderText="Estado" />
                                                                    <asp:BoundField DataField="UserName" HeaderText="Utilizador" />
                                                                </Columns>
                                                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                                <SortedAscendingHeaderStyle BackColor="#808080" />
                                                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                                <SortedDescendingHeaderStyle BackColor="#383838" />
                                                            </asp:GridView>
                                                        </div>
                                                        <br />
                                                        <span class="txt_Titulo">LISTA DE COMENTÁRIOS</span><br />
                                                        <hr />
                                                        <div id="tasks_detalhe_comentario">
                                                            <asp:GridView ID="gridview_comentario" runat="server" AutoGenerateColumns="False" Width="100%" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="0px" CellPadding="3" ForeColor="Black" GridLines="Horizontal">
                                                                <FooterStyle BackColor="#e6e7e8" />
                                                                <HeaderStyle HorizontalAlign="Left" CssClass="head_lista" BackColor="#58595b" Font-Bold="True" ForeColor="White" />
                                                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                                                <RowStyle />
                                                                <SelectedRowStyle CssClass="selected_row" BackColor="#808080" Font-Bold="True" ForeColor="White" />
                                                                <AlternatingRowStyle BackColor="#e6e7e8" />
                                                                <Columns>
                                                                    <asp:BoundField DataField="DataRegisto" HeaderText="Data" />
                                                                    <asp:BoundField DataField="Comentario" HeaderText="Comentário" />
                                                                    <asp:BoundField DataField="UserName" HeaderText="Utilizador" />
                                                                </Columns>
                                                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                                <SortedAscendingHeaderStyle BackColor="#808080" />
                                                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                                <SortedDescendingHeaderStyle BackColor="#383838" />
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                    <asp:UpdatePanel ID="updpnl_gestao_emails" runat="server">
                                                        <ContentTemplate>
                                                            <span class="txt_Titulo">INTERAÇÕES POR EMAILs</span><br />
                                                            <hr />
                                                            <div id="detalhe_email_bottom">

                                                                <asp:GridView ID="gridview_email" runat="server" Width="99%" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px"
                                                                    CellPadding="3" ForeColor="Black" GridLines="Horizontal" OnSelectedIndexChanged="gridview_email_SelectedIndexChanged" AutoGenerateColumns="false">
                                                                    <AlternatingRowStyle BackColor="#CCCCCC" />
                                                                    <Columns>
                                                                        <asp:CommandField ButtonType="Button" HeaderText="Seleccionar" SelectText="Detalhe" ControlStyle-ForeColor="#8c2034" ControlStyle-Font-Bold="true" ControlStyle-CssClass="btn_form" ShowSelectButton="True" ControlStyle-BackColor="Transparent">
                                                                            <ControlStyle CssClass="btn_form" />
                                                                        </asp:CommandField>
                                                                        <asp:BoundField DataField="ID" HeaderText="ID" />
                                                                        <asp:BoundField DataField="From" HeaderText="From" />
                                                                        <asp:BoundField DataField="To" HeaderText="To" />
                                                                        <asp:BoundField DataField="Assunto" HeaderText="Assunto" />
                                                                        <asp:BoundField DataField="DataEmail" HeaderText="Data" />
                                                                        <asp:BoundField DataField="Tipo" HeaderText="Tipo" />
                                                                        <asp:BoundField DataField="UserName" HeaderText="Utilizador" />
                                                                    </Columns>
                                                                    <FooterStyle BackColor="#e6e7e8" />
                                                                    <HeaderStyle HorizontalAlign="Left" BackColor="Black" Font-Bold="True" ForeColor="White" />
                                                                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                                                    <SelectedRowStyle BackColor="#808080" Font-Bold="True" ForeColor="White" />
                                                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                                    <SortedAscendingHeaderStyle BackColor="#808080" />
                                                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                                    <SortedDescendingHeaderStyle BackColor="#383838" />
                                                                </asp:GridView>

                                                            </div>
                                                            <br />
                                                            <div id="panel_detalhe_email">

                                                                <br />

                                                                <span class="txt_Titulo">NOVO EMAIL</span><br />
                                                                <hr />

                                                                <asp:Panel ID="pnl_detalhe_email" runat="server" Visible="false">
                                                                    <email:Email_Detalhe ID="email_tarefa_detalhe" runat="server" />
                                                                </asp:Panel>
                                                            </div>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </asp:View>
                                    <asp:View ID="view_novo" runat="server">
                                        <taskcreate:Task_Create ID="wuc_create_task" runat="server" />
                                    </asp:View>
                                </asp:MultiView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </asp:Panel>
                </div>

                <ajaxToolkit:CollapsiblePanelExtender ID="collpanel_lista" runat="server"
                    ExpandControlID="btn_lista" CollapseControlID="btn_lista" TargetControlID="panel_lista" SuppressPostBack="true" />
                <button id="btn_lista" class="accordion">LISTA DE TICKETs</button>
                <asp:Panel ID="panel_lista" runat="server">
                    <div id="div_filtro">

                        <br />

                        <table style="width: 100%;">
                            <tr>
                                <td class="corpo_item">
                                    <asp:TextBox ID="txtbox_id" runat="server" CssClass="txtInput" TextMode="Number" placeholder="ID"></asp:TextBox>
                                    <br />
                                    <asp:TextBox ID="txtbox_descricao" runat="server" CssClass="txtInput" placeholder="Descricao" Rows="3" Columns="1"></asp:TextBox>
                                </td>

                                <td>
                                    <asp:TextBox ID="txtbox_dataregisto_inicio" runat="server" CssClass="txtInput" placeholder="Data Registo Inicio"></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="cal_dataregisto_inicio" runat="server" Format="yyyy-MM-dd" TargetControlID="txtbox_dataregisto_inicio" />
                                    <br />
                                    <asp:TextBox ID="txtbox_dataregisto_fim" runat="server" CssClass="txtInput" placeholder="Data Registo Fim"></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="cal_dataregisto_fim" runat="server" Format="yyyy-MM-dd" TargetControlID="txtbox_dataregisto_fim" />
                                </td>

                                <td>
                                    <asp:TextBox ID="txtbox_datafecho_inicio" runat="server" CssClass="txtInput" placeholder="Data Fecho Inicio"></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="cal_datafecho_inicio" runat="server" Format="yyyy-MM-dd" TargetControlID="txtbox_datafecho_inicio" />
                                    <br />
                                    <asp:TextBox ID="txtbox_datafecho_fim" runat="server" CssClass="txtInput" placeholder="Data Fecho Fim"></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="cal_datafecho_fim" runat="server" Format="yyyy-MM-dd" TargetControlID="txtbox_datafecho_fim" />
                                </td>

                                <td>
                                    <%--<asp:ImageButton ID="" runat="server" CssClass="img_bt" ImageUrl="~/images/search_01.png" />
                                    <asp:ImageButton ID="" runat="server" CssClass="img_bt" ImageUrl="~/images/close_01.png" title="Limpa Busca" />--%>

                                    <asp:Button ID="btn_filtro" CssClass="btn_form" runat="server" title="Aplicar filtro" Text="Filtrar" OnClick="btn_filtro_Click" />
                                    <asp:Button ID="btn_clearfiltro" CssClass="btn_form" runat="server" title="limpar campos de filtro" Text="Limpar" OnClick="btn_clearfiltro_Click" />

                                </td>
                            </tr>

                            <tr>
                                <td class="corpo_item">
                                    <br />
                                    <asp:TextBox ID="txtbox_username" runat="server" CssClass="txtInput" placeholder="Login"></asp:TextBox>
                                </td>

                                <td>
                                    <br />
                                    <asp:TextBox ID="txtbox_nome_user" runat="server" CssClass="txtInput" placeholder="Nome de Usuário" Rows="3" Columns="1"></asp:TextBox>
                                </td>

                                <td>
                                    <label class="head_detalhe" for="dropbox_estado">
                                        Canal de Contacto</label><br />
                                    <asp:DropDownList ID="dropbox_canal_contacto" runat="server" CssClass="selectInput">
                                        <asp:ListItem Selected="false" Text="Canal de Contacto" Value="-1"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>

                                <td>
                                    <%--<asp:ImageButton ID="" runat="server" CssClass="img_bt" Style="/*margin-left: 100px; */" ImageUrl="~/images/adicionar_01.png" title="Novo Ticket" />--%>
                                    <br />
                                    <asp:Button ID="btn_nova_task" CssClass="btn_form" runat="server" title="Novo Ticket" Text="Novo Ticket" OnClick="btn_nova_task_Click" />

                                </td>
                            </tr>

                            <tr>
                                <td class="corpo_item">
                                    <%--                              <label class="head_detalhe" for="dropbox_estado">
                                    Assunto da Tipificação</label><br />
                                <asp:DropDownList ID="dropbox_tipificacao" runat="server" CssClass="selectInput" OnSelectedIndexChanged="dropbox_tipificacao_SelectedIndexChanged" AutoPostBack="true" >
                                    <asp:ListItem Selected="false" Text="Assunto da Tipificação" Value="-1"></asp:ListItem>
                                </asp:DropDownList>--%>  
                                </td>

                                <td>
                                    <%--                                <label class="head_detalhe" for="dropbox_estado">
                                    Sub-assunto da Tipificação</label><br />
                                <asp:DropDownList ID="dropbox_sub_tipificacao" runat="server" CssClass="selectInput">
                                    <asp:ListItem Selected="false" Text="Sub-assunto da Tipificação" Value="-1"></asp:ListItem>
                                </asp:DropDownList>   --%>
                                    <%--<asp:TextBox ID="txtbox_assunto" runat="server" CssClass="txtInput" placeholder="Assunto"></asp:TextBox>--%>
                                </td>
                                <td>
                                    <%--                                    <asp:CheckBox ID="radiobtn_filtro_cliente" runat="server" Text="Cliente" CssClass="head_detalhe" />--%>
                          

                                </td>

                                <td></td>
                            </tr>

                            <tr>
                                <td>
                                    <label class="head_detalhe" for="dropbox_estado">
                                        Estado</label><br />
                                    <asp:DropDownList ID="dropbox_estado" runat="server" CssClass="selectInput">
                                        <asp:ListItem Selected="false" Text="Estado" Value="-1"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>

                                <td class="corpo_item">
                                    <label for="dropbox_tipo" class="head_detalhe">Tipo de Tarefa</label><br />
                                    <asp:DropDownList ID="dropbox_tipo" CssClass="selectInput" runat="server">
                                        <asp:ListItem Selected="false" Text="Tipo Tarefa" Value="-1"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>

                                <td class="corpo_item">
                                    <label for="dropbox_departamento" class="head_detalhe">Departamento</label><br />
                                    <asp:DropDownList ID="dropbox_departamento" CssClass="selectInput" runat="server">
                                        <asp:ListItem Selected="false" Text="Departamento" Value="-1"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>

                                <td>
                                    <asp:CheckBox ID="radiobtn_filtro_cliente" runat="server" Text="Cliente" CssClass="head_detalhe" />
                                    <div id="container_task_count" align="center">

                                        <asp:Label runat="server" ID="literal_count"></asp:Label>

                                    </div>
                                </td>

                            </tr>

                        </table>
                        <br />

                        <div id="content_gridview_tarefas" align="center">
                            <asp:GridView ID="gridview_tarefas" runat="server" AllowSorting="True" AutoGenerateColumns="False" Width="100%"
                                OnSorting="gridview_tarefas_Sorting" OnSelectedIndexChanged="gridview_tarefas_SelectedIndexChanged" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="0px" CellPadding="3" ForeColor="Black" GridLines="Horizontal">

                                <FooterStyle BackColor="#e6e7e8" />
                                <HeaderStyle HorizontalAlign="Left" CssClass="head_lista" BackColor="#58595b" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                <RowStyle />
                                <SelectedRowStyle CssClass="selected_row" BackColor="#808080" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="#e6e7e8" />
                                <Columns>
                                    <asp:CommandField ButtonType="Button" HeaderText="Seleccionar" SelectText="Detalhe" ControlStyle-ForeColor="#8c2034" ControlStyle-Font-Bold="true" ControlStyle-CssClass="btn_form" ShowSelectButton="True" ControlStyle-BackColor="Transparent">
                                        <ControlStyle CssClass="btn_form" />
                                    </asp:CommandField>
                                    <%--<asp:ImageButton ID="ImageButton1" ImageUrl="~/images/search_01.png" ButtonType="Image" CssClass="img_bt" ShowSelectButton="True" runat="server" />--%>
                                    <asp:BoundField DataField="ID" HeaderText="ID" SortExpression="id" />
                                    <asp:BoundField DataField="Descricao" HeaderText="Descrição" SortExpression="Descricao" />
                                    <asp:BoundField DataField="Tipo" HeaderText="Tipo Tarefa" SortExpression="Tipo" />
                                    <asp:BoundField HeaderStyle-Width="70px" DataField="DataRegisto" HeaderText="Data de Registo" SortExpression="DataRegisto" />
                                    <asp:BoundField DataField="DataFecho" HeaderText="Data de Fecho" SortExpression="DataFecho" />
                                    <asp:BoundField DataField="Estado" HeaderText="Estado" SortExpression="Estado" />
                                    <asp:BoundField DataField="Canal_Contacto" HeaderText="Canal de Contacto" SortExpression="Canal_Contacto" />
                                    <asp:BoundField DataField="Departamento" HeaderText="Departamento" SortExpression="Departamento" />
                                    <asp:BoundField DataField="Tipo" HeaderText="Tipo" SortExpression="Tipo" />
                                    <asp:BoundField DataField="Assunto" HeaderText="Assunto da Tipificação" SortExpression="Assunto" />
                                    <asp:BoundField DataField="SLAHoras" HeaderText="SLA Horas" SortExpression="SLAHoras" />
                                    <asp:BoundField DataField="diferenca_SLA" HeaderText="SLA To" SortExpression="diferenca_SLA" />
                                    <asp:BoundField DataField="Usuario" HeaderText="Username" SortExpression="Usuario" />
                                    <asp:BoundField DataField="Feito_por" HeaderText="Nome" SortExpression="Feito_por" />
                                </Columns>
                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#808080" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#383838" />


                            </asp:GridView>
                        </div>

                    </div>


                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>


    </div>

</body>
</html>
