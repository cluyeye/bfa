﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace uTools.Controls
{
    public partial class Estatistica_Manager : System.Web.UI.UserControl
    {

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                Populate_Estatisticas();

            }
        }


        private void Populate_Estatisticas()
        {

            string sqlstr = string.Empty;
            string sqlstr_from = string.Empty;
            string sqlstr_add = string.Empty;

            sqlstr_add = " OR tb_users.Name IS NOT NULL ORDER BY tb_users.Name ASC";

            sqlstr_from = " FROM uAuth.[dbo].[AspNetUsers] tb_users LEFT JOIN (SELECT UserID, COUNT(*) tickets FROM [dbo].[Tarefa_Accao] WHERE EstadoID = 5 GROUP BY UserID) tb_atribuido ON tb_users.id = tb_atribuido.UserID LEFT JOIN (SELECT UserID, COUNT(*) tickets FROM [dbo].[Tarefa_Accao] WHERE EstadoID = 1 GROUP BY UserID) tb_abertos ON tb_users.id = tb_abertos.UserID LEFT JOIN (SELECT UserID, COUNT(*) tickets FROM [dbo].[Tarefa_Accao] WHERE EstadoID = 3 GROUP BY UserID) tb_fechados ON tb_users.id = tb_fechados.UserID where tb_atribuido.tickets is not null OR tb_abertos.tickets is not null OR tb_fechados.tickets is not null";

            Populate_Tasks_Count("Listado(s)", sqlstr_from);

            sqlstr_from = sqlstr_from + sqlstr_add;

            sqlstr = "SELECT tb_users.Name Nome, tb_users.UserName, tb_atribuido.tickets Atribuidos, tb_abertos.tickets Abertos , tb_fechados.tickets Fechados" + sqlstr_from;


            Tools.Carregar_GridView("uTasks_DB", sqlstr, gridview_tickets);

        }

        private void Populate_Tasks_Count(String label, String filtro_count)
        {
            String query_count = "SELECT COUNT(tb_users.Name) as Count ";

            literal_count.Text = Tools.Carregar_Task_Count("uTasks_DB", query_count + "" + filtro_count) + "  " + label;
        }

        private void Populate_filtrar_dados()
        {

            string db = "uTasks_DB";
            string where_user = "";
            string where_date = "";
            string sqlstr = string.Empty;
            string sqlstr_from = string.Empty;
            string sqlstr_add = string.Empty;


            if (txt_name_user.Text.Length > 0) where_user = " AND tb_users.Name like '%" + txt_name_user.Text + "%'";

            if (txt_username.Text.Length > 0) where_user = " AND tb_users.UserName like '%" + txt_username.Text + "%'";

            if (data_find_parent_1.Text.Length > 0)
            {
                if (data_find_parent_2.Text.Length > 0)
                {
                    where_date = where_date + " AND DataRegisto Between '" + data_find_parent_1.Text + "' AND '" + data_find_parent_2.Text + "'";

                }
            }

            sqlstr_add = " OR tb_users.Name IS NOT NULL ORDER BY tb_users.Name ASC";
            
            sqlstr_from = " FROM uAuth.[dbo].[AspNetUsers] tb_users LEFT JOIN (SELECT UserID, COUNT(*) tickets FROM [dbo].[Tarefa_Accao] WHERE EstadoID = 5" + where_date + " GROUP BY UserID) tb_atribuido ON tb_users.id=tb_atribuido.UserID LEFT JOIN (SELECT UserID, COUNT(*) tickets FROM [dbo].[Tarefa_Accao] WHERE EstadoID = 1" + where_date + " GROUP BY UserID) tb_abertos ON tb_users.id=tb_abertos.UserID LEFT JOIN (SELECT UserID, COUNT(*) tickets FROM [dbo].[Tarefa_Accao] WHERE EstadoID = 3" + where_date + " GROUP BY UserID) tb_fechados ON tb_users.id=tb_fechados.UserID WHERE (tb_atribuido.tickets is not null OR tb_abertos.tickets is not null OR tb_fechados.tickets is not null)" + where_user;

            Populate_Tasks_Count("Listado(s)", sqlstr_from);

            //sqlstr_from = " FROM uAuth.[dbo].[AspNetUsers] tb_users LEFT JOIN (SELECT UserID, COUNT(*) tickets FROM [dbo].[Tarefa_Accao] WHERE EstadoID = 5" + where_date + " GROUP BY UserID) tb_atribuido ON tb_users.id=tb_atribuido.UserID LEFT JOIN (SELECT UserID, COUNT(*) tickets FROM [dbo].[Tarefa_Accao] WHERE EstadoID = 1" + where_date + " GROUP BY UserID) tb_abertos ON tb_users.id=tb_abertos.UserID LEFT JOIN (SELECT UserID, COUNT(*) tickets FROM [dbo].[Tarefa_Accao] WHERE EstadoID = 3" + where_date + " GROUP BY UserID) tb_fechados ON tb_users.id=tb_fechados.UserID WHERE (tb_atribuido.tickets is not null OR tb_abertos.tickets is not null OR tb_fechados.tickets is not null" + sqlstr_add + ")" + where_user;

            //sqlstr_from = sqlstr_from + sqlstr_add;


            sqlstr = "SELECT tb_users.Name Nome, tb_users.UserName, tb_atribuido.tickets Atribuidos, tb_abertos.tickets Abertos , tb_fechados.tickets Fechados" + sqlstr_from;

            Tools.Carregar_GridView(db, sqlstr, gridview_tickets);

        }

        protected void btn_filtrar_Click(object sender, ImageClickEventArgs e)
        {
            System.Threading.Thread.Sleep(1000);

            Populate_filtrar_dados();

        }

        protected void gridview_agente_tickets_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Update_Detalhe_Parent(gridview_tickets.Rows[gridview_tickets.SelectedIndex].Cells[1].Text);

            int atribuidos = valor_armazenado(gridview_tickets.Rows[gridview_tickets.SelectedIndex].Cells[3].Text);
            int abertos = valor_armazenado(gridview_tickets.Rows[gridview_tickets.SelectedIndex].Cells[4].Text);
            int fechados = valor_armazenado(gridview_tickets.Rows[gridview_tickets.SelectedIndex].Cells[5].Text);
            int total = (atribuidos + abertos + fechados);

            lbl_nome_detail.Text = "" + gridview_tickets.Rows[gridview_tickets.SelectedIndex].Cells[1].Text;
            lbl_username_detail.Text = "" + gridview_tickets.Rows[gridview_tickets.SelectedIndex].Cells[2].Text;
            lbl_atribuido_detail.Text = "" + atribuidos;
            lbl_aberto_detail.Text = "" + abertos;
            lbl_fechado_detail.Text = "" + fechados;
            lbl_total_detail.Text = "" + total;

        }

        private int valor_armazenado(string valor)
        {
            if (valor == "&nbsp;")
                return 0;
            return Convert.ToInt32(valor);
        }


        private void Update_Detalhe_Parent(string user)
        {

            string db = "uTasks_DB";
            string sqlstr = "SELECT tb_users.UserName, tb_atribuido.tickets total, tb_abertos.tickets abertos , tb_fechados.tickets fechados FROM"
                            + " uAuth.[dbo].[AspNetUsers] tb_users"
                            + " LEFT JOIN (SELECT UserID, COUNT(*) tickets FROM [dbo].[Tarefa_Accao] WHERE DataRegisto Between '" + data_find_parent_1.Text + "' AND '" + data_find_parent_2.Text + "' AND EstadoID=5 GROUP BY UserID) tb_atribuido ON tb_users.id=tb_atribuido.UserID"
                            + " LEFT JOIN (SELECT UserID, COUNT(*) tickets FROM [dbo].[Tarefa_Accao] WHERE DataRegisto Between '" + data_find_parent_1.Text + "' AND '" + data_find_parent_2.Text + "' AND EstadoID=1 GROUP BY UserID) tb_abertos ON tb_users.id=tb_abertos.UserID"
                            + " LEFT JOIN (SELECT UserID, COUNT(*) tickets FROM [dbo].[Tarefa_Accao] WHERE DataRegisto Between '" + data_find_parent_1.Text + "' AND '" + data_find_parent_2.Text + "' AND EstadoID=3 GROUP BY UserID) tb_fechados ON tb_users.id=tb_fechados.UserID"
                            + " WHERE tb_atribuido.tickets is not null OR tb_abertos.tickets is not null OR tb_fechados.tickets is not null AND UserID";



        }

        protected void btn_limpar_filtro_Click(object sender, ImageClickEventArgs e)
        {
            data_find_parent_1.Text = string.Empty;
            data_find_parent_2.Text = string.Empty;
            txt_username.Text = string.Empty;
            txt_name_user.Text = string.Empty;

            lbl_nome_detail.Text = string.Empty;
            lbl_username_detail.Text = string.Empty;
            lbl_atribuido_detail.Text = string.Empty;
            lbl_aberto_detail.Text = string.Empty;
            lbl_fechado_detail.Text = string.Empty;
            lbl_total_detail.Text = string.Empty;

            Populate_Estatisticas();
        }

    
    }
}