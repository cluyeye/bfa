﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="uKnowledgeView.ascx.cs" Inherits="uTools.Controls.uKnowledgeView" %>

<!DOCTYPE html>

<html>
<head>


    <title>uTasks</title>

    
    <%--<link rel="stylesheet" type="text/css" href="../css/style_modal.css" />--%>
</head>
<body>


    <div class="container">

        <nav id="content_treeview">

<%--
                <div align="center">
                <asp:Panel ID="pnl_btns_accao" runat="server">
                    <asp:ImageButton ID="btn_editar" runat="server" ImageUrl="../images/edit_01.png" CssClass="img_bt" />
                    <asp:ImageButton ID="btn_adicionar" runat="server" ImageUrl="../images/adicionar_01.png" CssClass="img_bt" />
                    <asp:ImageButton ID="btn_persquisar" runat="server" ImageUrl="../images/search_01.png" CssClass="img_bt" />
                </asp:Panel>
            </div>--%>

            <%--<hr />--%>

            <asp:TreeView ID="TreeView1" runat="server" Target="iframe_link" OnTreeNodePopulate="menu_TreeNodePopulate" ImageSet="Simple">
                <HoverNodeStyle Font-Underline="True" ForeColor="#5555DD" />
                <Nodes>
                    <asp:TreeNode PopulateOnDemand="True" Text="CONTEÚDOS" Value="-1"></asp:TreeNode>
                </Nodes>
                <NodeStyle Font-Names="ephismere" Font-Size="8pt" ForeColor="#58595b" HorizontalPadding="0px" NodeSpacing="0px" VerticalPadding="0px" />
                <ParentNodeStyle Font-Bold="False" />
                <SelectedNodeStyle Font-Underline="True" HorizontalPadding="0px" VerticalPadding="0px" ForeColor="#5555DD" />
            </asp:TreeView>

        </nav>


        <div id="content_kb">

            <iframe id="iframe_link" src="Slider_View.aspx" name="iframe_link">

            </iframe>

        </div>

    </div>

</body>
</html>
