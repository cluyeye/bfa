﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="uTasksView_script.ascx.cs" Inherits="uTools.Controls.uTasksView_script" %>


<%@ Register Src="~/Controls/email_enviar.ascx" TagName="Email_Detalhe" TagPrefix="email" %>
<%@ Register Src="~/Controls/uTaskCreate_script.ascx" TagName="Task_Create" TagPrefix="taskcreate" %>


<!DOCTYPE html>

<html>
<head>
    <title>uTasks</title>


    <style>
        @charset 'utf-8';


        @font-face {
            font-family: ephismere;
            src: url('~/fontes/Styling W00 Regular.ttf');
        }


        body, button
        {
            background-color: #e6e7e8;
            font-family: 'ephismere';
            font-size: 12px;
            color: #58595b;
            max-height: none;
        }

        .txt_Titulo
        {
            color: #58595b;
            font-family: 'ephismere';
            font-weight: bold;

            cursor: pointer;
            padding: 5px;
            width: 100%;
            border: none;
            text-align: left;
            outline: none;
            font-size: 14px;
            transition: 0.4s;
        }

    </style>


</head>
<body>

<asp:ScriptManager ID="ScriptManagerTarefas" runat="server">
</asp:ScriptManager>
<div id="corpo_tasks">

    <asp:UpdatePanel ID="updpanel_lista" runat="server">
        <ContentTemplate>

            <ajaxToolkit:CollapsiblePanelExtender ID="collpanel_Detalhe" runat="server"
                ExpandControlID="btn_detalhe" CollapseControlID="btn_detalhe" TargetControlID="panel_detalhe" SuppressPostBack="true" />
            <button id="btn_detalhe" title="" class="accordion">
                <asp:Label ID="lxt_head_main_tasks" runat="server"></asp:Label>
            </button>


            <div id="detalhe_principal_Tasks_all">



                <asp:Panel ID="panel_detalhe" runat="server">
                    <asp:UpdatePanel ID="updpanel_detalhe" runat="server" class="updatepanel">
                        <ContentTemplate>

                            <asp:MultiView ID="mv_tarefa" runat="server" ActiveViewIndex="0" Visible="false">
                                <asp:View ID="view_detalhe" runat="server">
                                    <div id="content_accao">
                                        <asp:UpdatePanel ID="pnlupd_accao" runat="server">
                                            <ContentTemplate>
                                                <br />
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:DropDownList ID="dropbox_accao" CssClass="selectInput" runat="server" OnSelectedIndexChanged="dropbox_accao_SelectedIndexChanged" AutoPostBack="true">
                                                                <asp:ListItem Value="1">ATRIBUIR</asp:ListItem>
                                                                <asp:ListItem Value="2">ALTERAR ESTADO</asp:ListItem>
                                                                <asp:ListItem Value="3">ALTERAR TIPO</asp:ListItem>
                                                                <asp:ListItem Value="4">ADICIONAR COMENTÁRIO</asp:ListItem>
                                                                <asp:ListItem Value="5">GERIR CONTEÚDOS</asp:ListItem>
                                                                <asp:ListItem Value="6">ENVIAR TICKET</asp:ListItem>
                                                            </asp:DropDownList>

                                                        </td>

                                                        <td>
                                                            <asp:ImageButton ID="btn_accao_ok" runat="server" CssClass="img_bt" ImageUrl="~/images/ok.png" title="Filtar Busca" />
                                                        </td>
                                                    </tr>

                                                </table>

                                                <ajaxToolkit:ModalPopupExtender ID="modal_accao" runat="server"
                                                    TargetControlID="btn_accao_ok" PopupControlID="pnl_accao" CancelControlID="btn_accao_cancel"
                                                    BackgroundCssClass="fundo_modal" DropShadow="true"
                                                    OnPreRender="dropbox_accao_SelectedIndexChanged">
                                                </ajaxToolkit:ModalPopupExtender>

                                                <asp:Panel ID="pnl_accao" runat="server" CssClass="modal" align="center">
                                                    <asp:MultiView ID="mv_accao" runat="server" ActiveViewIndex="3">
                                                        <asp:View ID="view_dropbox" runat="server">
                                                            <div class="content_modal">
                                                                <span class="txt_Titulo">ALTERAÇÃO</span><br />
                                                                <hr class="space_bottom" />
                                                                <asp:DropDownList ID="dropbox_alterar" CssClass="selectInput" runat="server"></asp:DropDownList>
                                                            </div>
                                                        </asp:View>
                                                        <asp:View ID="view_comentario" runat="server">
                                                            <div class="content_modal">
                                                                <span class="txt_Titulo">ADIÇÃO DE COMENTÁRIO</span><br />
                                                                <hr class="space_bottom" />
                                                                <label for="txtbox_editar_comentario" class="head_detalhe">Comentário</label><br />
                                                                <asp:TextBox ID="txtbox_editar_comentario" runat="server" TextMode="MultiLine" Rows="6" Width="99%" Columns="70" CssClass="editdescricao"></asp:TextBox>
                                                            </div>
                                                        </asp:View>
                                                        <asp:View ID="view_descricao" runat="server">
                                                            <div class="content_modal">
                                                                <span class="txt_Titulo">GESTÃO DE CONTEÚDOS</span><br />
                                                                <hr class="space_bottom" />
                                                                <label for="txtbox_editar_descricao" class="head_detalhe">Descricao</label><br />
                                                                <asp:TextBox ID="txtbox_editar_descricao" runat="server" TextMode="MultiLine" Rows="6" Width="99%" Columns="70" CssClass="editdescricao"></asp:TextBox>
                                                                <br />
                                                                <label for="txtbox_editar_resumo" class="head_detalhe">Resumo</label><br />
                                                                <asp:TextBox ID="txtbox_editar_resumo" runat="server" TextMode="MultiLine" Rows="6" Width="99%" Columns="70" CssClass="editdescricao"></asp:TextBox>
                                                                <br />
                                                                <label for="txtbox_editar_resolucao" class="head_detalhe">Resolução</label><br />
                                                                <asp:TextBox ID="txtbox_editar_resolucao" runat="server" TextMode="MultiLine" Rows="6" Width="99%" Columns="70" CssClass="editdescricao"></asp:TextBox>
                                                                <br />

                                                            </div>
                                                        </asp:View>
                                                        <asp:View ID="view_email_send" runat="server">
                                                            <div id="panel_detalhe_bottom">
                                                            <%--<div class="content_modal">--%>
                                                                <br />
                                                                <span class="txt_Titulo">ENVIAR INFO POR EMAIL</span><br />
                                                                <hr class="space_bottom" />
                                                                <email:Email_Detalhe ID="email_enviar_info" runat="server" />
                                                                <br />
                                                            </div>
                                                        </asp:View>
                                                    </asp:MultiView>
                                                    <div id="content_modal_bt" align="justify">
                                                        <asp:ImageButton ID="btn_accao_confirm" runat="server" CssClass="img_bt" ImageUrl="~/images/ok.png" title="Confirmar" OnClick="btn_accao_confirm_Click" />
                                                        <asp:ImageButton ID="btn_accao_cancel" runat="server" CssClass="img_bt" ImageUrl="~/images/close_01.png" title="Cancelar" />
                                                    </div>
                                                </asp:Panel>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="btn_accao_confirm" EventName="Click" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </div>
                                    <asp:UpdatePanel ID="updpnl_detalhe_tarefa" runat="server">
                                        <ContentTemplate>
                                            <div id="panel_detalhe_section">
                                                <div id="detalhe_principal_Tasks">
                                                        <table style="width: 100%;">
                                                            <tr>
                                                                <td class="corpo_item">
                                                                    <label for="lbl_ID" class="head_detalhe">ID Tarefa</label><br />
                                                                    <asp:Label ID="lbl_ID" runat="server"></asp:Label>

                                                                </td>

                                                                <td class="corpo_item" colspan="2">
                                                                    <label for="lbl_tipificacao" class="head_detalhe">Tipificação</label><br />
                                                                    <asp:Label ID="lbl_tipificacao" runat="server"></asp:Label>
                                                                </td>

                                                                <td class="corpo_item">
                                                                    <label for="lbl_tipo" class="head_detalhe">Tipo</label><br />
                                                                    <asp:Label ID="lbl_tipo" runat="server"></asp:Label>


                                                                </td>

                                                            </tr>


                                                            <tr>

                                                                <td>
                                                                    <label for="lbl_canal_contacto" class="head_detalhe">ID Canal</label><br />
                                                                    <asp:Label ID="lbl_canal_contacto_id" runat="server"></asp:Label>

                                                                    <br />

                                                                    <label for="lbl_canal_contacto_id" class="head_detalhe">Descrição Canal</label><br />
                                                                    <asp:Label ID="lbl_canal_contacto" runat="server"></asp:Label><br />
                                                                </td>

                                                                <td>
                                                                    <label for="lbl_AspUser" class="head_detalhe">Usuário</label><br />
                                                                    <asp:Label ID="lbl_AspUser" runat="server"></asp:Label>

                                                                    <br />

                                                                    <label for="lbl_feito_por" class="head_detalhe">Nome de Usuário</label><br />
                                                                    <asp:Label ID="lbl_feito_por" runat="server"></asp:Label>

                                                                </td>

                                                                <td>
                                                                    <label for="lbl_cliente" class="head_detalhe">ID Cliente</label><br />
                                                                    <asp:Label ID="lbl_cliente" runat="server"></asp:Label>
                                                                    <br />

                                                                    <label for="lbl_feito_por" class="head_detalhe">Nome do Cliente</label><br />
                                                                    <asp:Label ID="lbl_cliente_nome" runat="server"></asp:Label>

                                                                </td>

                                                                <td>
                                                                    <label for="lbl_diferenca_SLA" class="head_detalhe">Diferença SLA</label><br />
                                                                    <asp:Label ID="lbl_diferenca_SLA" runat="server"></asp:Label>

                                                                    <br />

                                                                    <label for="lbl_sla" class="head_detalhe">SLA</label><br />
                                                                    <asp:Label ID="lbl_sla" runat="server"></asp:Label>
                                                                </td>


                                                            </tr>



                                                            <tr>

                                                                                          <br />                                  <td>
                                                                    <label for="lbl_detalhe_departamento" class="head_detalhe">Departamento</label><br />
                                                                    <asp:Label ID="lbl_detalhe_departamento" runat="server"></asp:Label>
                                                                    <asp:HiddenField ID="hf_departamentoID" runat="server" />
                                                                </td>

                                                                <td>
                                                                    <label for="lbl_detalhe_estado" class="head_detalhe">Estado</label><br />
                                                                    <asp:Label ID="lbl_detalhe_estado" runat="server"></asp:Label>
                                                                    <asp:HiddenField ID="hf_estadoID" runat="server" />

                                                                    <br />

                                                                </td>

                                                                <td>
                                                                    <label for="lbl_registo" class="head_detalhe">Registo</label><br />
                                                                    <asp:Label ID="lbl_registo" runat="server"></asp:Label>
                                                                </td>

                                                                <td>
                                                                    <label for="lbl_fecho" class="head_detalhe">Fecho</label><br />
                                                                    <asp:Label ID="lbl_fecho" runat="server"></asp:Label>

                                                                </td>
                                                            </tr>

                                                        </table>

                                                        <br />

                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <label for="lbl_descricao" class="head_detalhe">Descrição</label><br />
                                                                    <asp:Label ID="lbl_descricao" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td>
                                                                    <label for="lbl_resumo" class="head_detalhe">Resumo</label><br />
                                                                    <asp:Label ID="lbl_resumo" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <label for="lbl_resolucao" class="head_detalhe">Resolução</label><br />
                                                                    <asp:Label ID="lbl_resolucao" runat="server"></asp:Label>
                                                                </td>

                                                            </tr>
                                                        </table>
                                                    </div>
                                                <div id="detalhe_lateral_Tasks">
                                                    <span class="txt_Titulo">LISTA DE HISTÓRICO</span><br />
                                                    <hr />
                                                    <div id="tasks_detalhe_historico">
                                                        <asp:GridView ID="gridview_historico" runat="server" AutoGenerateColumns="False" BackColor="White" Width="100%" BorderColor="#999999" BorderStyle="Solid" BorderWidth="0px" CellPadding="3" ForeColor="Black" GridLines="Horizontal">
                                                            <FooterStyle BackColor="#e6e7e8" />
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="head_lista" BackColor="#58595b" Font-Bold="True" ForeColor="White" />
                                                            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                                            <RowStyle />
                                                            <SelectedRowStyle CssClass="selected_row" BackColor="#808080" Font-Bold="True" ForeColor="White" />
                                                            <AlternatingRowStyle BackColor="#e6e7e8" />
                                                            <Columns>
                                                                <asp:BoundField DataField="DataRegisto" HeaderText="Data" />
                                                                <asp:BoundField DataField="Departamento" HeaderText="Departamento" />
                                                                <asp:BoundField DataField="Estado" HeaderText="Estado" />
                                                                <asp:BoundField DataField="UserName" HeaderText="Utilizador" />
                                                            </Columns>
                                                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                            <SortedAscendingHeaderStyle BackColor="#808080" />
                                                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                            <SortedDescendingHeaderStyle BackColor="#383838" />
                                                        </asp:GridView>
                                                    </div>
                                                    <br />
                                                    <span class="txt_Titulo">LISTA DE COMENTÁRIOS</span><br />
                                                    <hr />
                                                    <div id="tasks_detalhe_comentario">
                                                        <asp:GridView ID="gridview_comentario" runat="server" AutoGenerateColumns="False" Width="100%" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="0px" CellPadding="3" ForeColor="Black" GridLines="Horizontal">
                                                            <FooterStyle BackColor="#e6e7e8" />
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="head_lista" BackColor="#58595b" Font-Bold="True" ForeColor="White" />
                                                            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                                            <RowStyle />
                                                            <SelectedRowStyle CssClass="selected_row" BackColor="#808080" Font-Bold="True" ForeColor="White" />
                                                            <AlternatingRowStyle BackColor="#e6e7e8" />
                                                            <Columns>
                                                                <asp:BoundField DataField="DataRegisto" HeaderText="Data" />
                                                                <asp:BoundField DataField="Comentario" HeaderText="Comentário" />
                                                                <asp:BoundField DataField="UserName" HeaderText="Utilizador" />
                                                            </Columns>
                                                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                            <SortedAscendingHeaderStyle BackColor="#808080" />
                                                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                            <SortedDescendingHeaderStyle BackColor="#383838" />
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                                <asp:UpdatePanel ID="updpnl_gestao_emails" runat="server">
                                                    <ContentTemplate>
                                                        <span class="txt_Titulo">INTERAÇÕES POR EMAILs</span><br />
                                                        <hr />
                                                        <div id="detalhe_email_bottom">

                                                            <asp:GridView ID="gridview_email" runat="server" Width="99%" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px"
                                                                CellPadding="3" ForeColor="Black" GridLines="Horizontal" OnSelectedIndexChanged="gridview_email_SelectedIndexChanged" AutoGenerateColumns="false">
                                                                <AlternatingRowStyle BackColor="#CCCCCC" />
                                                                <Columns>
                                                                    <asp:CommandField ButtonType="Image" ControlStyle-CssClass="img_bt" SelectImageUrl="~/images/plus_button_01.png" ShowSelectButton="True" ControlStyle-BackColor="Transparent">
                                                                        <ControlStyle CssClass="img_bt" Width="11px" />
                                                                    </asp:CommandField>
                                                                    <asp:BoundField DataField="ID" HeaderText="ID" />
                                                                    <asp:BoundField DataField="From" HeaderText="From" />
                                                                    <asp:BoundField DataField="To" HeaderText="To" />
                                                                    <asp:BoundField DataField="Assunto" HeaderText="Assunto" />
                                                                    <asp:BoundField DataField="DataEmail" HeaderText="Data" />
                                                                    <asp:BoundField DataField="Tipo" HeaderText="Tipo" />
                                                                    <asp:BoundField DataField="UserName" HeaderText="Utilizador" />
                                                                </Columns>
                                                                <FooterStyle BackColor="#e6e7e8" />
                                                                <HeaderStyle HorizontalAlign="Left" BackColor="Black" Font-Bold="True" ForeColor="White" />
                                                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                                                <SelectedRowStyle BackColor="#808080" Font-Bold="True" ForeColor="White" />
                                                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                                <SortedAscendingHeaderStyle BackColor="#808080" />
                                                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                                <SortedDescendingHeaderStyle BackColor="#383838" />
                                                            </asp:GridView>

                                                        </div>
                                                        <br />
                                                        <div id="panel_detalhe_email">
                                                            
                                                            <br />

                                                            <span class="txt_Titulo">NOVO EMAIL</span><br />
                                                            <hr />

                                                            <asp:Panel ID="pnl_detalhe_email" runat="server" Visible="false">
                                                                <email:Email_Detalhe ID="email_tarefa_detalhe" runat="server" />
                                                            </asp:Panel>
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </asp:View>
                                <asp:View ID="view_novo" runat="server">
                                    <taskcreate:Task_Create ID="wuc_create_task" runat="server" />
                                </asp:View>
                            </asp:MultiView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
            </div>

            <ajaxToolkit:CollapsiblePanelExtender ID="collpanel_lista" runat="server"
                ExpandControlID="btn_lista" CollapseControlID="btn_lista" TargetControlID="panel_lista" SuppressPostBack="true" />
            <button id="btn_lista" class="accordion">LISTA DE TICKETs</button>
            <asp:Panel ID="panel_lista" runat="server">
                <div id="div_filtro">

                    <br />

                    <table style="width: 100%;">
                            <tr>
                                <td class="corpo_item">
                                    <asp:TextBox ID="txtbox_id" runat="server" CssClass="txtInput" TextMode="Number" placeholder="ID"></asp:TextBox>
                                    <br />
                                    <asp:TextBox ID="txtbox_descricao" runat="server" CssClass="txtInput" placeholder="Descricao" Rows="3" Columns="1"></asp:TextBox>
                                </td>

                                <td>
                                    <asp:TextBox ID="txtbox_dataregisto_inicio" runat="server" CssClass="txtInput" placeholder="Data Registo Inicio"></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="cal_dataregisto_inicio" runat="server" Format="yyyy-MM-dd" TargetControlID="txtbox_dataregisto_inicio" />
                                    <br />
                                    <asp:TextBox ID="txtbox_dataregisto_fim" runat="server" CssClass="txtInput" placeholder="Data Registo Fim"></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="cal_dataregisto_fim" runat="server" Format="yyyy-MM-dd" TargetControlID="txtbox_dataregisto_fim" />
                                </td>

                                <td>
                                    <asp:TextBox ID="txtbox_datafecho_inicio" runat="server" CssClass="txtInput" placeholder="Data Fecho Inicio"></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="cal_datafecho_inicio" runat="server" Format="yyyy-MM-dd" TargetControlID="txtbox_datafecho_inicio" />
                                    <br />
                                    <asp:TextBox ID="txtbox_datafecho_fim" runat="server" CssClass="txtInput" placeholder="Data Fecho Fim"></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="cal_datafecho_fim" runat="server" Format="yyyy-MM-dd" TargetControlID="txtbox_datafecho_fim" />
                                </td>

                                <td>
                                    <asp:TextBox ID="txtbox_clientID" runat="server" CssClass="txtInput" placeholder="Cliente ID" Rows="3" Columns="1"></asp:TextBox>
                                    <br />
                                    <asp:ImageButton ID="btn_filtro" runat="server" CssClass="img_bt" ImageUrl="~/images/search_01.png" title="Filtar Busca" OnClick="btn_filtro_Click" />
                                    <asp:ImageButton ID="btn_clearfiltro" runat="server" CssClass="img_bt" ImageUrl="~/images/close_01.png" title="Limpa Busca" OnClick="btn_clearfiltro_Click" />

                                </td>
                            </tr>

                            <tr>
                                <td class="corpo_item">
                                    <br />
                                    <asp:TextBox ID="txtbox_username" runat="server" CssClass="txtInput" placeholder="Login"></asp:TextBox>
                                </td>

                                <td>
                                    <br />
                                    <asp:TextBox ID="txtbox_nome_user" runat="server" CssClass="txtInput" placeholder="Nome de Usuário" Rows="3" Columns="1"></asp:TextBox>
                                </td>

                                <td>
                                    <label class="head_detalhe" for="dropbox_estado">
                                        Canal de Contacto</label><br />
                                    <asp:DropDownList ID="dropbox_canal_contacto" runat="server" CssClass="selectInput">
                                        <asp:ListItem Selected="false" Text="Canal de Contacto" Value="-1"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>

                                <td>
                                    <br />
                                    <asp:ImageButton ID="btn_nova_task" runat="server" CssClass="img_bt" Style="/*margin-left: 100px;*/" ImageUrl="~/images/adicionar_01.png" title="Nova Tarefa" OnClick="btn_nova_task_Click" />

                                </td>
                            </tr>

                             <tr>
                            <td class="corpo_item">
  <%--                              <label class="head_detalhe" for="dropbox_estado">
                                    Assunto da Tipificação</label><br />
                                <asp:DropDownList ID="dropbox_tipificacao" runat="server" CssClass="selectInput" OnSelectedIndexChanged="dropbox_tipificacao_SelectedIndexChanged" AutoPostBack="true" >
                                    <asp:ListItem Selected="false" Text="Assunto da Tipificação" Value="-1"></asp:ListItem>
                                </asp:DropDownList>--%>  
                            </td>

                            <td>
<%--                                <label class="head_detalhe" for="dropbox_estado">
                                    Sub-assunto da Tipificação</label><br />
                                <asp:DropDownList ID="dropbox_sub_tipificacao" runat="server" CssClass="selectInput">
                                    <asp:ListItem Selected="false" Text="Sub-assunto da Tipificação" Value="-1"></asp:ListItem>
                                </asp:DropDownList>   --%>
                                                                <%--<asp:TextBox ID="txtbox_assunto" runat="server" CssClass="txtInput" placeholder="Assunto"></asp:TextBox>--%>
                            </td>
                            <td>
<%--                                    <asp:CheckBox ID="radiobtn_filtro_cliente" runat="server" Text="Cliente" CssClass="head_detalhe" />--%>
                          

                            </td>

                            <td>

                            </td>
                        </tr>

                            <tr>
                                <td>
                                    <label class="head_detalhe" for="dropbox_estado">
                                        Estado</label><br />
                                    <asp:DropDownList ID="dropbox_estado" runat="server" CssClass="selectInput">
                                        <asp:ListItem Selected="false" Text="Estado" Value="-1"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>

                                <td class="corpo_item">
                                    <label for="dropbox_tipo" class="head_detalhe">Tipo de Tarefa</label><br />
                                    <asp:DropDownList ID="dropbox_tipo" CssClass="selectInput" runat="server">
                                        <asp:ListItem Selected="false" Text="Tipo Tarefa" Value="-1"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>

                                <td class="corpo_item">
                                    <label for="dropbox_departamento" class="head_detalhe">Departamento</label><br />
                                    <asp:DropDownList ID="dropbox_departamento" CssClass="selectInput" runat="server">
                                        <asp:ListItem Selected="false" Text="Departamento" Value="-1"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>

                                <td>
                                    <asp:CheckBox ID="radiobtn_filtro_cliente" runat="server" Text="Cliente" CssClass="head_detalhe" />
                                    <div id="container_task_count" align="center">

                                        <asp:Label runat="server" ID="literal_count"></asp:Label>

                                    </div>
                                </td>

                            </tr>

                        </table>
                    <br />

                    <div id="content_gridview_tarefas" align="center">
                        <asp:GridView ID="gridview_tarefas" runat="server" AllowSorting="True" AutoGenerateColumns="False" Width="100%"
                            OnSorting="gridview_tarefas_Sorting" OnSelectedIndexChanged="gridview_tarefas_SelectedIndexChanged" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="0px" CellPadding="3" ForeColor="Black" GridLines="Horizontal">

                            <FooterStyle BackColor="#e6e7e8" />
                            <HeaderStyle HorizontalAlign="Left" CssClass="head_lista" BackColor="#58595b" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                            <RowStyle />
                            <SelectedRowStyle CssClass="selected_row" BackColor="#808080" Font-Bold="True" ForeColor="White" />
                            <AlternatingRowStyle BackColor="#e6e7e8" />
                            <Columns>
                                <asp:CommandField ButtonType="Image" ControlStyle-CssClass="img_bt" SelectImageUrl="~/images/plus_button_01.png" ShowSelectButton="True" ControlStyle-BackColor="Transparent">
                                    <ControlStyle CssClass="img_bt" Width="11px" />
                                </asp:CommandField>
                                <%--<asp:ImageButton ID="ImageButton1" ImageUrl="~/images/search_01.png" ButtonType="Image" CssClass="img_bt" ShowSelectButton="True" runat="server" />--%>
                                <asp:BoundField DataField="ID" HeaderText="ID" SortExpression="id" />
                                <asp:BoundField DataField="Tipo" HeaderText="Tipo Tarefa" SortExpression="Tipo" />
                                <asp:BoundField DataField="Descricao" HeaderText="Descrição" SortExpression="Descricao" />
                                <asp:BoundField DataField="DataRegisto" HeaderText="Data de Registo" SortExpression="DataRegisto" />
                                <asp:BoundField DataField="DataFecho" HeaderText="Data de Fecho" SortExpression="DataFecho" />
                                <asp:BoundField DataField="Departamento" HeaderText="Departamento" SortExpression="Departamento" />
                                <asp:BoundField DataField="Estado" HeaderText="Estado" SortExpression="Estado" />
                                <asp:BoundField DataField="diferenca_SLA" HeaderText="SLA To" SortExpression="diferenca_SLA" />
                            </Columns>
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#808080" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#383838" />


                        </asp:GridView>
                    </div>

                </div>


            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>


</div>



</body>
</html>