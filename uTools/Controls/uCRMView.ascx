﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="uCRMView.ascx.cs" Inherits="uTools.Controls.uCRMView" %>

<!DOCTYPE html>

<html>
<head>


    <title>uCRM</title>




</head>
<body>

    <%--<asp:ScriptManager ID="ScriptManagerCRM" runat="server">
</asp:ScriptManager>--%>

    <asp:UpdatePanel ID="updpnl_crm" runat="server">
        <ContentTemplate>

            <span class="txt_Titulo">uCRM</span><br />
            <hr />
            <div id="container_crm" align="center">

                <asp:Panel ID="pnl_detalhe_cliente" runat="server" Enabled="false">
                    <asp:HiddenField ID="hidden_cliente_id" runat="server" />
                    <table>
                        <tr>
                            <td>
                                <label class="lb_crm">ID : </label>
                                <asp:Label ID="lbl_cliente_id" runat="server" CssClass="lb_crm"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="" class="lb_crm">Tipo de Cliente</label><br />

                                <asp:DropDownList ID="dropbox_tipo_cliente" CssClass="selectInput" runat="server">
                                    <asp:ListItem Value="-1">NA</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label for="txtbox_nome" class="lb_crm">Nome</label><br />

                                <asp:TextBox ID="txtbox_nome" CssClass="txtInput" runat="server" OnTextChanged="txtbox_nome_TextChanged"></asp:TextBox>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label for="txtbox_email" class="lb_crm">Email</label><br />

                                <asp:TextBox ID="txtbox_email" CssClass="txtInput" TextMode="Email" runat="server" OnTextChanged="txtbox_email_TextChanged"></asp:TextBox>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label for="txtbox_telefone1" class="lb_crm">Telefone 1</label><br />

                                <asp:TextBox ID="txtbox_telefone1" CssClass="txtInput" TextMode="Number" runat="server"></asp:TextBox>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label for="txtbox_telefone2" class="lb_crm">Telefone 2</label><br />

                                <asp:TextBox ID="txtbox_telefone2" CssClass="txtInput" TextMode="Number" runat="server"></asp:TextBox>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label for="txtbox_numero_conta" class="lb_crm">Numero de Conta</label><br />

                                <asp:TextBox ID="txtbox_numero_conta" CssClass="txtInput" TextMode="Number" runat="server"></asp:TextBox>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label for="dropbox_agencia" class="lb_crm">Agência</label><br />

                                <asp:DropDownList ID="dropbox_agencia" CssClass="selectInput" TextMode="Number" runat="server">
                                    <asp:ListItem Value="-1">NA</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <div id="container_message">
                    <asp:Label ID="message_status" runat="server"></asp:Label>
                </div>

            </div>



            <br />

            <asp:Panel ID="pnl_btns_accao" runat="server">
                <asp:Button ID="btn_editar" CssClass="btn_form" runat="server" title="Editar Cliente" Text="Editar" OnClick="btn_editar_Click" />
                <asp:Button ID="btn_adicionar" CssClass="btn_form" runat="server" title="Novo Cliente" Text="Novo" OnClick="btn_criar_Click" />
                <asp:Button ID="btn_persquisar" CssClass="btn_form" runat="server" title="Filtrar Cliente" Text="Filtrar" OnClick="btn_persquisar_Click" />

            </asp:Panel>
            <asp:Panel ID="pnl_btns_ok" runat="server" Visible="false">
                <asp:Button ID="btn_ok" CssClass="btn_form" runat="server" title="Salvar Cliente" Text="     Ok     " OnClick="btn_ok_Click" />
                <asp:Button ID="btn_cancel" CssClass="btn_form" runat="server" title="Cancelar Cliente" Text="Cancelar" OnClick="btn_cancel_Click" />

            </asp:Panel>

            <span class="txt_Titulo">
                <asp:Label ID="lbl_gridview_interaccoes_clients" runat="server" for="gridview_interaccoes_clients"></asp:Label></span><br />
            <hr />

            <div id="container_interaccoes" align="center">

                <asp:GridView ID="gridview_interaccoes_clients" runat="server" Width="100%" AllowSorting="True" OnRowCommand="gridview_interaccoes_clients_RowCommand" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="0px" CellPadding="3" ForeColor="Black" GridLines="Horizontal">
                    <AlternatingRowStyle BackColor="#e6e7e8" />

                    <Columns>
                        <asp:CommandField ButtonType="Button" HeaderText="Seleccionar" SelectText="Contextualizar" ControlStyle-ForeColor="#8c2034" ControlStyle-Font-Bold="true" ControlStyle-CssClass="btn_form" ShowSelectButton="True" ControlStyle-BackColor="Transparent">
                            <ControlStyle CssClass="btn_form" />
                        </asp:CommandField>
                    </Columns>
                    <FooterStyle BackColor="#e6e7e8" />
                    <HeaderStyle HorizontalAlign="Left" BackColor="#58595b" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <%--<SelectedRowStyle BackColor="#808080" Font-Bold="True" ForeColor="White" />--%>
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#808080" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#383838" />
                </asp:GridView>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>

</body>
</html>




