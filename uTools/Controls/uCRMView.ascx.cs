﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;


namespace uTools.Controls
{
    public partial class uCRMView : System.Web.UI.UserControl
    {

        Cliente cliente = new Cliente();
        int acao_log = 0;


        public String Contacto
        {
            get
            {
                object o = ViewState["Contacto"];
                return (o == null) ? String.Empty : (string)o;
            }

            set
            {
                ViewState["Contacto"] = value;
            }
        }

        

        public string ClienteID
        {
            get
            {
                return lbl_cliente_id.Text;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            /*if (User.Identity.IsAuthenticated)
            {
                //bool test = User.IsInRole("user");
                Populate_Agencias();

            }
            else
            {
                Response.Redirect("~/Login.aspx?urlredirect=uCRMView.aspx");
            }*/

            if (!IsPostBack)
            {
                string strsql_agencias = "SELECT [Balcao].[ID], [Decricao] + ' ' + [dbo].[Tipo].Descricao + '-' + LTRIM(STR([Area])) Descricao " +
                    "FROM [uCRM].[dbo].[Balcao] " +
                    "LEFT JOIN [dbo].[Balcao_Area] ON [Balcao_Area].id=areaID " +
                    "LEFT JOIN [dbo].[Tipo] ON [dbo].[Tipo].ID=RedeID ORDER BY Descricao";

                Tools.Carregar_DropDownBox("uCRM_DB", "SELECT ID, Descricao FROM Tipo_Cliente", dropbox_tipo_cliente);
                Tools.Carregar_DropDownBox("uCRM_DB", strsql_agencias, dropbox_agencia);
                
                Contacto = Request.QueryString["contacto"];

                if (Contacto.IndexOf("@") == -1)
                {
                    cliente.Telefone1 = Contacto;
                    cliente.Telefone2 = Contacto;
                }
                else cliente.Email = Contacto;
                
                
                //ViewState["contacto_id"] = Request.QueryString["contacto_id"];
                //ViewState["user_id"] = Request.QueryString["user_id"];
                //ViewState["tarefa_id"] = Request.QueryString["user_id"];

                //message_status.Text = "contacto=[" + Contacto + "]";

                DataTable dt_clients = cliente.select_clients();

                if (dt_clients.Rows.Count !=0)
                {
                    if (dt_clients.Rows.Count > 1)
                    {
                        Carregar_Lista_Clientes(dt_clients);
                    }
                    else
                    {
                        lbl_cliente_id.Text = dt_clients.Rows[0].ItemArray[0].ToString();
                        cliente.SelectCliente_byID(int.Parse(lbl_cliente_id.Text), "uCRM_DB");
                        Load_Cliente();
                    }
                }                                                
            }
            

        }


        private void Load_Cliente()
        {
            lbl_cliente_id.Text = cliente.Cliente_id.ToString();
            txtbox_nome.Text = cliente.Nome.ToString();
            txtbox_email.Text = cliente.Email.ToString();
            txtbox_telefone1.Text = cliente.Telefone1.ToString();
            txtbox_telefone2.Text = cliente.Telefone2.ToString();
            txtbox_numero_conta.Text = cliente.Numero_conta;
            if (cliente.Agencia_id.ToString() != "0") dropbox_agencia.SelectedValue = cliente.Agencia_id.ToString();
            if (cliente.Tipo_id.ToString() != "0") dropbox_tipo_cliente.SelectedValue = cliente.Tipo_id.ToString();
        }

        private void Carregar_Lista_Clientes(DataTable dt)
        {
            gridview_interaccoes_clients.DataSource = dt;
            gridview_interaccoes_clients.DataBind();
            lbl_gridview_interaccoes_clients.Text = "CLIENTES";
        }
        

        protected void Populate_Interaccoes()
        {
            string db = "uCRM_DB";
            string sqlstr = "SELECT [uCRM].[dbo].[Interacao].[Id] " +
                            ",[uCRM].[dbo].[Tipo_Interacao].Descricao Interaccao, CONVERT(char(10),[Data],105) AS Data,[TipificacaoID] Tipificacao FROM [uCRM].[dbo].[Interacao] " +
                            "LEFT JOIN [uCRM].[dbo].[Tipo_Interacao] ON [uCRM].[dbo].[Interacao].TipoID=[uCRM].[dbo].[Tipo_Interacao].Id " +
                            "WHERE ClientID=" + cliente.Cliente_id + " ORDER BY Data";

            Tools.Carregar_GridView(db, sqlstr, gridview_interaccoes_clients);
            lbl_gridview_interaccoes_clients.Text = "ÚLTIMAS INTERAÇÕES";
        }

        protected void Editar_Dados()
        {
            if (lbl_cliente_id.Text != "") 
            {
                cliente.Cliente_id = int.Parse(lbl_cliente_id.Text);
            cliente.Nome = txtbox_nome.Text;
            cliente.Email = txtbox_email.Text;
            cliente.Telefone1 = txtbox_telefone1.Text;
            cliente.Telefone2 = txtbox_telefone2.Text;
            cliente.Numero_conta = txtbox_numero_conta.Text;
            cliente.Agencia_id = int.Parse(dropbox_agencia.SelectedValue);
            cliente.Tipo_id = int.Parse(dropbox_tipo_cliente.SelectedValue);

            acao_log = 2;

            registar_Log();

            cliente.update_cliente("uCRM_DB");
            //if (!cliente.update_cliente("uCRM_DB")) message_status.Text = "ERRO : a fazer udpate";
            }
        }

        protected void Adicionar_Cliente()
        {           
            cliente.Nome = txtbox_nome.Text;
            cliente.Email = txtbox_email.Text;
            cliente.Telefone1 = txtbox_telefone1.Text;
            cliente.Telefone2 = txtbox_telefone2.Text;
            cliente.Numero_conta = txtbox_numero_conta.Text;
            cliente.Agencia_id = int.Parse(dropbox_agencia.SelectedValue);
            cliente.Tipo_id = int.Parse(dropbox_tipo_cliente.SelectedValue);

            acao_log = 1;

            registar_Log();

            cliente.Cliente_id = cliente.insert_cliente("uCRM_DB");
            if (cliente.Cliente_id > 0)
            {
                lbl_cliente_id.Text = cliente.Cliente_id.ToString();
                Populate_Interaccoes();
            }
                
            //else
            //     message_status.Text = "ERRO : a fazer insert";            
        }

        private void Pesquisar_Dados()
        {

            // Tratar da excepção de campos int vázios
            cliente.Nome = txtbox_nome.Text;
            cliente.Email = txtbox_email.Text;
            cliente.Telefone1 = txtbox_telefone1.Text;
            cliente.Telefone2 = txtbox_telefone2.Text;
            cliente.Numero_conta = txtbox_numero_conta.Text;
            cliente.Agencia_id = int.Parse(dropbox_agencia.SelectedValue);
            cliente.Tipo_id = int.Parse(dropbox_tipo_cliente.SelectedValue);

            gridview_interaccoes_clients.DataSource = cliente.select_clients();
            gridview_interaccoes_clients.DataBind();
        }
        
        protected void btn_cancel_Click(object sender, EventArgs e)
        {
            System.Threading.Thread.Sleep(1000);

            if (lbl_cliente_id.Text != "")
            {
                if (cliente.SelectCliente_byID(int.Parse(lbl_cliente_id.Text), "uCRM_DB"))
                {
                    Load_Cliente();
                    Populate_Interaccoes();

                }
                //else
                //    message_status.Text = "ERRO : Cliente não existe";

                pnl_detalhe_cliente.Enabled = false;
                pnl_btns_accao.Visible = true;
                pnl_btns_ok.Visible = false;
            }

        }


        protected void gridview_interaccoes_clients_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            GridViewRow row = gridview_interaccoes_clients.Rows[Convert.ToInt32(e.CommandArgument)];

            if (cliente.SelectCliente_byID(int.Parse(row.Cells[1].Text), "uCRM_DB"))
            {
                Load_Cliente();
                Populate_Interaccoes();
            }            
        }        

        protected void btn_editar_Click(object sender, EventArgs e)
        {
            System.Threading.Thread.Sleep(1000);

            if (lbl_cliente_id.Text != "")
            {
                pnl_detalhe_cliente.Enabled = true;
                btn_ok.CommandName = "editar";
                pnl_btns_ok.Visible = true;
                pnl_btns_accao.Visible = false;

            }
            
        }

        protected void btn_persquisar_Click(object sender, EventArgs e)
        {

            System.Threading.Thread.Sleep(1000);

            //txtbox_nome.Text = string.Empty;
            //txtbox_telefone1.Text = string.Empty;
            //txtbox_telefone2.Text = string.Empty;
            //txtbox_email.Text = string.Empty;
            //txtbox_numero_conta.Text = string.Empty;
            //dropbox_agencia.SelectedIndex = 0;
            //dropbox_tipo_cliente.SelectedIndex = 0;

            Clear_TxtBox_Content();

            pnl_detalhe_cliente.Enabled = true;
            btn_ok.CommandName = "pesquisar";
            pnl_btns_ok.Visible = true;
            pnl_btns_accao.Visible = false;
        }

        protected void Clear_TxtBox_Content()
        {
            //lbl_cliente_id.Text = string.Empty;
            txtbox_nome.Text = string.Empty;
            txtbox_telefone1.Text = string.Empty;
            txtbox_telefone2.Text = string.Empty;
            txtbox_email.Text = string.Empty;
            txtbox_numero_conta.Text = string.Empty;
            dropbox_agencia.SelectedIndex = 0;
            dropbox_tipo_cliente.SelectedIndex = 0;
        }

        protected void btn_ok_Click(object sender, EventArgs e)
        {
            System.Threading.Thread.Sleep(1000);

            switch (btn_ok.CommandName)
            {
                case "editar":
                    Editar_Dados();
                    break;
                case "pesquisar":
                    Pesquisar_Dados();
                    break;
                case "adicionar":
                    Adicionar_Cliente();
                    break;
                default:
                    break;

            }

            pnl_detalhe_cliente.Enabled = false;
            pnl_btns_accao.Visible = true;
            pnl_btns_ok.Visible = false;
        }

        protected void btn_criar_Click(object sender, EventArgs e)
        {
            System.Threading.Thread.Sleep(1000);

            Clear_TxtBox_Content();
            pnl_detalhe_cliente.Enabled = true;
            btn_ok.CommandName = "adicionar";
            pnl_btns_ok.Visible = true;
            pnl_btns_accao.Visible = false;

        }

        public void Update_client(string id)
        {   
            if (id != "")
            {
                cliente.SelectCliente_byID(int.Parse(id), "uCRM_DB");
                Load_Cliente();
            }
            
        }



        private void registar_Log()
        {
            String[] log = new String[15];

            log[0] = "" + Context.User.Identity.Name;
            log[1] = "" + DateTime.Now.ToString();
            log[2] = "uCRM.dbo.SP_Inserir_Cliente";

            if (acao_log == 1)
            {
                log[3] = "" + 1;
                log[4] = "" + 4;
            }
            
            if (acao_log == 2)
            {
                log[3] = "" + 1;
                log[4] = "" + 5;

            }

            log[5] = "" + cliente.Cliente_id;
            log[6] = "" + cliente.Tipo_id;
            log[7] = cliente.Nome;
            log[8] = cliente.Email;
            log[9] = cliente.Telefone1;
            log[10] = cliente.Telefone2;
            log[11] = cliente.Numero_conta;
            log[12] = "" + cliente.Agencia_id;

            Tools.registar_Log(log);
        }

        protected void txtbox_nome_TextChanged(object sender, EventArgs e)
        {
            
            txtbox_nome.Text = txtbox_nome.Text.ToUpper();
        }

        protected void txtbox_email_TextChanged(object sender, EventArgs e)
        {
            txtbox_email.Text.ToLower();
        }

        
    }
}