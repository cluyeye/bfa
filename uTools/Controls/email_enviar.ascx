﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="email_enviar.ascx.cs" Inherits="uTools.Controls.email_enviar" %>

<style>
    table .txtInput {
        width: 97%;
        border-radius: 4px;
        margin-bottom: 6px;
    }
</style>


<asp:Panel ID="pnl_btn_accao" runat="server" Visible="false">
    <div align="justify">
        <asp:ImageButton ID="btn_reply" runat="server" CssClass="img_bt" ImageUrl="~/images/reply.png" title="Responder" OnClick="btn_reply_Click" />
        <asp:ImageButton ID="btn_replyall" runat="server" CssClass="img_bt" ImageUrl="~/images/reply_all.png" title="Responder Todos" OnClick="btn_replyall_Click" />
        <asp:ImageButton ID="btn_forward" runat="server" CssClass="img_bt" ImageUrl="~/images/reply_150px.png" title="Reencaminhar" OnClick="btn_forward_Click" />
    </div>

</asp:Panel>
<asp:HiddenField ID="hf_ID" runat="server" />

<table style="width: 100%;">
    <tr>
        <td style="width: 70%">
            <asp:HiddenField ID="txtbox_from" runat="server" />
            <label for="txtbox_to">To</label><br />
            <asp:TextBox ID="txtbox_to" runat="server" CssClass="txtInput" Style="width: 90%">
            </asp:TextBox>

        </td>
        <td style="width: 30%" rowspan="3">
            <ajaxToolkit:AjaxFileUpload ID="fupload_attachs" runat="server" OnUploadComplete="fupload_attachs_UploadComplete" AutoStartUpload="true" Width="100%" Height="100%" />

        </td>
    </tr>
    <tr>
        <td>
            <label for="txtbox_cc">CC</label><br />
            <asp:TextBox ID="txtbox_cc" runat="server" CssClass="txtInput" Style="width: 90%"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <label for="txtbox_assunto">Assunto</label><br />
            <asp:TextBox ID="txtbox_assunto" runat="server" CssClass="txtInput" Style="width: 90%"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <ajaxToolkit:HtmlEditorExtender ID="rtf_email_HtmlEditorExtender" runat="server" BehaviorID="rtf_email_HtmlEditorExtender" TargetControlID="rtf_email">
            </ajaxToolkit:HtmlEditorExtender>
            <asp:TextBox ID="rtf_email" CssClass="txtInput_email" runat="server" Width="97%" Height="250px"></asp:TextBox>
        </td>
    </tr>
</table>

<asp:Panel ID="pnl_btn_enviar" runat="server" Visible="false">

    <asp:ImageButton ID="btn_send" runat="server" CssClass="img_bt" ImageUrl="~/images/ok.png" title="Limpa Busca" OnClick="btn_send_Click" />
    <asp:ImageButton ID="btn_cancelar" runat="server" CssClass="img_bt" ImageUrl="~/images/close_01.png" title="Limpa Busca" OnClick="btn_cancelar_Click" />


</asp:Panel>

