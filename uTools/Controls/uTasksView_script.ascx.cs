﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using Microsoft.AspNet.Identity;

namespace uTools.Controls
{
    public partial class uTasksView_script : System.Web.UI.UserControl
    {
        
        int acao_log = 0;
        
        public class uCRMClient : EventArgs
        {
            public string ClientID { get; set; }
            public uCRMClient(string client_id)
            {
                ClientID = client_id;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lxt_head_main_tasks.Text = "DETALHE DO TICKET";

            if (!IsPostBack)
            {
                if (Request.QueryString["tarefa_id"] != "")
                    Update_TarefaID_From_uCI(Request.QueryString["tarefa_id"]);
                Populate_Lista_Tarefas();
                Tools.Carregar_DropDownBox("uTasks_DB", "Select ID, Descricao from Departamento ORDER BY Descricao", dropbox_departamento);
                Tools.Carregar_DropDownBox("uTasks_DB", "Select ID, Descricao from Estado where visible = 1 ORDER BY Descricao", dropbox_estado);
                Tools.Carregar_DropDownBox("uTasks_DB", "SELECT ID, Descricao FROM Tipificacao WHERE TipificacaoID=1 ORDER BY Descricao", dropbox_tipo);
                Tools.Carregar_DropDownBox("uTasks_DB", "SELECT ID, Descricao FROM Canal_Contacto ORDER BY Descricao", dropbox_canal_contacto);

                if (this.Page.GetType().FullName == "ASP.utools_main_aspx") radiobtn_filtro_cliente.Visible = true;
                else radiobtn_filtro_cliente.Visible = false;

            }

            Populate_Tasks_Count("Total de Tickets", "FROM Tarefa");
        }

        private void Update_TarefaID_From_uCI(string id)
        {
            string db = "uTasks_DB";

            string sqlstr = "SELECT TipoID FROM Tarefa WHERE Tarefa.ID=" + id;
            string connectionString = ConfigurationManager.ConnectionStrings[db].ConnectionString;
            SqlConnection DBConnection = new SqlConnection(connectionString);
            using (DBConnection)
            {
                SqlCommand command = new SqlCommand(sqlstr, DBConnection);
                try
                {
                    DBConnection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        reader.Read();
                        if (reader["TipoID"].ToString() == "11")
                            wuc_create_task.TarefaID = id;
                        else Update_Detalhe_Tarefa(id);
                    }
                    reader.Close();
                }
                catch (Exception er)
                {

                }

            }

        }

        private void Populate_Tasks_Count(String label, String filtro_count)
        {
            String query_count = "SELECT COUNT(*) as Count ";

            literal_count.Text = label + ": " + Tools.Carregar_Task_Count("uTasks_DB", query_count + "" + filtro_count) + "";
        }


        private void Populate_Lista_Tarefas()
        {
            //string sqlstr = "SELECT TOP 50 Tarefa.ID, (LEFT(Tarefa.Descricao, 70) + '...') Descricao, CONVERT(char(16),Tarefa.DataRegisto,121) DataRegisto, CONVERT(char(16),Tarefa.DataFecho,121) DataFecho, " +
            //    " Tarefa.SLAHoras, Estado.Descricao Estado, Departamento.Descricao Departamento, Tipificacao.Descricao Tipo, Tarefa.SLAHoras, (Tarefa.SLAHoras - DATEDIFF(HH, ISNULL(Tarefa.DataFecho, GETDATE()),Tarefa.DataRegisto )) diferenca_SLA " +
            //    "FROM Tarefa " +
            //    "LEFT JOIN Estado ON Tarefa.EstadoID=Estado.ID " +
            //    "LEFT JOIN Departamento ON Tarefa.DepartamentoID=Departamento.ID " +
            //    "LEFT JOIN Tipificacao ON Tarefa.TipoID=Tipificacao.ID WHERE Tarefa.TipoID<>11 ORDER BY DataFecho ASC, DataRegisto ASC";

            string sqlstr = " SELECT TOP 50 Tarefa.ID, (LEFT(Tarefa.Descricao, 70) + '...') AS Descricao, CONVERT(char(16),Tarefa.DataRegisto,121) AS DataRegisto, CONVERT(char(16),Tarefa.DataFecho,121) AS DataFecho," +
                            " Tarefa.SLAHoras, Estado.Descricao AS Estado, Departamento.Descricao AS Departamento, Tipo.Descricao AS Tipo, Canal_Contacto.Descricao AS Canal_Contacto, Assunto.Descricao AS Assunto, Tarefa.SLAHoras AS SLAHoras, (Tarefa.SLAHoras - DATEDIFF(HH, ISNULL(Tarefa.DataFecho, GETDATE()),Tarefa.DataRegisto )) AS Diferenca_SLA, AspUser.UserName AS Usuario, (LEFT(AspUser.Name, 170) + '...') AS Feito_por" +
                            " FROM Tarefa" +
                            " LEFT JOIN Estado ON (Tarefa.EstadoID = Estado.ID)" +
                            " LEFT JOIN Canal_Contacto ON (Tarefa.CanalContactoID = Canal_Contacto.ID)" +
                            " LEFT JOIN Departamento ON (Tarefa.DepartamentoID = Departamento.ID)" +
                            " LEFT JOIN Tipificacao AS Tipo ON (Tarefa.TipoID = Tipo.ID)" +
                            " LEFT JOIN Tipificacao AS Assunto ON (Tarefa.TipificacaoID = Assunto.ID)" +
                            " LEFT JOIN uAuth.dbo.AspNetUsers AS AspUser ON (Tarefa.UserIDRegisto = AspUser.Id)" +
                            " WHERE Tarefa.TipoID<>11" +
                            " ORDER BY DataFecho ASC, DataRegisto ASC"; 
            
            Tools.Carregar_GridView("uTasks_DB", sqlstr, gridview_tarefas);


        }

        protected void gridview_tarefas_Sorting(object sender, GridViewSortEventArgs e)
        {
            //string sqlstr = "SELECT TOP 50 Tarefa.ID, (LEFT(Tarefa.Descricao, 70) + '...') Descricao, CONVERT(char(16),Tarefa.DataRegisto,121) DataRegisto, CONVERT(char(16),Tarefa.DataFecho,121) DataFecho, " +
            //    "Tarefa.SLAHoras, Estado.Descricao Estado, Departamento.Descricao Departamento, Tipificacao.Descricao Tipo, Tarefa.SLAHoras, (Tarefa.SLAHoras - DATEDIFF(HH, ISNULL(Tarefa.DataFecho, GETDATE()),Tarefa.DataRegisto )) diferenca_SLA " +
            //    "FROM Tarefa " +
            //    "LEFT JOIN Estado ON Tarefa.EstadoID=Estado.ID " +
            //    "LEFT JOIN Departamento ON Tarefa.DepartamentoID=Departamento.ID " +
            //    "LEFT JOIN Tipificacao ON Tarefa.TipoID=Tipificacao.ID WHERE Tarefa.TipoID<>11 " +
            //    "ORDER BY " + e.SortExpression + " ASC";

            string sqlstr = " SELECT TOP 50 Tarefa.ID, (LEFT(Tarefa.Descricao, 70) + '...') AS Descricao, CONVERT(char(16),Tarefa.DataRegisto,121) AS DataRegisto, CONVERT(char(16),Tarefa.DataFecho,121) AS DataFecho," +
                " Tarefa.SLAHoras, Estado.Descricao AS Estado, Departamento.Descricao AS Departamento, Tipo.Descricao AS Tipo, Canal_Contacto.Descricao AS Canal_Contacto, Assunto.Descricao AS Assunto, Tarefa.SLAHoras AS SLAHoras, (Tarefa.SLAHoras - DATEDIFF(HH, ISNULL(Tarefa.DataFecho, GETDATE()),Tarefa.DataRegisto )) AS Diferenca_SLA, AspUser.UserName AS Usuario, (LEFT(AspUser.Name, 170) + '...') AS Feito_por" +
                " FROM Tarefa" +
                " LEFT JOIN Estado ON (Tarefa.EstadoID = Estado.ID)" +
                " LEFT JOIN Canal_Contacto ON (Tarefa.CanalContactoID = Canal_Contacto.ID)" +
                " LEFT JOIN Departamento ON (Tarefa.DepartamentoID = Departamento.ID)" +
                " LEFT JOIN Tipificacao AS Tipo ON (Tarefa.TipoID = Tipo.ID)" +
                " LEFT JOIN Tipificacao AS Assunto ON (Tarefa.TipificacaoID = Assunto.ID)" +
                " LEFT JOIN uAuth.dbo.AspNetUsers AS AspUser ON (Tarefa.UserIDRegisto = AspUser.Id)" +
                " WHERE Tarefa.TipoID<>11" +
                " ORDER BY " + e.SortExpression + " ASC"; 
            
            
            Tools.Carregar_GridView("uTasks_DB", sqlstr, gridview_tarefas);

        }


        private void Populate_Lista_Email()
        {
            string sqlstr = "SELECT Top 50 Tarefa_Email.[ID], Tarefa_Email.[From], Tarefa_Email.[To], Tarefa_Email.[Assunto], " +
                "CONVERT(char(16),Tarefa_Email.[DataEmail],121) DataEmail, Tipificacao.Descricao Tipo, uAuth.UserName " +
                "FROM [uTasks].[dbo].[Tarefa_Email] " +
                "LEFT JOIN Tipificacao ON Tarefa_Email.TipoID=Tipificacao.ID " +
                "LEFT JOIN uAuth.dbo.AspNetUsers uAuth ON [uTasks].[dbo].[Tarefa_Email].UserID=uAuth.Id " +
                "WHERE TarefaID=" + lbl_ID.Text.ToString().Trim() + " ORDER BY DataEmail DESC";

            Tools.Carregar_GridView("uTasks_DB", sqlstr, gridview_email);
        }


        protected void gridview_tarefas_SelectedIndexChanged(object sender, EventArgs e)
        {
            Update_Detalhe_Tarefa(gridview_tarefas.Rows[gridview_tarefas.SelectedIndex].Cells[1].Text);
        }


        public delegate bool OnUpdateDetalheTarefa_event(string id);
        public event OnUpdateDetalheTarefa_event OnUpdateDetalheTarefa;



        private void Update_Detalhe_Tarefa(string id)
        {


            string db = "uTasks_DB";

            //string sqlstr = "SELECT Tarefa.ID, Tarefa.Descricao, Tarefa.Resumo, Tarefa.ResolucaoResumo, CONVERT(char(16),Tarefa.DataRegisto,121) DataRegisto, CONVERT(char(16),Tarefa.DataFecho,121) DataFecho, " +
            //    "Tarefa.SLAHoras, Tarefa.EstadoID, Estado.Descricao Estado, Tarefa.DepartamentoID, Departamento.Descricao Departamento, Tipificacao.Descricao Tipo, Tarefa.SLAHoras, (Tarefa.SLAHoras - DATEDIFF(HH, ISNULL(Tarefa.DataFecho, GETDATE()),Tarefa.DataRegisto )) diferenca_SLA, tarefa.clienteID " +
            //    "FROM Tarefa " +
            //    "LEFT JOIN Estado ON Tarefa.EstadoID=Estado.ID " +
            //    "LEFT JOIN Departamento ON Tarefa.DepartamentoID=Departamento.ID " +
            //    "LEFT JOIN Tipificacao ON Tarefa.TipoID=Tipificacao.ID " +
            //    "WHERE Tarefa.ID=" + id;


            string sqlstr = "SELECT Tarefa.ID, Tarefa.Descricao, Tarefa.Resumo, Tarefa.ResolucaoResumo, CONVERT(char(16),Tarefa.DataRegisto,121) DataRegisto, CONVERT(char(16),Tarefa.DataFecho,121) DataFecho, " +
                "Tarefa.EstadoID, Estado.Descricao Estado, Tarefa.DepartamentoID, Departamento.Descricao Departamento, Tipificacao.Descricao Tipo, Tarefa.SLAHoras, (Tarefa.SLAHoras - DATEDIFF(HH, ISNULL(Tarefa.DataFecho, GETDATE()),Tarefa.DataRegisto )) diferenca_SLA, tarefa.clienteID, Cliente.Nome AS ClienteNome, " +
                "Tarefa.CanalContactoID AS Canal_ContactoID, Canal_Contacto.Descricao AS Canal_Contacto, AspUser.UserName AS Usuario, (LEFT(AspUser.Name, 170) + '...') AS Feito_por " +
                "FROM Tarefa " +
                "LEFT JOIN Estado ON Tarefa.EstadoID=Estado.ID " +
                "LEFT JOIN Departamento ON Tarefa.DepartamentoID=Departamento.ID " +
                "LEFT JOIN Tipificacao ON Tarefa.TipoID=Tipificacao.ID " +
                "LEFT JOIN uCRM.dbo.Cliente AS Cliente ON (Tarefa.ClienteID = Cliente.Id) " +
                "LEFT JOIN Canal_Contacto ON (Tarefa.CanalContactoID = Canal_Contacto.ID) " +
                "LEFT JOIN Tipificacao AS Assunto ON (Tarefa.TipificacaoID = Assunto.ID) " +
                "LEFT JOIN uAuth.dbo.AspNetUsers AS AspUser ON (Tarefa.UserIDRegisto = AspUser.Id) " +

                "WHERE Tarefa.ID=" + id; 
            
            string connectionString = ConfigurationManager.ConnectionStrings
                [db].ConnectionString;
            SqlConnection DBConnection = new SqlConnection(connectionString);
            using (DBConnection)
            {
                SqlCommand command = new SqlCommand(sqlstr, DBConnection);
                try
                {
                    DBConnection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        reader.Read();
                        lbl_ID.Text = reader["ID"].ToString();
                        lbl_descricao.Text = reader["Descricao"].ToString();
                        lbl_resumo.Text = reader["Resumo"].ToString();
                        lbl_resolucao.Text = reader["ResolucaoResumo"].ToString();
                        lbl_registo.Text = reader["DataRegisto"].ToString();
                        lbl_fecho.Text = reader["DataFecho"].ToString();
                        hf_estadoID.Value = reader["EstadoID"].ToString();
                        lbl_detalhe_estado.Text = reader["Estado"].ToString();
                        hf_departamentoID.Value = reader["DepartamentoID"].ToString();
                        lbl_detalhe_departamento.Text = reader["Departamento"].ToString();
                        lbl_tipo.Text = reader["Tipo"].ToString();
                        lbl_sla.Text = reader["SLAHoras"].ToString() + "(" + reader["diferenca_SLA"].ToString() + ")";
                        lbl_diferenca_SLA.Text = reader["diferenca_SLA"].ToString();
                        lbl_cliente.Text = reader["ClienteID"].ToString();
                        lbl_cliente_nome.Text = reader["ClienteNome"].ToString();
                        lbl_canal_contacto_id.Text = reader["Canal_ContactoID"].ToString();
                        lbl_canal_contacto.Text = reader["Canal_Contacto"].ToString();
                        lbl_AspUser.Text = reader["Usuario"].ToString();
                        lbl_feito_por.Text = reader["Feito_por"].ToString();

                        if (OnUpdateDetalheTarefa != null)
                        {

                            // uCRMClient cliente = new uCRMClient(reader["ClienteID"].ToString());
                            OnUpdateDetalheTarefa(reader["ClienteID"].ToString());
                        }
                        //((utools_main)this.Page).Update_CRM(reader["ClienteID"].ToString());
                    }
                    reader.Close();
                    sqlstr = "SELECT Departamento.Descricao Departamento, Estado.Descricao Estado, CONVERT(char(16),Tarefa_Accao.DataRegisto,121) " +
                        "DataRegisto, uAuth.UserName FROM Tarefa_Accao " +
                        "LEFT JOIN Estado ON Tarefa_Accao.EstadoID=Estado.ID " +
                        "LEFT JOIN Departamento ON Tarefa_Accao.DepartamentoID=Departamento.ID " +
                        "LEFT JOIN uAuth.dbo.AspNetUsers uAuth ON Tarefa_Accao.UserID=uAuth.Id " +
                        "WHERE Tarefa_Accao.TarefaID=" + id + " ORDER BY Tarefa_Accao.DataRegisto ASC;";
                    command.CommandText = sqlstr;
                    reader = command.ExecuteReader();
                    gridview_historico.DataSource = reader;
                    gridview_historico.DataBind();
                    reader.Close();

                    sqlstr = "SELECT Tarefa_Comentario.Comentario, CONVERT(char(16),Tarefa_Comentario.DataRegisto,121) DataRegisto, UserName " +
                        "FROM Tarefa_Comentario LEFT JOIN uAuth.dbo.AspNetUsers uAuth ON Tarefa_Comentario.UserID=uAuth.Id " +
                        "WHERE Tarefa_Comentario.TarefaID=" + id + " ORDER BY DataRegisto ASC";
                    command.CommandText = sqlstr;
                    reader = command.ExecuteReader();
                    gridview_comentario.DataSource = reader;
                    gridview_comentario.DataBind();
                    reader.Close();

                    sqlstr = "SELECT TipificacaoID FROM Tarefa_Tipificacao WHERE TarefaID=" + id;
                    command.CommandText = sqlstr;
                    reader = command.ExecuteReader();
                    List<int> tipificacoes = new List<int>();

                    while (reader.Read())
                    {
                        tipificacoes.Add(reader.GetInt32(0));
                    }
                    reader.Close();

                    lbl_tipificacao.Text = "";

                    foreach (int tipificacao in tipificacoes)
                    {
                        //SqlCommand cmdTipificacoes = new SqlCommand("exec Niveis_Tipificacoes @TipificacaoID=" + reader["TipificacaoID"], DBConnection);
                        command.CommandText = "SP_Niveis_Tipificacoes";
                        command.Parameters.Add(new SqlParameter("@TipificacaoID", tipificacao));
                        command.CommandType = CommandType.StoredProcedure;
                        //cmdTipificacoes.CommandType = CommandType.StoredProcedure;                        
                        //SqlDataReader readerniveistipificacao =  command.ExecuteReader();
                        reader = command.ExecuteReader();
                        reader.Read();
                        while (reader.Read()) lbl_tipificacao.Text = lbl_tipificacao.Text + "\\" + reader["Descricao"];
                        lbl_tipificacao.Text = lbl_tipificacao.Text;
                        reader.Close();
                    }

                    Populate_Lista_Email();

                    mv_tarefa.ActiveViewIndex = 0;
                    mv_tarefa.Visible = true;
                    //pnlupd_accao.Visible = true;
                    collpanel_Detalhe.Collapsed = false;
                }
                catch (Exception erro)
                {
                    //label_status.Text = "ERRO : " + erro.Message;

                }
            }
        }

        private string get_selected_from_crm()
        {
            if (this.Page.GetType().FullName == "ASP.utools_main_aspx")
                return ((utools_main)this.Page).Get_Selected_Cliente();
            else return string.Empty;
        }

        protected void btn_filtro_Click(object sender, ImageClickEventArgs e)
        {
            System.Threading.Thread.Sleep(1000);
            
            string where = string.Empty;
            string sqlstr = string.Empty;
            string sqlstr_from = string.Empty;

            if (txtbox_id.Text.Length > 0) where = " AND Tarefa.ID = " + txtbox_id.Text.Trim();
            if (txtbox_descricao.Text.Length > 0) where = where + " AND Tarefa.Descricao LIKE '%" + txtbox_descricao.Text + "%'";
            if (txtbox_username.Text.Length > 0) where = where + " AND AspUser.UserName LIKE '%" + txtbox_username.Text + "%'";
            if (txtbox_nome_user.Text.Length > 0) where = where + " AND AspUser.Name LIKE '%" + txtbox_nome_user.Text + "%'";

            if (txtbox_clientID.Text.Length > 0) where = where + " AND Tarefa.ClienteID LIKE '%" + txtbox_clientID.Text + "%'";


            if (dropbox_tipo.SelectedIndex != 0) where = where + " AND Tarefa.TipoID = " + dropbox_tipo.SelectedValue;
            if (dropbox_departamento.SelectedIndex != 0) where = where + " AND Tarefa.DepartamentoID = " + dropbox_departamento.SelectedValue;
            if (dropbox_estado.SelectedIndex != 0) where = where + " AND Tarefa.EstadoID = " + dropbox_estado.SelectedValue;
            if (dropbox_canal_contacto.SelectedIndex != 0) where = where + " AND Tarefa.CanalContactoID = " + dropbox_canal_contacto.SelectedValue;

            if (txtbox_dataregisto_inicio.Text.Length > 0)
            {
                if (txtbox_dataregisto_fim.Text.Length > 0) where = where + " AND DataRegisto BETWEEN '" + txtbox_dataregisto_inicio.Text + "' AND '" + txtbox_dataregisto_fim.Text + "'";
            }
            if (txtbox_datafecho_inicio.Text.Length > 0)
            {
                if (txtbox_datafecho_fim.Text.Length > 0) where = where + " AND DataFecho BETWEEN '" + txtbox_datafecho_inicio.Text + "' AND '" + txtbox_datafecho_fim.Text + "'";
            }



            string cliente_id = get_selected_from_crm();

            if (radiobtn_filtro_cliente.Checked && cliente_id != string.Empty) where = where + " AND Tarefa.ClienteID =" + cliente_id;

            //sqlstr = "SELECT TOP 50 Tarefa.ID, (LEFT(Tarefa.Descricao, 70)) Descricao, CONVERT(char(16),Tarefa.DataRegisto,121) DataRegisto, CONVERT(char(16),Tarefa.DataFecho,121) DataFecho, " +
            //"Tarefa.SLAHoras, Estado.Descricao Estado, Departamento.Descricao Departamento, Tipificacao.Descricao Tipo, Tarefa.SLAHoras, (Tarefa.SLAHoras - DATEDIFF(HH, ISNULL(Tarefa.DataFecho, GETDATE()),Tarefa.DataRegisto )) diferenca_SLA ";

            sqlstr = "SELECT TOP 50 Tarefa.ID, (LEFT(Tarefa.Descricao, 70) + '...') AS Descricao, CONVERT(char(16),Tarefa.DataRegisto,121) AS DataRegisto, CONVERT(char(16),Tarefa.DataFecho,121) AS DataFecho," +
            " Tarefa.SLAHoras, Estado.Descricao AS Estado, Departamento.Descricao AS Departamento, Tipo.Descricao AS Tipo, Canal_Contacto.Descricao AS Canal_Contacto, Assunto.Descricao AS Assunto, Tarefa.SLAHoras AS SLAHoras, (Tarefa.SLAHoras - DATEDIFF(HH, ISNULL(Tarefa.DataFecho, GETDATE()),Tarefa.DataRegisto )) AS Diferenca_SLA, AspUser.UserName AS Usuario, (LEFT(AspUser.Name, 170) + '...') AS Feito_por";

            
            //sqlstr_from = "FROM Tarefa " +
            //"LEFT JOIN Estado ON Tarefa.EstadoID=Estado.ID " +
            //"LEFT JOIN Departamento ON Tarefa.DepartamentoID=Departamento.ID " +
            //"LEFT JOIN Tipificacao ON Tarefa.TipoID=Tipificacao.ID " +
            //"WHERE Tarefa.TipoID<>11 " + where;

            sqlstr_from = " FROM Tarefa" +
            " LEFT JOIN Estado ON (Tarefa.EstadoID = Estado.ID)" +
            " LEFT JOIN Canal_Contacto ON (Tarefa.CanalContactoID = Canal_Contacto.ID)" +
            " LEFT JOIN Departamento ON (Tarefa.DepartamentoID = Departamento.ID)" +
            " LEFT JOIN Tipificacao AS Tipo ON (Tarefa.TipoID = Tipo.ID)" +
            " LEFT JOIN Tipificacao AS Assunto ON (Tarefa.TipificacaoID = Assunto.ID)" +
            " LEFT JOIN uAuth.dbo.AspNetUsers AS AspUser ON (Tarefa.UserIDRegisto = AspUser.Id)" +
            " WHERE Tarefa.TipoID<>11" + where;
            
            sqlstr = sqlstr + "" + sqlstr_from;

            Tools.Carregar_GridView("uTasks_DB", sqlstr, gridview_tarefas);

            Populate_Tasks_Count("Total de Filtrados", sqlstr_from);
        }

        protected void btn_clearfiltro_Click(object sender, ImageClickEventArgs e)
        {
            txtbox_id.Text = "";
            txtbox_descricao.Text = "";

            txtbox_username.Text = "";
            txtbox_nome_user.Text = "";

            txtbox_dataregisto_inicio.Text = "";
            txtbox_dataregisto_fim.Text = "";

            txtbox_datafecho_inicio.Text = "";
            txtbox_datafecho_fim.Text = "";

            dropbox_tipo.SelectedIndex = 0;
            dropbox_departamento.SelectedIndex = 0;
            dropbox_estado.SelectedIndex = 0;
            dropbox_canal_contacto.SelectedIndex = 0;

            Populate_Lista_Tarefas();
        }



        protected void btn_accao_confirm_Click(object sender, ImageClickEventArgs e)
        {
            System.Threading.Thread.Sleep(1000);
            string sqlstr;

            switch (dropbox_accao.SelectedValue)
            {
                case "1":
                    sqlstr = "INSERT INTO Tarefa_Accao (TarefaID, DepartamentoID, EstadoID, DataRegisto, UserID) VALUES (" +
                        lbl_ID.Text + "," + dropbox_alterar.SelectedValue + ", 5, GETDATE(),'" + Context.User.Identity.GetUserId() + "')";
                    Tools.Update_Registo("uTasks_DB", sqlstr);
                    sqlstr = "UPDATE Tarefa SET departamentoID=" + dropbox_alterar.SelectedValue + ", EstadoID = 5 WHERE ID=" + lbl_ID.Text;
                    Tools.Update_Registo("uTasks_DB", sqlstr);

                    acao_log = 1;
                    registar_Log();

                    break;
                case "2":
                    sqlstr = "INSERT INTO Tarefa_Accao (TarefaID, DepartamentoID, EstadoID, DataRegisto, UserID) VALUES (" +
                        lbl_ID.Text + "," + hf_departamentoID.Value + "," + dropbox_alterar.SelectedValue + ", GETDATE(), '" + Context.User.Identity.GetUserId() + "')";
                    Tools.Update_Registo("uTasks_DB", sqlstr);
                    sqlstr = "UPDATE Tarefa SET estadoID=" + dropbox_alterar.SelectedValue + ", DepartamentoID=" + hf_departamentoID.Value + " WHERE ID=" + lbl_ID.Text;
                    Tools.Update_Registo("uTasks_DB", sqlstr);

                    acao_log = 2;
                    registar_Log();

                    break;
                case "3":
                    sqlstr = "UPDATE Tarefa SET tipoID=" + dropbox_alterar.SelectedValue + " WHERE ID = " + lbl_ID.Text;
                    Tools.Update_Registo("uTasks_DB", sqlstr);

                    acao_log = 3;
                    registar_Log();

                    break;
                case "4":
                    sqlstr = "INSERT INTO Tarefa_Comentario (Comentario,TarefaID, DataRegisto, UserID) VALUES ('" + txtbox_editar_comentario.Text +
                        "'," + lbl_ID.Text + ",GETDATE(),'" + Context.User.Identity.GetUserId() + "')";
                    Tools.Update_Registo("uTasks_DB", sqlstr);

                    acao_log = 4;
                    registar_Log();

                    break;
                case "5":
                    sqlstr = "UPDATE Tarefa SET Descricao='" + txtbox_editar_descricao.Text + "', ResolucaoResumo='" + txtbox_editar_resolucao.Text +
                        "', Resumo='" + txtbox_editar_resumo.Text + "' WHERE ID=" + lbl_ID.Text;
                    Tools.Update_Registo("uTasks_DB", sqlstr);

                    acao_log = 5;
                    registar_Log();

                    break;
                case "6":
                    email_enviar_info.Send_Email_Outside_Comd();
                    dropbox_accao.SelectedIndex = 0;
                    mv_accao.ActiveViewIndex = 0;

                    acao_log = 6;
                    registar_Log();

                    break;
                default:
                    break;
            }
            Update_Detalhe_Tarefa(lbl_ID.Text);
        }

        protected void dropbox_accao_SelectedIndexChanged(object sender, EventArgs e)
        {
            string sqlstr;

            switch (dropbox_accao.SelectedValue)
            {
                case "1":
                    dropbox_alterar.Items.Clear();
                    sqlstr = "Select ID, Descricao from Departamento ORDER BY Descricao";
                    Tools.Carregar_DropDownBox("uTasks_DB", sqlstr, dropbox_alterar);
                    pnl_accao.ControlStyle.CssClass = "modal_0";
                    mv_accao.ActiveViewIndex = 0;
                    break;
                case "2":
                    dropbox_alterar.Items.Clear();
                    sqlstr = "Select ID, Descricao from Estado where visible = 1 ORDER BY Descricao";
                    Tools.Carregar_DropDownBox("uTasks_DB", sqlstr, dropbox_alterar);
                    pnl_accao.ControlStyle.CssClass = "modal_0";
                    mv_accao.ActiveViewIndex = 0;
                    break;
                case "3":
                    dropbox_alterar.Items.Clear();
                    sqlstr = "Select ID, Descricao from Tipificacao WHERE TipificacaoID=1 ORDER BY Descricao";
                    Tools.Carregar_DropDownBox("uTasks_DB", sqlstr, dropbox_alterar);
                    pnl_accao.ControlStyle.CssClass = "modal_0";
                    mv_accao.ActiveViewIndex = 0;
                    break;
                case "4":
                    mv_accao.ActiveViewIndex = 1;
                    pnl_accao.ControlStyle.CssClass = "modal_1";
                    break;
                case "5":
                    txtbox_editar_descricao.Text = lbl_descricao.Text;
                    txtbox_editar_resumo.Text = lbl_resumo.Text;
                    txtbox_editar_resolucao.Text = lbl_resolucao.Text;
                    pnl_accao.ControlStyle.CssClass = "modal_2";
                    mv_accao.ActiveViewIndex = 2;
                    break;
                case "6":
                    if (pnl_detalhe_email.Visible) pnl_detalhe_email.Visible = false;
                    Set_Body_Mail_Info();
                    pnl_accao.ControlStyle.CssClass = "modal_3";
                    mv_accao.ActiveViewIndex = 3;
                    break;
                default:
                    break;

            }
        }


        protected void btn_nova_task_Click(object sender, ImageClickEventArgs e)
        {
            System.Threading.Thread.Sleep(1000);
            
            lxt_head_main_tasks.Text = "NOVO TICKET";
            mv_tarefa.ActiveViewIndex = 1;
            mv_tarefa.Visible = true;
            collpanel_Detalhe.Collapsed = false;
            gridview_tarefas.SelectedIndex = -1;
        }

        protected void gridview_email_SelectedIndexChanged(object sender, EventArgs e)
        {
            dropbox_accao.SelectedIndex = 0;
            mv_accao.ActiveViewIndex = 0;
            pnl_detalhe_email.Visible = true;
            email_tarefa_detalhe.Show_Email_Detail(gridview_email.SelectedRow.Cells[1].Text);
        }

        private void Set_Body_Mail_Info()
        {


            String body = "<br/><br/><br/><hr>Departamento : " + lbl_detalhe_departamento.Text + " - Estado : " + lbl_detalhe_estado.Text + "<br/><br/><br/>" +
                        "<b>Descrição : </b><br>" + lbl_descricao.Text + "<br/><br/><hr>Comentários :<br/>";



            foreach (GridViewRow row in gridview_comentario.Rows)
            {
                body = body + "<br><br><b>De : " + row.Cells[2].Text + " - " + row.Cells[0].Text + "</b><br/>" + row.Cells[1].Text + "<br/><br/>";
            }

            string assunto = "[" + lbl_ID.Text + "] " + lbl_tipo.Text + " - " + lbl_tipificacao.Text;

            email_enviar_info.Carregar_Info_Body(body, assunto, lbl_ID.Text);
        }


        private void registar_Log()
        {
            String[] log = new String[15];

            log[0] = "" + Context.User.Identity.GetUserId();
            log[1] = "" + DateTime.Now.ToString();

            if (acao_log == 1)
            {
                log[2] = "uTasks.dbo.Tarefa_Accao";
                log[3] = "" + 11;
                log[4] = "" + 5;
            }

            if (acao_log == 2)
            {
                log[2] = "uTasks.dbo.Estado";
                log[3] = "" + 7;
                log[4] = "" + 5;
            }

            if (acao_log == 3)
            {
                log[2] = "uTasks.dbo.Tarefa_Accao";
                log[3] = "" + 8;
                log[4] = "" + 5;
            }

            if (acao_log == 4)
            {
                log[2] = "uTasks.dbo.Comentario";
                log[3] = "" + 9;
                log[4] = "" + 5;
            }

            if (acao_log == 5)
            {
                log[2] = "uTasks.dbo.Tarefa";
                log[3] = "" + 10;
                log[4] = "" + 5;
            }

            if (acao_log == 6)
            {
                log[2] = "uTasks.dbo.Tarefa_Email";
                log[3] = "" + 4;
                log[4] = "" + 5;
            }

            log[5] = "" + get_selected_from_crm();
            //log[6] = txt_descricao.Text;
            log[7] = dropbox_departamento.SelectedValue;
            log[8] = dropbox_tipo.SelectedValue;
            log[9] = "" + 1;
            log[10] = "" + 48;

            Tools.registar_Log(log);
        }



    }
}