﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace uTools.Controls
{
    public partial class uKnowledgeView : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private DataSet RunQuery(SqlCommand sqlQuery)
        {
            string connectionString = ConfigurationManager.ConnectionStrings
                ["uKB_DB"].ConnectionString;
            SqlConnection DBConnection = new SqlConnection(connectionString);
            SqlDataAdapter dbAdapter = new SqlDataAdapter();
            dbAdapter.SelectCommand = sqlQuery;
            sqlQuery.Connection = DBConnection;
            DataSet resultsDataSet = new DataSet();
            try
            {
                dbAdapter.Fill(resultsDataSet);
            }
            catch
            {
                System.Windows.Forms.MessageBox.Show("Unable to connect to SQL Server.");
            }
            return resultsDataSet;
        }

        protected void menu_TreeNodePopulate(object sender, TreeNodeEventArgs e)
        {
            if (e.Node.ChildNodes.Count == 0)
            {
 
                PopulateCategories(e.Node);

            }

        }


        void PopulateCategories(TreeNode node)
        {
            string strsql;
            SqlCommand sqlQuery = new SqlCommand();
            DataSet resultSet = new DataSet();

            if (node.Value.Trim() == "-1")
            {
                strsql = "SELECT ID, Descricao, NULL as URL From Classificacao where ParentID is null ORDER BY Descricao ASC";
            }
            else
            {
                strsql = "SELECT Classificacao.ID as ID, Classificacao.Descricao as Descricao, NULL as URL FROM Classificacao WHERE Classificacao.ParentID = " + node.Value.Trim() +
                         " UNION " +
                         " SELECT Conteudo.ID, Conteudo.Descricao, Conteudo.URL FROM Conteudo WHERE Conteudo.ClassificacaoID = " + node.Value.Trim() + " ORDER BY Descricao ASC";
            }

            sqlQuery.CommandText = strsql;
            resultSet = RunQuery(sqlQuery);

            if (resultSet.Tables.Count > 0)
            {
                foreach (DataRow row in resultSet.Tables[0].Rows)
                {

                    TreeNode NewNode = new TreeNode();

                    if (row["URL"].ToString() == "")
                    {
                        NewNode.Text = row["Descricao"].ToString();
                        NewNode.Value = row["ID"].ToString();
                        NewNode.SelectAction = TreeNodeSelectAction.Expand;
                        NewNode.PopulateOnDemand = true;
                        NewNode.Expanded = false;

                    }
                    else
                    {
                        NewNode.Text = row["Descricao"].ToString();
                        NewNode.Value = "-1";
                        NewNode.NavigateUrl = "~/App_Data/Info_Html/" + row["URL"].ToString();
                        NewNode.SelectAction = TreeNodeSelectAction.Select;
                        NewNode.PopulateOnDemand = false;
                        

                    }
                    node.ChildNodes.Add(NewNode);
                }
            }

        }


    }
}