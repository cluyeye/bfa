﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Email_Detalhe.ascx.cs" Inherits="uTools.Controls.Email_Detalhe" %>


<!DOCTYPE html>

<html>
<head>

    <title>uEmail</title> 
    <style type="text/css">
        .auto-style1 {
        }
    </style>
</head>
<body>

    

        <asp:UpdatePanel ID="updpnl_email" runat="server">
            <ContentTemplate>

                
                    
                       
                            <asp:Panel ID="pnl_btn_accao" runat="server" Visible="false">
                                <div align="justify">
                                    <asp:ImageButton ID="btn_reply" runat="server" CssClass="img_bt" ImageUrl="~/images/reply.png" title="Responder" OnClick="btn_reply_Click" />
                                    <asp:ImageButton ID="btn_replyall" runat="server" CssClass="img_bt" ImageUrl="~/images/reply_all.png" title="Responder Todos" OnClick="btn_replyall_Click" />
                                    <asp:ImageButton ID="btn_forward" runat="server" CssClass="img_bt" ImageUrl="~/images/reply_150px.png" title="Reencaminhar" OnClick="btn_forward_Click" />
                                </div>

                            </asp:Panel>
                        
                
                            

                                <asp:HiddenField ID="hf_ID" runat="server" />
                                <table id="tbl_conteudo">
                                    <tr>
                                        <td class="auto-style1">
                                 <label for="txtbox_from">From</label><br />
                                <asp:TextBox ID="txtbox_from" runat="server" CssClass="txtInput" ></asp:TextBox>                               
                                        </td>
                                        <td rowspan="4">
                                            <ajaxToolkit:AjaxFileUpload ID="fupload_attachs" runat="server" OnUploadComplete="fupload_attachs_UploadComplete" AutoStartUpload="true" Width="100%" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="auto-style1">
                                <label for="txtbox_to">To</label><br />
                                <asp:TextBox ID="txtbox_to" runat="server" CssClass="txtInput" ></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="auto-style1">
                                <label for="txtbox_cc">BCC</label><br />
                                <asp:TextBox ID="txtbox_cc" runat="server" CssClass="txtInput" ></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="auto-style1">
                                <label for="txtbox_assunto">Assunto</label>                                
                                <asp:TextBox ID="txtbox_assunto" runat="server" CssClass="txtInput" ></asp:TextBox>                                
                                        </td>
                                    </tr>
                                   <tr>
                                        <td class="auto-style1" colspan="2">                               
                                <ajaxToolkit:HtmlEditorExtender ID="rtf_email_HtmlEditorExtender" runat="server" BehaviorID="rtf_email_HtmlEditorExtender" TargetControlID="rtf_email">
                                </ajaxToolkit:HtmlEditorExtender>
                                <asp:TextBox ID="rtf_email" CssClass="txtInput_email" runat="server" Width="90%" Height="250px"></asp:TextBox>                               
                                        </td>
                                    </tr>
                                     <tr>
                                        <td class="auto-style1">
                                
                                        </td>
                                        <td>
                                
                                            &nbsp;</td>
                                    </tr>
                                     <tr>
                                        <td class="auto-style1">
                                
                                        </td>
                                        <td>
                                
                                            &nbsp;</td>
                                    </tr>
  
                                </table>
                           </nav>

                            
                        </div>

                        <br />
                    
              

                

                    <asp:Panel ID="pnl_email_2" runat="server">
                    </asp:Panel>

                <asp:Panel ID="pnl_btn_enviar" runat="server" Visible="false">
                   
                        <asp:ImageButton ID="btn_send" runat="server" CssClass="img_bt" ImageUrl="~/images/ok.png" title="Limpa Busca" OnClick="btn_send_Click" />
                        <asp:ImageButton ID="btn_cancelar" runat="server" CssClass="img_bt" ImageUrl="~/images/close_01.png" title="Limpa Busca" OnClick="btn_cancelar_Click" />
      
                </asp:Panel>

            </ContentTemplate>
        </asp:UpdatePanel>


</body>
</html>
