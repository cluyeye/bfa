﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using Microsoft.AspNet.Identity;
using System.Net.Mail;

namespace uTools.Controls
{
    public partial class uTaskCreate : System.Web.UI.UserControl
    {

        String[] cliente = new String[12];
        //Boolean flag_clean = true;

        public String TarefaID
        {
            get
            {
                object o = ViewState["TarefaID"];
                return (o == null) ? String.Empty : (string)o;
            }

            set
            {
                ViewState["TarefaID"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //if(flag_clean == true)
            //    limpar_campos();

            //flag_clean = false;

            if (!IsPostBack)
            {
                Tools.Carregar_DropDownBox("uTasks_DB", "Select ID, Descricao from Departamento ORDER BY Descricao", dropbox_departamento);
                dropbox_departamento.SelectedIndex = 1;

                Tools.Carregar_DropDownBox("uTasks_DB", "Select ID, Descricao from [uTasks].[dbo].[Canal_Contacto] ORDER BY Descricao", dropbox_canal);

                Tools.Carregar_DropDownBox("uTasks_DB", "Select ID, Descricao from Tipificacao WHERE TipificacaoID=1 ORDER BY Descricao", dropbox_tipo);

                if (TarefaID != "") Carreagar_Email(TarefaID);
                else pnl_email_new_task.Visible = false;
            }

            Tools.Carregar_Cliente(get_selected_from_crm(), cliente);

            lbl_cliente_id.Text = cliente[0];
            lbl_cliente_nome.Text = cliente[1];



        }


        private string get_selected_from_crm()
        {
            if (this.Page.GetType().FullName == "ASP.utools_main_aspx")
                return ((utools_main)this.Page).Get_Selected_Cliente();
            else
                return string.Empty;
        }

        protected void btn_inserir_Click(object sender, EventArgs e)
        {
            
            System.Threading.Thread.Sleep(1000); 
            
            if (validar_dados() == true)
            {
                inserir_dados();
                limpar_campos();
            }
            else
            {
                StatusMessage.Text = "Operação inválida, preencha os campos correctamente.";
                //limpar_campos();
            }

        }

        private void limpar_campos()
        {
            txt_descricao.Text = "";

            dropbox_departamento.SelectedIndex = -1;

            dropbox_tipo.SelectedIndex = -1;

            for (int i = 0; i < tv_tipificacao.Nodes.Count; i++)
            {
                tv_tipificacao.Nodes[i].Checked = false;
            }

            StatusMessage.Text = "";

        }


        private void inserir_dados()
        {
            string sqlstr = string.Empty;
            string up_tarefa_id = TarefaID;
            string db = "uTasks_DB";

            /*"EXECUTE [dbo].[SP_Inserir_Cliente] '" + nome + "','" + email + "','" + telefone1 + 
                "','" + telefone2 + "','" + numero_conta + "'," + agencia_id.ToString() + "," + tipo_id;*/

            string connectionString = ConfigurationManager.ConnectionStrings[db].ConnectionString;
            SqlConnection DBConnection = new SqlConnection(connectionString);
            using (DBConnection)
            {
                string str_cliente_id = get_selected_from_crm();
                int cliente_id;
                if (str_cliente_id == string.Empty) cliente_id = -1;
                else cliente_id = int.Parse(str_cliente_id);

                if (up_tarefa_id == "")
                {
                    try
                    {
                        sqlstr = "[dbo].[SP_Inserir_Tarefa_new]";
                        SqlCommand command = new SqlCommand(sqlstr, DBConnection);
                        command.CommandType = CommandType.StoredProcedure;

                        SqlParameter param_id = command.Parameters.Add("@id", SqlDbType.Int);
                        param_id.Direction = ParameterDirection.ReturnValue;
                        command.Parameters.Add(new SqlParameter("@Descricao", txt_descricao.Text));
                        command.Parameters.Add(new SqlParameter("@DepartamentoID", int.Parse(dropbox_departamento.SelectedValue)));
                        command.Parameters.Add(new SqlParameter("@EstadoID", 1));
                        command.Parameters.Add(new SqlParameter("@SLAHoras", 48));
                        command.Parameters.Add(new SqlParameter("@TipoID", int.Parse(dropbox_tipo.SelectedValue)));
                        command.Parameters.Add(new SqlParameter("@UserID", Context.User.Identity.GetUserId()));
                        command.Parameters.Add(new SqlParameter("@ClienteID", cliente_id));

                        DBConnection.Open();
                        command.ExecuteNonQuery();

                        registar_Log();

                        //modal_text_header.InnerText = "OK buttom ";
                        //modal_text_1.InnerText = "Programming the Modal form ";
                        //modal_text_2.InnerText = "Sucessfull";

                        up_tarefa_id = param_id.Value.ToString();
                    }
                    catch (Exception ex)
                    {
                        return;
                    }
                }
                else
                {

                    sqlstr = "UPDATE [dbo].[Tarefa] SET [Descricao] = '" + txt_descricao.Text + "',[DepartamentoID] = " +
                                dropbox_departamento.SelectedValue + ",[EstadoID] = 1, [DataRegisto] = GETDATE(), [SLAHoras] = 48,[TipoID] = " +
                                dropbox_tipo.SelectedValue + ", ClienteID=" + cliente_id.ToString() + ", UserIdRegisto='" + Context.User.Identity.GetUserId() + "' WHERE ID=" + up_tarefa_id;
                    Tools.Update_Registo(db, sqlstr);
                }

                if (tv_tipificacao.CheckedNodes.Count > 0)
                    Tools.Update_Registo(db, "INSERT INTO Tarefa_Tipificacao (TarefaID, TipificacaoID) VALUES (" +
                    up_tarefa_id + "," + tv_tipificacao.CheckedNodes[0].Value + ")");

                Tools.Update_Registo(db, "INSERT INTO Tarefa_Accao (TarefaID, DepartamentoID, EstadoID, DataRegisto, UserID) VALUES (" +
                    up_tarefa_id + "," + dropbox_departamento.SelectedValue + ",1,GETDATE(),1,'" + Context.User.Identity.GetUserId() + "')");


                Tools.Update_Registo(db, "UPDATE uTasks.dbo.Tarefa SET CanalContactoID=" + dropbox_canal.SelectedValue + " WHERE ID=" + up_tarefa_id);


                if (dropbox_tipo.SelectedValue != "4") Notificar_Cliente(up_tarefa_id);

            }
            MultiView mv = this.Parent.Parent as MultiView;
            //mv.ActiveViewIndex = 0;
            mv.Visible = false;
        }


        private DataSet RunQuery(SqlCommand sqlQuery)
        {
            string connectionString = ConfigurationManager.ConnectionStrings
                ["uTasks_DB"].ConnectionString;
            SqlConnection DBConnection = new SqlConnection(connectionString);
            SqlDataAdapter dbAdapter = new SqlDataAdapter();
            dbAdapter.SelectCommand = sqlQuery;
            sqlQuery.Connection = DBConnection;
            DataSet resultsDataSet = new DataSet();
            try
            {
                dbAdapter.Fill(resultsDataSet);
            }
            catch
            {
                System.Windows.Forms.MessageBox.Show("Unable to connect to SQL Server.");
            }
            return resultsDataSet;
        }

        protected void menu_TreeNodePopulate(object sender, TreeNodeEventArgs e)
        {
            if (e.Node.ChildNodes.Count == 0)
            {

                PopulateCategories(e.Node);

            }

        }

        void PopulateCategories(TreeNode node)
        {
            string strsql;
            SqlCommand sqlQuery = new SqlCommand();
            DataSet resultSet = new DataSet();

            if (node.Value.Trim() == "-1")
            {
                strsql = "SELECT Tipificacao.ID, Tipificacao.Descricao, Childnode.TipificacaoID had_child_node FROM Tipificacao left join (SELECT TipificacaoID FROM Tipificacao GROUP BY TipificacaoID) Childnode on Tipificacao.id=Childnode.TipificacaoID where Tipificacao.TipificacaoID = 5";
            }
            else
            {
                strsql = "SELECT Tipificacao.ID, Tipificacao.Descricao, Childnode.TipificacaoID had_child_node FROM Tipificacao left join (SELECT TipificacaoID FROM Tipificacao GROUP BY TipificacaoID) Childnode on Tipificacao.id=Childnode.TipificacaoID where Tipificacao.TipificacaoID = " + node.Value.Trim();
            }

            sqlQuery.CommandText = strsql;
            resultSet = RunQuery(sqlQuery);

            if (resultSet.Tables.Count > 0)
            {
                foreach (DataRow row in resultSet.Tables[0].Rows)
                {

                    TreeNode NewNode = new TreeNode();

                    if (row["had_child_node"].ToString() != "")
                    {
                        NewNode.Text = row["Descricao"].ToString();
                        NewNode.Value = row["ID"].ToString();
                        NewNode.SelectAction = TreeNodeSelectAction.Expand;
                        NewNode.PopulateOnDemand = true;
                        NewNode.Expanded = false;

                    }
                    else
                    {
                        NewNode.Text = row["Descricao"].ToString();
                        NewNode.Value = row["ID"].ToString();
                        NewNode.ShowCheckBox = true;
                        //NewNode.SelectAction = TreeNodeSelectAction.Select;
                        NewNode.PopulateOnDemand = false;

                    }
                    node.ChildNodes.Add(NewNode);
                }
            }

        }

        protected void tv_tipificacao_TreeNodeCheckChanged(object sender, TreeNodeEventArgs e)
        {
            //lbl_tipificacao.Text = e.Node.DataPath;
        }

        protected void btn_cancelar_Click(object sender, EventArgs e)
        {

            
            System.Threading.Thread.Sleep(1000); 
            
            //modal_text_header.InnerText = "Cancel buttom ";
            //modal_text_1.InnerText = "Programming the Modal form ";
            //modal_text_2.InnerText = "Sucessfull";

            
            //utv.visible_task_create(false);
            MultiView mv = this.Parent.Parent as MultiView;
            //mv.ActiveViewIndex = 0;
            mv.Visible = false;
        }

        private void Carreagar_Email(string tarefa_id)
        {
            string db = "uTasks_DB";

            string sqlstr = "SELECT [From],[To],[CC],[Assunto],[Conteudo] " +
                "FROM [uTasks].[dbo].[Tarefa_Email] WHERE TarefaID=" + tarefa_id;
            string connectionString = ConfigurationManager.ConnectionStrings[db].ConnectionString;
            SqlConnection DBConnection = new SqlConnection(connectionString);
            using (DBConnection)
            {
                SqlCommand command = new SqlCommand(sqlstr, DBConnection);
                try
                {
                    DBConnection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        reader.Read();
                        lxt_from.Text = reader["From"].ToString();
                        lxt_to.Text = reader["To"].ToString();
                        lxt_cc.Text = reader["CC"].ToString();
                        lxt_assunto.Text = reader["Assunto"].ToString();
                        lxt_mail.Text = reader["Conteudo"].ToString().Replace(Environment.NewLine, "<br/>");
                    }
                    reader.Close();
                    pnl_email_new_task.Visible = true;
                }
                catch (Exception er)
                {

                }

            }
        }

        protected void Notificar_Cliente(string taskid)
        {
            string db = "uTasks_DB";

            string sqlstr = "SELECT Tarefa.ID, Tipo.Descricao Tipo, cliente.Email FROM Tarefa "
                + "LEFT JOIN Tipificacao Tipo on Tipo.id=Tarefa.TipoID "
                + "LEFT JOIN uCRM.dbo.Cliente cliente On cliente.id=Tarefa.ClienteID "
                + "WHERE Tarefa.ID=" + taskid;

            string connectionString = ConfigurationManager.ConnectionStrings[db].ConnectionString;
            SqlConnection DBConnection = new SqlConnection(connectionString);

            string email = string.Empty;
            string tipo = string.Empty;
            string tipificacao = string.Empty;

            using (DBConnection)
            {
                SqlCommand command = new SqlCommand(sqlstr, DBConnection);
                try
                {
                    DBConnection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        reader.Read();
                        email = reader["Email"].ToString();
                        tipo = reader["Tipo"].ToString();
                    }
                    reader.Close();
                }


                catch (Exception er)
                {
                    return;
                }

                try
                {
                    MailMessage mail_message = new MailMessage();
                    SmtpClient smtpclient = new SmtpClient("srv-tl-ex01.ucall.co.ao", 25);

                    mail_message.To.Add(email);
                    mail_message.From = new MailAddress("bfa@bfa.ao");
                    mail_message.Subject = "[BFATCK" + taskid + "] " + tipo + " - " + Get_Tipificacao(taskid);

                    string str_bna = string.Empty;

                    if (tipo == "Reclamação")
                        str_bna = "Caso deseje efectuar reclamação junto do Banco Nacional de Angola, poderá  faze-lo através do Portal da Provedoria do Cliente Bancário em www.provedoriadoclientebancario.bna.ao <br/><br/>";

                    string body = "Caro(a) Cliente,<br/><br/>Na sequência do seu contacto para a Linha de Atendimento BFA, informamos que a sua situação ficou registada. Para qualquer questão referente a este assunto por favor indique o número [" + taskid + "].<br/><br/>"
                                            + "Contamos responder-lhe o mais rapidamente possível.<br/><br/>"
                                            + str_bna
                                            + "Estamos disponíveis para futuros esclarecimentos.<br/><br/>"
                                            + "Os nossos cumprimentos,<br/><br/>"
                                            + "Serviço de Apoio ao Cliente <br/>"
                                            + "BFA <br/>"
                                            + "Rua Amílcar Cabral, 58, Maianga – Luanda<br/>"
                                            + "Linha de Atendimento BFA: 923 120 120  <br/>"
                                            + "bfa@bfa.ao<br/>"
                                            + "www.bfa.ao <br/>"
                                            + "<img src='cid:signature' />";


                    mail_message.IsBodyHtml = true;


                    AlternateView htmlView = AlternateView.CreateAlternateViewFromString(body, null, System.Net.Mime.MediaTypeNames.Text.Html);

                    string signature_filepath = HttpContext.Current.Server.MapPath("~/") + "signature_pic_1.jpg";

                    System.Net.Mail.LinkedResource signature_img = new LinkedResource(signature_filepath);

                    signature_img.ContentId = "signature";

                    htmlView.LinkedResources.Add(signature_img);

                    mail_message.AlternateViews.Add(htmlView);

                    smtpclient.Send(mail_message);
                }
                catch (Exception ex)
                {
                    //lbl_send_email.Text = "Erro a enviar email"; 
                }
            }

        }

        private string Get_Tipificacao(string taskid)
        {
            try
            {
                string str_tipificacao = string.Empty;
                string db = "uTasks_DB";
                string connectionString = ConfigurationManager.ConnectionStrings[db].ConnectionString;
                SqlConnection DBConnection = new SqlConnection(connectionString);
                DBConnection.Open();

                string sqlstr = "SELECT TipificacaoID FROM Tarefa_Tipificacao WHERE TarefaID=" + taskid;
                SqlCommand command = new SqlCommand(sqlstr, DBConnection);
                SqlDataReader reader = command.ExecuteReader();

                List<int> tipificacoes = new List<int>();

                while (reader.Read())
                {
                    tipificacoes.Add(reader.GetInt32(0));
                }
                reader.Close();


                foreach (int tipificacao in tipificacoes)
                {
                    //SqlCommand cmdTipificacoes = new SqlCommand("exec Niveis_Tipificacoes @TipificacaoID=" + reader["TipificacaoID"], DBConnection);
                    command.CommandText = "SP_Niveis_Tipificacoes";
                    command.Parameters.Add(new SqlParameter("@TipificacaoID", tipificacao));
                    command.CommandType = CommandType.StoredProcedure;
                    //cmdTipificacoes.CommandType = CommandType.StoredProcedure;                        
                    //SqlDataReader readerniveistipificacao =  command.ExecuteReader();
                    reader = command.ExecuteReader();
                    reader.Read();
                    while (reader.Read()) str_tipificacao = str_tipificacao + "\\" + reader["Descricao"];
                    reader.Close();
                }

                return str_tipificacao;
            }
            catch (Exception excp)
            {
                return string.Empty;
            }

        }

        private Boolean validar_dados()
        {
            int departamento = int.Parse(dropbox_departamento.SelectedValue);
            int tipo = int.Parse(dropbox_tipo.SelectedValue);

            
            if (lbl_cliente_id.Text == "")
                return false;

            if(txt_descricao.Text == "")
                return false;
            
            if(departamento <= 0)
                return false;
            
            if (tipo <= 0)
                return false;

            if (tv_tipificacao.CheckedNodes.Count != 1)
                return false;

            return true;
        }


        private void registar_Log()
        {
            String[] log = new String[15];

            log[0] = "" + Context.User.Identity.GetUserId();
            log[1] = "" + DateTime.Now.ToString();
            log[2] = "uTasks.dbo.SP_Inserir_Tarefa_new";

            log[3] = "" + 3;
            log[4] = "" + 4;

            log[5] = "" + get_selected_from_crm();
            log[6] = Tools.Load_Last_uTasksCreate(Context.User.Identity.GetUserId());


            Tools.registar_Log(log);
        }

    }
}