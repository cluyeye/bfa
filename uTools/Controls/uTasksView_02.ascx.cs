﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace uTools.Controls
{
    public partial class uTasksView_02 : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Populate_Lista_Tarefas();
                Tools.Carregar_DropDownBox("uTasks_DB", "Select ID, Descricao from Departamento ORDER BY Descricao", dropbox_departamento);
                Tools.Carregar_DropDownBox("uTasks_DB", "Select ID, Descricao from Estado ORDER BY Descricao", dropbox_estado);
                Tools.Carregar_DropDownBox("uTasks_DB", "SELECT ID, Descricao FROM Tipificacao WHERE TipificacaoID=1 ORDER BY Descricao", dropbox_tipo);
                Tools.Carregar_DropDownBox("uTasks_DB", "Select ID, Descricao from Estado ORDER BY Descricao", dropbox_estado_edit);
                Tools.Carregar_DropDownBox("uTasks_DB", "Select ID, Descricao from Departamento ORDER BY Descricao", dropbox_departamento_edit);
                Tools.Carregar_DropDownBox("uTasks_DB", "Select ID, Descricao from Departamento ORDER BY Descricao", dropbox_detalhe_departamento);
                Tools.Carregar_DropDownBox("uTasks_DB", "Select ID, Descricao from Estado ORDER BY Descricao", dropbox_detalhe_estado);
            }
        }


        private void Populate_Lista_Tarefas()
        {
            string sqlstr = "SELECT Tarefa.ID, (LEFT(Tarefa.Descricao, 70) + '...') Descricao, Tarefa.DataRegisto, " +
                "Tarefa.DataFecho, Tarefa.SLAHoras, Estado.Descricao Estado, Departamento.Descricao Departamento, Tipificacao.Descricao Tipo, Tarefa.SLAHoras, (Tarefa.SLAHoras - DATEDIFF(HH, Tarefa.DataRegisto, ISNULL(Tarefa.DataFecho, GETDATE()))) diferenca_SLA " +
                "FROM Tarefa " +
                "LEFT JOIN Estado ON Tarefa.EstadoID=Estado.ID " +
                "LEFT JOIN Departamento ON Tarefa.DepartamentoID=Departamento.ID " +
                "LEFT JOIN Tipificacao ON Tarefa.TipoID=Tipificacao.ID ";

            Tools.Carregar_GridView("uTasks_DB", sqlstr, gridview_tarefas);


        }

        protected void gridview_tarefas_Sorting(object sender, GridViewSortEventArgs e)
        {
            string sqlstr = "SELECT Tarefa.ID, (LEFT(Tarefa.Descricao, 70) + '...') Descricao, Tarefa.DataRegisto, " +
                "Tarefa.DataFecho, Tarefa.SLAHoras, Estado.Descricao Estado, Departamento.Descricao Departamento, Tipificacao.Descricao Tipo, Tarefa.SLAHoras, (Tarefa.SLAHoras - DATEDIFF(HH, Tarefa.DataRegisto, ISNULL(Tarefa.DataFecho, GETDATE()))) diferenca_SLA " +
                "FROM Tarefa " +
                "LEFT JOIN Estado ON Tarefa.EstadoID=Estado.ID " +
                "LEFT JOIN Departamento ON Tarefa.DepartamentoID=Departamento.ID " +
                "LEFT JOIN Tipificacao ON Tarefa.TipoID=Tipificacao.ID " +
                "ORDER BY " + e.SortExpression + " ASC";

            Tools.Carregar_GridView("uTasks_DB", sqlstr, gridview_tarefas);

        }

        protected void gridview_tarefas_SelectedIndexChanged(object sender, EventArgs e)
        {
            //label_status.Text = "changed index " + gridview_tarefas.SelectedIndex.ToString();
            Update_Detalhe_Tarefa(gridview_tarefas.Rows[gridview_tarefas.SelectedIndex].Cells[1].Text);
        }

        private void Update_Detalhe_Tarefa(string id)
        {
            string db = "uTasks_DB";

            string sqlstr = "SELECT Tarefa.ID, Tarefa.Descricao, Tarefa.Resumo, Tarefa.ResolucaoResumo, Tarefa.DataRegisto, " +
                "Tarefa.DataFecho, Tarefa.SLAHoras, Tarefa.EstadoID, Estado.Descricao Estado, Tarefa.DepartamentoID, Departamento.Descricao Departamento, Tipificacao.Descricao Tipo, Tarefa.SLAHoras, (Tarefa.SLAHoras - DATEDIFF(HH, Tarefa.DataRegisto, ISNULL(Tarefa.DataFecho, GETDATE()))) diferenca_SLA " +
                "FROM Tarefa " +
                "LEFT JOIN Estado ON Tarefa.EstadoID=Estado.ID " +
                "LEFT JOIN Departamento ON Tarefa.DepartamentoID=Departamento.ID " +
                "LEFT JOIN Tipificacao ON Tarefa.TipoID=Tipificacao.ID " +
                "WHERE Tarefa.ID=" + id;
            string connectionString = ConfigurationManager.ConnectionStrings
                [db].ConnectionString;
            SqlConnection DBConnection = new SqlConnection(connectionString);
            using (DBConnection)
            {
                SqlCommand command = new SqlCommand(sqlstr, DBConnection);
                try
                {
                    DBConnection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        reader.Read();
                        lbl_ID.Text = reader["ID"].ToString();
                        
                        dropbox_detalhe_departamento.SelectedValue = reader["DepartamentoID"].ToString();
                        dropbox_detalhe_estado.SelectedValue = reader["EstadoID"].ToString();
                        
                        lbl_tipo.Text = reader["Tipo"].ToString();
                        lbl_descricao.Text = reader["Descricao"].ToString();
                        lbl_resumo.Text = reader["Resumo"].ToString();
                        lbl_resolucao.Text = reader["ResolucaoResumo"].ToString();
                        lbl_registo.Text = reader["DataRegisto"].ToString();
                        lbl_fecho.Text = reader["DataFecho"].ToString();
                        lbl_sla.Text = reader["SLAHoras"].ToString() + "(" + reader["diferenca_SLA"].ToString() + ")";
                    }
                    reader.Close();
                    sqlstr = "SELECT Departamento.Descricao Departamento, Estado.Descricao Estado, Tarefa_Accao.DataRegisto, Tarefa_Accao.UserID FROM Tarefa_Accao " +
                                "LEFT JOIN Estado ON Tarefa_Accao.EstadoID=Estado.ID " +
                                "LEFT JOIN Departamento ON Tarefa_Accao.DepartamentoID=Departamento.ID " +
                                "WHERE Tarefa_Accao.TarefaID=" + id + " ORDER BY Tarefa_Accao.DataRegisto ASC";
                    command.CommandText = sqlstr;
                    reader = command.ExecuteReader();
                    gridview_historico.DataSource = reader;
                    gridview_historico.DataBind();
                    reader.Close();

                    sqlstr = "SELECT Comentario, DataRegisto, UserID FROM Tarefa_Comentario where Tarefa_Comentario.ID=" + id +
                                " ORDER BY DataRegisto ASC";
                    command.CommandText = sqlstr;
                    reader = command.ExecuteReader();
                    gridview_comentario.DataSource = reader;
                    gridview_comentario.DataBind();
                    reader.Close();

                    sqlstr = "SELECT TipificacaoID FROM Tarefa_Tipificacao WHERE TarefaID=" + id;
                    command.CommandText = sqlstr;
                    reader = command.ExecuteReader();
                    List<int> tipificacoes = new List<int>();

                    while (reader.Read())
                    {
                        tipificacoes.Add(reader.GetInt32(0));
                    }
                    reader.Close();

                    foreach (int tipificacao in tipificacoes)
                    {
                        //SqlCommand cmdTipificacoes = new SqlCommand("exec Niveis_Tipificacoes @TipificacaoID=" + reader["TipificacaoID"], DBConnection);
                        command.CommandText = "Niveis_Tipificacoes";
                        command.Parameters.Add(new SqlParameter("@TipificacaoID", tipificacao));
                        command.CommandType = CommandType.StoredProcedure;
                        //cmdTipificacoes.CommandType = CommandType.StoredProcedure;                        
                        //SqlDataReader readerniveistipificacao =  command.ExecuteReader();
                        reader = command.ExecuteReader();
                        while (reader.Read()) lbl_tipificacao.Text = lbl_tipificacao.Text + "\\" + reader["Descricao"];
                        reader.Close();
                    }
                }
                catch (Exception erro)
                {
                    //label_status.Text = "ERRO : " + erro.Message;

                }
            }
        }

        protected void btn_filtro_Click(object sender, ImageClickEventArgs e)
        {


            string where = string.Empty;
            string sqlstr = string.Empty;

            if (txtbox_id.Text.Length > 0) where = " Tarefa.ID = " + txtbox_id.Text.Trim() + " AND ";
            if (txtbox_descricao.Text.Length > 0) where = where + " Tarefa.Descricao LIKE '%" + txtbox_descricao.Text + "%' AND ";
            if (dropbox_tipo.SelectedIndex != 0) where = where + " Tarefa.TipoID = " + dropbox_tipo.SelectedValue + " AND ";
            if (dropbox_departamento.SelectedIndex != 0) where = where + " Tarefa.DepartamentoID = " + dropbox_departamento.SelectedValue + " AND ";
            if (dropbox_estado.SelectedIndex != 0) where = where + " Tarefa.EstadoID = " + dropbox_estado.SelectedValue + " AND ";
            if (txtbox_dataregisto_inicio.Text.Length > 0)
            {
                if (txtbox_dataregisto_fim.Text.Length > 0) where = where + " Tarefa.DataRegisto BETWEEN '" + txtbox_dataregisto_inicio.Text + "' AND '" + txtbox_dataregisto_fim.Text + "' AND ";
            }
            if (txtbox_datafecho_inicio.Text.Length > 0)
            {
                if (txtbox_datafecho_fim.Text.Length > 0) where = where + " Tarefa.DataFecho BETWEEN '" + txtbox_datafecho_inicio.Text + "' AND '" + txtbox_datafecho_fim.Text + "' AND ";
            }


            if (where.Length > 0)
                sqlstr = "SELECT Tarefa.ID, (LEFT(Tarefa.Descricao, 70)) Descricao, Tarefa.DataRegisto, " +
                "Tarefa.DataFecho, Tarefa.SLAHoras, Estado.Descricao Estado, Departamento.Descricao Departamento, Tipificacao.Descricao Tipo, Tarefa.SLAHoras, (Tarefa.SLAHoras - DATEDIFF(HH, Tarefa.DataRegisto, ISNULL(Tarefa.DataFecho, GETDATE()))) diferenca_SLA " +
                "FROM Tarefa " +
                "LEFT JOIN Estado ON Tarefa.EstadoID=Estado.ID " +
                "LEFT JOIN Departamento ON Tarefa.DepartamentoID=Departamento.ID " +
                "LEFT JOIN Tipificacao ON Tarefa.TipoID=Tipificacao.ID " +
                "WHERE " + where.Substring(0, where.Length - 4);
            else
                sqlstr = "SELECT Tarefa.ID, (LEFT(Tarefa.Descricao, 70) Descricao, Tarefa.DataRegisto, " +
                "Tarefa.DataFecho, Tarefa.SLAHoras, Estado.Descricao Estado, Departamento.Descricao Departamento, Tipificacao.Descricao Tipo, Tarefa.SLAHoras, (Tarefa.SLAHoras - DATEDIFF(HH, Tarefa.DataRegisto, ISNULL(Tarefa.DataFecho, GETDATE()))) diferenca_SLA " +
                "FROM Tarefa " +
                "LEFT JOIN Estado ON Tarefa.EstadoID=Estado.ID " +
                "LEFT JOIN Departamento ON Tarefa.DepartamentoID=Departamento.ID" +
                "LEFT JOIN Tipificacao ON Tarefa.TipoID=Tipificacao.ID";

            Tools.Carregar_GridView("uTasks_DB", sqlstr, gridview_tarefas);
        }

        protected void btn_clearfiltro_Click(object sender, ImageClickEventArgs e)
        {
            txtbox_id.Text = "";
            txtbox_descricao.Text = "";
            txtbox_dataregisto_inicio.Text = "";
            txtbox_dataregisto_fim.Text = "";
            txtbox_datafecho_inicio.Text = "";
            txtbox_datafecho_fim.Text = "";
            dropbox_tipo.SelectedIndex = 0;
            dropbox_departamento.SelectedIndex = 0;
            dropbox_estado.SelectedIndex = 0;

            Populate_Lista_Tarefas();
        }

        protected void btn_modal_action_Click(object sender, ImageClickEventArgs e)
        {
            string sqlstr = string.Empty;
            switch (hidden_modal_action.Value)
            {
                case "estado":
                    sqlstr = "INSERT INTO Tarefa_Accao (TarefaID, DepartamentoID, EstadoID, DataRegisto, UserID) VALUES (" + lbl_ID.Text + "," + dropbox_detalhe_departamento.SelectedValue + "," + dropbox_estado_edit.SelectedValue + ",GETDATE(),1)";
                    Tools.Update_Registo("uTasks_DB", sqlstr);
                    break;
                case "departamento":
                    string sqlstr_ins = "INSERT INTO Tarefa_Accao (TarefaID, DepartamentoID, EstadoID, DataRegisto, UserID) VALUES (" + lbl_ID.Text + "," + dropbox_departamento_edit.SelectedValue + ",1,GETDATE(),1)";
                    string sqlstr_upd = "UPDATE Tarefa SET DepartamentoID=" + dropbox_departamento_edit.SelectedValue + " AND EstadoID=1 WHERE ID=" + lbl_ID.Text;
                    if (Tools.Update_Registo("uTasks_DB", sqlstr_ins))
                        Tools.Update_Registo("uTasks_DB", sqlstr_upd);
                    break;
                default:
                    sqlstr = "INSERT INTO Tarefa_Comentario (TarefaID, Comentario, DataRegisto, UserID) VALUES (" + lbl_ID.Text + ",'" + txtbox_comentario_edit.Text + "',GETDATE(),1)";
                    Tools.Update_Registo("uTasks_DB", sqlstr);
                    break;
            }
            Update_Detalhe_Tarefa(lbl_ID.Text);
        }



    }
}