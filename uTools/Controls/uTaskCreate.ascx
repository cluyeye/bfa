﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="uTaskCreate.ascx.cs" Inherits="uTools.Controls.uTaskCreate" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>uTasks Create</title>

<style>
    @font-face {
        font-family: ephismere;
        src: url('../fontes/Styling W00 Regular.ttf');
    }

    div {
        margin: 0;
        padding: 0;
    }


    .img_bt {
        width: 40px;
        padding: 0;
        background-color: transparent;
    }


    #panel_detalhe_section, #panel_detalhe_bottom {
        /*border: 2px solid #0026ff;*/
        max-height: none;
    }

    #container_task_create {
        width: 100%;
        min-height: 0px;
        max-height: none;

        background-color: #e6e7e8;
    }


    #content_ut_create_treeview {
        float: left;
        width: 35%;
        height: 230px;
        background-color: #ffffff;
        border-radius: 4px;
        /*border: 1px solid #e6e7e8;*/
        padding: 0;
        margin-right: 60px;
        overflow: auto;
        margin-left: 0;
        margin-top: 0;
        margin-bottom: 0;
    }


    #tv_tipificacao {
        font-family: 'ephismere';
        margin: 0;
    }


    #content_ut_create {

        max-height: none;
        margin: 0;
        overflow: hidden;
        margin-left: 100px;
    }

    .ut_create_table {
        width: 100%;
        border: none;
    }

    .ut_create_table tr
    {
        width: 50%;
    }


    #content_ut_create_email .describe {
        font-size: 14px;
        font-weight: bold;
    }
    .editdescricao {}

    #content_ut_create .txtInput {
        border: 1px solid #e6e7e8;
        font-family: 'ephismere';
        border: 1px solid #e6e7e8;
        width: 155px;
        border-radius: 4px;
        height: 15px;
    }


</style>

</head>
<body>

<div class="container_task_create">
    <br />
    <br />
    <nav id="content_ut_create_treeview">

        <asp:TreeView ID="tv_tipificacao" runat="server" OnTreeNodePopulate="menu_TreeNodePopulate" ImageSet="Simple" OnTreeNodeCheckChanged="tv_tipificacao_TreeNodeCheckChanged">
            <HoverNodeStyle Font-Underline="True" ForeColor="#5555DD" />
            <Nodes>
                <asp:TreeNode PopulateOnDemand="True" Text="TIPIFICAÇÕES" Value="-1"></asp:TreeNode>
            </Nodes>

            <NodeStyle Font-Names="ephismere" Font-Size="8pt" ForeColor="#58595b" HorizontalPadding="0px" NodeSpacing="0px" VerticalPadding="0px" />
            <ParentNodeStyle Font-Bold="False" />
            <SelectedNodeStyle Font-Underline="True" HorizontalPadding="0px" VerticalPadding="0px" ForeColor="#5555DD" />

        </asp:TreeView>

    </nav>


    <article id="content_ut_create">

        <table class="ut_create_table">
            <tr>
                <td>
                    <label for="dropbox_tipo" class="head_detalhe">Tipo*</label><br />
                    <asp:DropDownList ID="dropbox_tipo" CssClass="selectInput" runat="server">
                    </asp:DropDownList>
                </td>

                <td class="auto-style2">
                    <label for="dropbox_departamento" class="head_detalhe">Departamento*</label><br />
                    <asp:DropDownList ID="dropbox_departamento" CssClass="selectInput" runat="server">
                    </asp:DropDownList>
                </td>
			</tr>
			<tr>
				<td>
                    <label for="dropbox_tipo" class="head_detalhe">Canal de Contacto*</label><br />
                    <asp:DropDownList ID="dropbox_canal" CssClass="selectInput" runat="server">
                    </asp:DropDownList>
                </td>

                <td>
                    <label for="txt_cliente_id" class="head_detalhe">Cliente*</label><br />

                    <span class="head_detalhe">ID  : </span>
                    <asp:Label ID="lbl_cliente_id" runat="server"></asp:Label><br />
                    <span class="head_detalhe">Nome : </span>
                    <asp:Label ID="lbl_cliente_nome" runat="server"></asp:Label>


                    <%--<asp:TextBox ID="txt_cliente_id" runat="server" TextMode="Number" />--%>
                </td>

            </tr>

        </table>


        <label for="lxt_descricao" class="head_detalhe">Descrição*</label><br />
        <asp:TextBox ID="txt_descricao" runat="server" TextMode="MultiLine" Rows="6" Width="97%" Columns="70" CssClass="editdescricao"></asp:TextBox>

        <asp:Panel ID="pnl_email_new_task" runat="server">
            <div id="
                _email">
                <span class="txt_Titulo">EMAIL</span><br />
                <hr />

                <table class="ut_create_table">
                    <tr>
                        <td>
                            <label for="lxt_from" class="head_detalhe">From</label><br />
                            <asp:Label ID="lxt_from" runat="server"></asp:Label>

                        </td>

                        <td>
                            <label for="lxt_to" class="head_detalhe">To</label><br />
                            <asp:Label ID="lxt_to" runat="server"></asp:Label>

                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label for="lxt_cc" class="head_detalhe">Cc</label><br />
                            <asp:Label ID="lxt_cc" runat="server"></asp:Label>

                        </td>

                        <td>
                            <label for="lxt_assunto" class="head_detalhe">Assunto</label><br />
                            <asp:Label ID="lxt_assunto" CssClass="describe" runat="server"></asp:Label>

                        </td>
                    </tr>
                </table>

                <br />

                <label for="lxt_mail" class="head_detalhe">Email</label><br />
                <asp:Label ID="lxt_mail" runat="server"></asp:Label>
            </div>
        </asp:Panel>

        <p>
            <asp:Literal runat="server" ID="StatusMessage" />
        </p>


<%--            <asp:ImageButton ID="" runat="server" ImageUrl="../images/ok.png" OnClick="btn_inserir_Click" CssClass="img_bt" title="Salvar Ticket" />
            <asp:ImageButton ID="" runat="server" ImageUrl="../images/close_01.png" CssClass="img_bt" OnClick="btn_cancelar_Click" title="Cancelar Ticket" />
--%>

            <asp:Button ID="btn_inserir" CssClass="btn_form" runat="server" title="Salvar Ticket" Text="Salvar" OnClick="btn_inserir_Click" OnClientClick="element = 'btn_inserir'" />
            <asp:Button ID="btn_cancelar" CssClass="btn_form" runat="server" title="Cancelar Ticket" Text="Cancelar" OnClick="btn_cancelar_Click" OnClientClick="element = 'btn_cancelar'" />

    </article>

</div>

    <!-- The Modal -->
<%--    <div id="myModal" class="modal">

        <!-- Modal content -->
        <div class="modal-content">
            <div class="modal-header">
                <h2 id="modal_text_header" runat="server">Modal Header</h2>
            </div>
            <div class="modal-body">
                <span id="modal_text_1" runat="server">Teste utools</span><br />
                <span id="modal_text_2" runat="server">Teste utools</span><br />
                <span id="modal_text_3" runat="server">Teste utools</span><br />
                <span id="modal_text_4" runat="server">Teste utools</span>
            </div>
            <div class="modal-footer" runat="server">
                <span class="close"><img id="modal_img_close" src="images/ok.png" /></span>
                <h3 id="modal_text_footer">Modal Footer</h3>
            </div>
        </div>

    </div>--%>

    <script>
        // Get the modal
        var element = '';

        var modal = document.getElementById('myModal');
        
        // Get the button that opens the modal
        var btn = document.getElementById("btn_cancelar");

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];

        // When the user clicks the button, open the modal 
        btn.onclick = function () {
            modal.style.display = "block";
        }

        // When the user clicks on <span> (x), close the modal
        span.onclick = function () {
            modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function (event) {
            //if (event.target == modal) {
            //    modal.style.display = "none";
            //}
        }
    </script>

    
</body>
</html>

