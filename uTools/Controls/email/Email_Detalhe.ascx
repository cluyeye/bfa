﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Email_Detalhe.ascx.cs" Inherits="uTools.Controls.Email_Detalhe" %>


<!DOCTYPE html>

<html>
<head>

    <title>uEmail</title> 
</head>
<body>

    <div id="container_email">

        <asp:UpdatePanel ID="updpnl_email" runat="server">
            <ContentTemplate>

                <div id="detalhe_email_main">
                    <asp:Panel ID="pnl_mail_1" runat="server">

                        <div id="detalhe_info_email">
                            <asp:Panel ID="pnl_btn_accao" runat="server">
                                <div align="justify">
                                    <asp:ImageButton ID="btn_reply" runat="server" CssClass="img_bt" ImageUrl="~/imagens/reply.png" alt="Responder" OnClick="btn_reply_Click" />
                                    <asp:ImageButton ID="btn_replyall" runat="server" CssClass="img_bt" ImageUrl="~/imagens/reply_all.png" alt="Responder Todos" OnClick="btn_replyall_Click" />
                                    <asp:ImageButton ID="btn_forward" runat="server" CssClass="img_bt" ImageUrl="~/imagens/reply_150px.png" alt="Reencaminhar" OnClick="btn_forward_Click" />
                                </div>

                            </asp:Panel>

                        </div>

                        <div id="info_email">
                            <nav id="content_details_email">

                                <asp:HiddenField ID="hf_ID" runat="server" />

                                <label for="txtbox_from">From</label><br />
                                <asp:TextBox ID="txtbox_from" runat="server" CssClass="txtInput" ></asp:TextBox>
                                <br />
                                <label for="txtbox_to">To</label><br />
                                <asp:TextBox ID="txtbox_to" runat="server" CssClass="txtInput" ></asp:TextBox>
                                <br />
                                <label for="txtbox_cc">BCC</label><br />
                                <asp:TextBox ID="txtbox_cc" runat="server" CssClass="txtInput" ></asp:TextBox>
                                <br />
                                <label for="txtbox_assunto">Assunto</label><br />
                                <asp:TextBox ID="txtbox_assunto" runat="server" CssClass="txtInput" ></asp:TextBox>

                            </nav>

                            <div id="content_upload_email">

                                <ajaxToolkit:AjaxFileUpload ID="fupload_attachs" runat="server" OnUploadComplete="fupload_attachs_UploadComplete" AutoStartUpload="true" Width="100%" />
                                <ajaxToolkit:HtmlEditorExtender ID="rtf_email_HtmlEditorExtender" runat="server" BehaviorID="rtf_email_HtmlEditorExtender" TargetControlID="rtf_email">
                                </ajaxToolkit:HtmlEditorExtender>

                            </div>
                        </div>

                        <br />
                    </asp:Panel>
                </div>

                <div id="email_body">

                    <asp:Panel ID="pnl_email_2" runat="server">
                        <asp:TextBox ID="rtf_email" CssClass="txtInput_email" runat="server" Width="90%" Height="250px"></asp:TextBox>
                    </asp:Panel>
                </div>

                    <br />

                <asp:Panel ID="pnl_btn_enviar" runat="server">
                    <div align="justify">
                        <asp:ImageButton ID="btn_send" runat="server" CssClass="img_bt" ImageUrl="~/imagens/ok.png" alt="Limpa Busca" OnClick="btn_send_Click" />
                        <asp:ImageButton ID="btn_cancelar" runat="server" CssClass="img_bt" ImageUrl="~/imagens/close_01.png" alt="Limpa Busca" OnClick="btn_cancelar_Click" />

                    </div>
                </asp:Panel>

                </div>

            </ContentTemplate>
        </asp:UpdatePanel>



    </div>



</body>
</html>
