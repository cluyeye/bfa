﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.IO;
using System.Configuration;
using System.Data.SqlClient;
using Microsoft.AspNet.Identity;


namespace uTools.Controls
{
    public partial class Email_Detalhe : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if(!IsPostBack)
            {
                
            }
            

        }


        protected void btn_send_Click(object sender, EventArgs e)
        {
            Send_Email();
            this.Parent.Visible = false;
            //Clear_Email_Detail();                        
        }

        

        protected void Send_Email()
        {
            try
            {
                MailMessage mail_message = new MailMessage();
                SmtpClient smtpclient = new SmtpClient("srv-tl-ex01.ucall.co.ao", 25);

                mail_message.To.Add(txtbox_to.Text);
                mail_message.From = new MailAddress(txtbox_from.Text);
                mail_message.Subject = txtbox_assunto.Text;
                mail_message.IsBodyHtml = true;
                mail_message.Body = rtf_email.Text;

                string[] attatch_files = Directory.GetFiles(HttpContext.Current.Server.MapPath("~/App_Data/MailAttachs/"));

                foreach (string file in attatch_files)
                {
                    mail_message.Attachments.Add(new Attachment(file));
                }

                smtpclient.Send(mail_message);

                //lbl_send_email.Text = "Email enviado";

                string tarefa_id = hf_ID.Value;

                string sqlins = "INSERT INTO Tarefa_Email (TarefaID, [From], [To], Assunto, Conteudo, DataEmail, TipoID, DataRegisto, UserID) VALUES (" +
                    tarefa_id + ",'" + txtbox_from.Text + "','" + txtbox_to.Text + "','" + txtbox_assunto.Text + "','" +
                    rtf_email.Text + "',GETDATE(),10,GETDATE(),'" + Context.User.Identity.GetUserId() + "')";

                Tools.Update_Registo("uTasks_DB", sqlins);
            }
            catch (Exception ex)
            {
                //lbl_send_email.Text = "Erro a enviar email"; 
            }
        }

        protected void fupload_attachs_UploadComplete(object sender, AjaxControlToolkit.AjaxFileUploadEventArgs e)
        {
            try
            {
                string filename = HttpContext.Current.Server.MapPath("~/App_Data/MailAttachs/" + e.FileName);
                fupload_attachs.SaveAs(filename);    
            } catch (Exception ex)
            {
                
            }
                    
        }

        protected void btn_reply_Click(object sender, EventArgs e)
        {
            txtbox_to.Text = txtbox_from.Text;
            txtbox_from.Text = "teste@ucall.co.ao";
            txtbox_cc.Text = "";

            pnl_btn_accao.Enabled = false;
            pnl_btn_enviar.Enabled = true;   
        }

        protected void btn_replyall_Click(object sender, EventArgs e)
        {
            txtbox_to.Text = txtbox_to.Text + " ; " + txtbox_from.Text;
            txtbox_from.Text = "teste@ucall.co.ao";

            pnl_btn_accao.Enabled = false;
            pnl_btn_enviar.Enabled = true;            
        }

        protected void btn_forward_Click(object sender, EventArgs e)
        {
            txtbox_to.Text = "";
            txtbox_from.Text = "teste@ucall.co.ao";
            txtbox_cc.Text = "";

            pnl_btn_accao.Enabled = false;
            pnl_btn_enviar.Enabled = true;   
        }

        public void Show_Email_Detail(string email_id)
        {
            string db = "uTasks_DB";

            string sqlstr = "SELECT [From], [To], [Assunto], [Conteudo], TarefaID FROM Tarefa_Email WHERE ID=" + email_id;

            string connectionString = ConfigurationManager.ConnectionStrings
                [db].ConnectionString;
            SqlConnection DBConnection = new SqlConnection(connectionString);
            using (DBConnection)
            {
                SqlCommand command = new SqlCommand(sqlstr, DBConnection);
                try
                {
                    DBConnection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        reader.Read();
                        hf_ID.Value = reader["TarefaID"].ToString();
                        txtbox_from.Text = reader["From"].ToString();
                        txtbox_to.Text = reader["To"].ToString();
                        txtbox_assunto.Text = reader["Assunto"].ToString();
                        rtf_email.Text = Signature() + reader["Conteudo"].ToString();
                    }
                }
                catch (Exception ex) { }
            }
        }

        protected void Clear_Email_Detail()
        {
            //txtbox_from.Text = "";
            //txtbox_to.Text = "";
            //txtbox_assunto.Text = "";
            //rtf_email.Text = "";
            this.Parent.Visible = false;
        }

        protected void btn_cancelar_Click(object sender, EventArgs e)
        {
            Clear_Email_Detail();
            pnl_btn_accao.Enabled = true;
            pnl_btn_enviar.Enabled = false;
        }

        public void Carregar_Info_Body(string body, string assunto, string tarefa_id)
        {
            hf_ID.Value = tarefa_id;
            rtf_email.Text = Signature() + body;
            txtbox_assunto.Text = assunto;
            pnl_btn_accao.Visible = false;
            pnl_btn_enviar.Visible = false;
        }

        public void Send_Email_Outside_Comd()
        {
            Send_Email();
        }

        private string Signature ()
        {
            string sig_jpg_file= "~/App_Data/MailSignature/signature_pic_1.jpg";
            string signature = "<br/><br/><br/><br/><br/><b>Serviço de Apoio ao Cliente<br/>BFA</b><br/>Rua Amílcar Cabral, 58, Maianga – Luanda<br/>" +
                "Linha de Atendimento BFA: 923 120 120<br/>bfa@bfa.ao<br/>www.bfa.ao<br/>" +
                "<img src='signature_pic_1.jpg' /><br/><br/>";

            return signature;
        }
    }
}