﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Web.Configuration;
using System.IO;
using uTools.dao;


namespace uTools
{
    public class Tools
    {

        public static int MAX_Cliente = 12;


        public static bool Carregar_DropDownBox(string db, string sqlstr, DropDownList dropdownlist)
        {
            string connectionString = ConfigurationManager.ConnectionStrings
                [db].ConnectionString;
            SqlConnection DBConnection = new SqlConnection(connectionString);
            using (DBConnection)
            {
                SqlCommand command = new SqlCommand(sqlstr, DBConnection);

                try
                {
                    DBConnection.Open();
                    SqlDataReader reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            ListItem item = new ListItem(reader["Descricao"].ToString(), reader["ID"].ToString());
                            dropdownlist.Items.Add(item);
                        }
                    }
                    else
                    {

                    }
                    reader.Close();
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
        }

        public static bool Carregar_GridView(string db, string sqlstr, GridView gridview)
        {
            string connectionString = ConfigurationManager.ConnectionStrings
                [db].ConnectionString;
            SqlConnection DBConnection = new SqlConnection(connectionString);
            using (DBConnection)
            {
                SqlCommand command = new SqlCommand(sqlstr, DBConnection);

                try
                {
                    DBConnection.Open();
                    SqlDataReader reader = command.ExecuteReader();

                    gridview.DataSource = reader;
                    gridview.DataBind();
                    reader.Close();
                    return true;
                }
                catch (Exception e)
                {
                    //label_status.Text = "ERRO : " + e.Message;
                    return false;

                }
            }
        }

        public static void Save_Ticket_Attachs(string filePath_Temp, TarefaAttach tarefaAttach)
        {
            string file_path = "~/App_Data/TicketAttachs/Temp/" + filePath_Temp;

            //filePath_Temp = "Temp/" + filePath_Temp;

            try
            {
                string[] attatch_files = Directory.GetFiles(HttpContext.Current.Server.MapPath(file_path));

                Email_FilePath(tarefaAttach, filePath_Temp);
                //criar uma pasta com o id do email
                //eliminar a pasta temporária

            }
            catch (Exception ex)
            {
                //lbl_send_email.Text = "Erro a enviar email"; 
            }
        }


        private static void Email_FilePath(TarefaAttach tarefaAttach, string filePath_temp)
        {

            string fileName = string.Empty;
            string sourcePath = "~/App_Data/TicketAttachs/Temp/" + filePath_temp;
            string targetPath = "~/App_Data/TicketAttachs/" + tarefaAttach.TarefaID;

            //emailFolder = "ticket_" + emailFolder;
            bool exists = System.IO.Directory.Exists(HttpContext.Current.Server.MapPath(sourcePath));

            if (exists && !(filePath_temp == "default"))
            {
                exists = System.IO.Directory.Exists(HttpContext.Current.Server.MapPath(targetPath));
                if (!exists)
                    System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath(targetPath));

                string destFile = string.Empty;


                string[] files = System.IO.Directory.GetFiles(HttpContext.Current.Server.MapPath(sourcePath));

                foreach (string s in files)
                {
                    fileName = System.IO.Path.GetFileName(s);
                    destFile = System.IO.Path.Combine(HttpContext.Current.Server.MapPath(targetPath), fileName);
                    System.IO.File.Copy(s, destFile, true);

                    tarefaAttach.Update_TarefaAttachList(fileName);
                }

            }
            else
            {
                Console.WriteLine("Source path does not exist!");
            }


        }
        public static void conexao()
        {
            String strConection;
            SqlConnection connection;

            strConection = WebConfigurationManager.ConnectionStrings["uCRM_DB"].ToString();
            connection = new SqlConnection(strConection);

            connection.Open();
        }


        public static string Load_AspNetUser(string id)
        {
            //String[] user = new String[MAX_Cliente];

            string user = "";

            string db = "DefaultConnection";

            string connectionString = ConfigurationManager.ConnectionStrings[db].ConnectionString;

            SqlConnection DBConnection = new SqlConnection(connectionString);

            //conexao();

            //SqlCommand command = new SqlCommand();
            string sqlstr = "SELECT Name FROM AspNetUsers WHERE Id = '" + id + "'";

            //command.CommandType = CommandType.Text;
            //command.CommandText = sqlstr;

            //command.Connection = conexao.abrirConexao();

            using (DBConnection)
            {
                SqlCommand command = new SqlCommand(sqlstr, DBConnection);


                try
                {
                    DBConnection.Open();
                    SqlDataReader reader = command.ExecuteReader();

                    if (reader.Read())
                    {
                        user = reader["Name"].ToString();

                        //return result;
                        return user;
                    }

                    reader.Close();

                }
                catch (Exception e)
                { }

                return null;

            }
        }


        public static bool Carregar_Cliente(string id, String[] result)
        {
            //String[] result = new String[MAX_Cliente];


            string db = "uCRM_DB";

            string connectionString = ConfigurationManager.ConnectionStrings[db].ConnectionString;

            SqlConnection DBConnection = new SqlConnection(connectionString);

            //conexao();

            //SqlCommand command = new SqlCommand();
            string sqlstr = "SELECT * FROM Cliente WHERE Id=" + id;

            //command.CommandType = CommandType.Text;
            //command.CommandText = sqlstr;

            //command.Connection = conexao.abrirConexao();

            using (DBConnection)
            {
                SqlCommand command = new SqlCommand(sqlstr, DBConnection);


                try
                {
                    DBConnection.Open();
                    SqlDataReader reader = command.ExecuteReader();

                    if (reader.Read())
                    {
                        result[0] = reader["Id"].ToString();
                        result[1] = reader["Nome"].ToString();
                        result[2] = reader["Email"].ToString();
                        result[3] = reader["Telefone1"].ToString();
                        result[4] = reader["Telefone2"].ToString();
                        result[5] = reader["Conta"].ToString();
                        result[6] = reader["AgenciaID"].ToString();
                        result[7] = reader["TipoID"].ToString();
                        result[8] = reader["AgenciaExt"].ToString();
                        result[9] = reader["Cliente"].ToString();
                        result[10] = reader["DataEditar"].ToString();
                        result[11] = reader["uniq_id"].ToString();

                        //return result;
                        return true;
                    }

                    reader.Close();

                }
                catch (Exception e)
                { }

                return false;

            }
        }

        public static void registar_Log(String[] log)
        {


            string db = "DefaultConnection";

            string connectionString = ConfigurationManager.ConnectionStrings[db].ConnectionString;

            SqlConnection DBConnection = new SqlConnection(connectionString);


            string sqlstr = "INSERT INTO uAuth.dbo.AspNetLogs (Author, DateTime_Log, Table_Log, Entity_Log, Action_Log, Field_1, Field_2) VALUES('" + log[0] + "', GETDATE(), '" + log[2] + "', " + Convert.ToInt32(log[3]) + ", " + Convert.ToInt32(log[4]) + ", '" + log[5] + "', '" + log[6] + "')";

            using (DBConnection)
            {
                SqlCommand command = new SqlCommand(sqlstr, DBConnection);


                try
                {
                    DBConnection.Open();

                    command.CommandType = CommandType.Text;
                    command.CommandText = sqlstr;

                    command.ExecuteNonQuery();

                }
                catch (Exception e)
                { }

                DBConnection.Close();

            }
        }


        public static int Carregar_Task_Count(string db, string sqlstr)
        {
            int result;

            string connectionString = ConfigurationManager.ConnectionStrings
                [db].ConnectionString;
            SqlConnection DBConnection = new SqlConnection(connectionString);
            using (DBConnection)
            {
                SqlCommand command = new SqlCommand(sqlstr, DBConnection);

                try
                {
                    DBConnection.Open();
                    SqlDataReader reader = command.ExecuteReader();


                    if (reader.Read())
                    {
                        result = Convert.ToInt32(reader["Count"].ToString());

                        return result;
                    }

                    reader.Close();

                }
                catch (Exception e)
                { }

                return -1;

            }
        }


        public static bool Update_Registo(string db, string sqlstr)
        {
            string connectionString = ConfigurationManager.ConnectionStrings[db].ConnectionString;
            SqlConnection DBConnection = new SqlConnection(connectionString);
            using (DBConnection)
            {
                SqlCommand command = new SqlCommand(sqlstr, DBConnection);
                try
                {
                    DBConnection.Open();
                    int affected_rows = command.ExecuteNonQuery();
                    if (affected_rows == 1) return true;
                    else return false;
                }
                catch (Exception e)
                {
                    return false;
                }
            }

        }

        internal static bool session_status(string username)
        {
            string result = "";


            string db = "DefaultConnection";

            string connectionString = ConfigurationManager.ConnectionStrings[db].ConnectionString;

            SqlConnection DBConnection = new SqlConnection(connectionString);

            //conexao();

            //SqlCommand command = new SqlCommand();
            string sqlstr = "SELECT Session FROM AspNetUsers WHERE Username = '" + username + "'";

            //command.CommandType = CommandType.Text;
            //command.CommandText = sqlstr;

            //command.Connection = conexao.abrirConexao();

            using (DBConnection)
            {
                SqlCommand command = new SqlCommand(sqlstr, DBConnection);


                try
                {
                    DBConnection.Open();
                    SqlDataReader reader = command.ExecuteReader();

                    if (reader.Read())
                    {
                        result = reader["Session"].ToString();


                        if (result == "" + true) return true;
                    }

                    reader.Close();

                }
                catch (Exception e)
                { }

                return false;
            }
        }

        public static void change_session_value_IN(string username, int value)
        {
            string db = "DefaultConnection";
            string input = "";

            string connectionString = ConfigurationManager.ConnectionStrings[db].ConnectionString;

            SqlConnection DBConnection = new SqlConnection(connectionString);


            if (value == 0) input = " SET Session = 0";
            if (value == 1) input = " SET Session = 1";

            string sqlstr = "UPDATE [uAuth].[dbo].[AspNetUsers]" + input + " WHERE UserName = '" + username + "'";


            using (DBConnection)
            {
                SqlCommand command = new SqlCommand(sqlstr, DBConnection);


                try
                {
                    DBConnection.Open();

                    command.CommandType = CommandType.Text;
                    command.CommandText = sqlstr;

                    command.ExecuteNonQuery();

                }
                catch (Exception e)
                { }

                DBConnection.Close();

            }
        }


        internal static void change_session_value_OUT(string id, int value)
        {
            string db = "DefaultConnection";
            string input = "";

            string connectionString = ConfigurationManager.ConnectionStrings[db].ConnectionString;

            SqlConnection DBConnection = new SqlConnection(connectionString);


            if (value == 0) input = " SET Session = 0";
            if (value == 1) input = " SET Session = 1";

            string sqlstr = "UPDATE [uAuth].[dbo].[AspNetUsers]" + input + " WHERE Id = '" + id + "'";
          

            using (DBConnection)
            {
                SqlCommand command = new SqlCommand(sqlstr, DBConnection);


                try
                {
                    DBConnection.Open();

                    command.CommandType = CommandType.Text;
                    command.CommandText = sqlstr;

                    command.ExecuteNonQuery();

                }
                catch (Exception e)
                { }

                DBConnection.Close();

            }
        }


        internal static string Load_Last_uTasksCreate(string user)
        {
            string task = "";

            string db = "uTasks_DB";

            string connectionString = ConfigurationManager.ConnectionStrings[db].ConnectionString;

            SqlConnection DBConnection = new SqlConnection(connectionString);

            string sqlstr = "SELECT MAX(Tarefa.ID) AS Task FROM [uTasks].[dbo].[Tarefa] WHERE UserIDRegisto = '" + user + "'";

            using (DBConnection)
            {
                SqlCommand command = new SqlCommand(sqlstr, DBConnection);


                try
                {
                    DBConnection.Open();
                    SqlDataReader reader = command.ExecuteReader();

                    if (reader.Read())
                    {
                        task = reader["Task"].ToString();

                        return task;
                    }

                    reader.Close();

                }
                catch (Exception e)
                { }

                return null;

            }
        }


        public static string get_filename_ticket(string file_id)
        {

            string filename = "";
            string db = "uTasks_DB";

            string connectionString = ConfigurationManager.ConnectionStrings[db].ConnectionString;

            SqlConnection DBConnection = new SqlConnection(connectionString);

            string sqlstr = "SELECT [Filename] FROM [Tarefa_Attach] WHERE ID = '" + file_id + "'";

            using (DBConnection)
            {
                SqlCommand command = new SqlCommand(sqlstr, DBConnection);
                
                try
                {
                    DBConnection.Open();
                    SqlDataReader reader = command.ExecuteReader();

                    if (reader.Read())
                    {
                        filename = reader["Filename"].ToString();

                        return filename;
                    }

                    reader.Close();

                }
                catch (Exception e)
                { }

                return null;

            }
        }

        public static void Delete_TicketAttach(string file_id)
        {
            string db = "uTasks_DB";

            string connectionString = ConfigurationManager.ConnectionStrings[db].ConnectionString;

            SqlConnection DBConnection = new SqlConnection(connectionString);

            string sqlstr = "DELETE FROM [Tarefa_Attach] WHERE ID = " + file_id + "";


            using (DBConnection)
            {
                SqlCommand command = new SqlCommand(sqlstr, DBConnection);


                try
                {
                    DBConnection.Open();

                    command.CommandType = CommandType.Text;
                    command.CommandText = sqlstr;

                    command.ExecuteNonQuery();

                }
                catch (Exception e)
                { }

                DBConnection.Close();

            }
        }






    }
    
}