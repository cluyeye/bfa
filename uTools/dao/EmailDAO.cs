﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace uTools
{
    public class EmailDAO
    {

        private string db = "uTasks_DB";
        //private string sender = "dmk@contacto.bfa.ao";

        private string from = string.Empty;
        private string to = string.Empty;
        private string cc = string.Empty;
        private string assunto = string.Empty;
        private string conteudo = string.Empty;
        private string conteudo_Alternativo = string.Empty;


        //public string Sender
        //{
        //    get { return sender; }
        //    set { sender = value; }
        //}

        public string Conteudo_Alternativo
        {
            get { return conteudo_Alternativo; }
            set { conteudo_Alternativo = value; }
        }

        public string From
        {
            get { return from; }
            set { from = value; }
        }

        public string To
        {
            get { return to; }
            set { to = value; }
        }

        public string Cc
        {
            get { return cc; }
            set { cc = value; }
        }

        public string Assunto
        {
            get { return assunto; }
            set { assunto = value; }
        }

        public string Conteudo
        {
            get { return conteudo; }
            set { conteudo = value; }
        }


        private void Carregar_MailMessage(System.Net.Mail.MailMessage mail_message)
        {

            String[] to_address_list = To.Split(';');
            AlternateView htmlView;

            foreach (string to in to_address_list)
            {
                mail_message.To.Add(to);
            }

            if (Cc != string.Empty)
            {
                String[] cc_address_list = Cc.Split(';');
                foreach (string cc in cc_address_list)
                {
                    mail_message.CC.Add(cc);
                }
            }

            mail_message.From = new MailAddress(From);
            mail_message.Subject = Assunto;
            mail_message.IsBodyHtml = true;

            if (Conteudo_Alternativo != string.Empty)
            {
                htmlView = AlternateView.CreateAlternateViewFromString(Conteudo + Signature() + Conteudo_Alternativo, null, System.Net.Mime.MediaTypeNames.Text.Html);
            }
            else
            {
                htmlView = AlternateView.CreateAlternateViewFromString(Conteudo + Signature(), null, System.Net.Mime.MediaTypeNames.Text.Html);
            }


            string signature_filepath = HttpContext.Current.Server.MapPath("~/dao/") + "signature.png";

            System.Net.Mail.LinkedResource signature_img = new LinkedResource(signature_filepath);

            signature_img.ContentId = "signature";

            htmlView.LinkedResources.Add(signature_img);

            mail_message.AlternateViews.Add(htmlView);
        }


        public string Signature()
        {
            //string sig_jpg_file = "signature.png";
            string signature_filepath = HttpContext.Current.Server.MapPath("~/dao/") + "signature.png";

            string signature = "<br/><br/><br/><br/><br/><b>Serviço de Apoio ao Cliente<br/>BFA</b><br/>Rua Amílcar Cabral, 58, Maianga – Luanda<br/>" +
                "Linha de Atendimento BFA: 923 120 120<br/>bfa@bfa.ao<br/><a href='http:\\www.bfa.ao'>www.bfa.ao</a><br/>" +
                "<img src='cid:signature'><br/><br/>";

            return signature;
        }


        private string Insert_NewEmail(string task_id, string user_id)
        {

            string sqlstr = "INSERT INTO Tarefa_Email (TarefaID ,[From], [To], Assunto, Conteudo, DataEmail, TipoID, DataRegisto, CC, UserID) VALUES (" +
                    "" + task_id + ", '" + From + "', '" + To + "', '" + Assunto + "', '" + Conteudo + "', GETDATE(), 10, GETDATE(), '" + Cc + "', '" + user_id + "')";

            string sqlstr_id = "SELECT MAX(ID) AS ID FROM Tarefa_Email";

            string connectionString = ConfigurationManager.ConnectionStrings[db].ConnectionString;
            SqlConnection DBConnection = new SqlConnection(connectionString);
            using (DBConnection)
            {
                SqlCommand command = new SqlCommand(sqlstr, DBConnection);
                try
                {
                    DBConnection.Open();

                    command.ExecuteNonQuery();

                    command = new SqlCommand(sqlstr_id, DBConnection);
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        reader.Read();
                        return reader["ID"].ToString();
                    }
                }
                catch (Exception e)
                {

                }

                return "-1";

            }
        }



        public void Select_EmailByID(string id)
        {
            string db = "uTasks_DB";

            string sqlstr = "SELECT [From], [To], [CC], [Assunto], [Conteudo] FROM Tarefa_Email WHERE ID=" + id;

            string connectionString = ConfigurationManager.ConnectionStrings
                [db].ConnectionString;
            SqlConnection DBConnection = new SqlConnection(connectionString);
            using (DBConnection)
            {
                SqlCommand command = new SqlCommand(sqlstr, DBConnection);
                try
                {
                    DBConnection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        reader.Read();
                        From = reader["From"].ToString();
                        To = reader["To"].ToString();
                        Cc = reader["CC"].ToString();
                        Assunto = reader["Assunto"].ToString();
                        Conteudo = reader["Conteudo"].ToString();
                    }
                }
                catch (Exception ex) { }
            }
        }



        public void Ticket_FilePath(string emailFolder, string filePath_temp, string fileSelected)
        {

            string sourcePath = string.Empty;
            string targetPath = string.Empty;

            string fileName = string.Empty;

            sourcePath = "~/App_Data/TicketAttachs/" + filePath_temp;
            targetPath = "~/App_Data/MailAttachs/Temp/" + emailFolder;

            //emailFolder = "ticket_" + emailFolder;
            bool exists = System.IO.Directory.Exists(HttpContext.Current.Server.MapPath(sourcePath));

            if (exists && !(filePath_temp == "default"))
            {
                exists = System.IO.Directory.Exists(HttpContext.Current.Server.MapPath(targetPath));
                if (!exists)
                    System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath(targetPath));

                string destFile = string.Empty;


                string[] files = System.IO.Directory.GetFiles(HttpContext.Current.Server.MapPath(sourcePath));

                foreach (string s in files)
                {
                    fileName = System.IO.Path.GetFileName(s);

                    if (fileName == fileSelected)
                    {
                        destFile = System.IO.Path.Combine(HttpContext.Current.Server.MapPath(targetPath), fileName);
                        System.IO.File.Copy(s, destFile, true);
                    }
                }

            }
            else
            {
                Console.WriteLine("Source path does not exist!");
            }


        }


        public void Send_Email(string filePath_Temp, string task_id, string user_id)
        {
            string file_path = "~/App_Data/MailAttachs/Temp/" + filePath_Temp;

            //filePath_Temp = "Temp/" + filePath_Temp;

            try
            {
                MailMessage mail_message = new MailMessage();
                SmtpClient smtpclient = new SmtpClient("srv-tl-ex01.ucall.co.ao", 25);


                Carregar_MailMessage(mail_message);

                //if (tipo_accao )
                string[] attatch_files = Directory.GetFiles(HttpContext.Current.Server.MapPath(file_path));

                foreach (string file in attatch_files)
                {
                    mail_message.Attachments.Add(new Attachment(file));
                }

                smtpclient.Send(mail_message);

                //filePath_Temp = Insert_NewEmail();

                Email_FilePath(Insert_NewEmail(task_id, user_id), filePath_Temp, 1);

            }
            catch (Exception ex)
            {
                //lbl_send_email.Text = "Erro a enviar email"; 
            }
        }


        public void Email_FilePath(string emailFolder, string filePath_temp, int opcao)
        {
            string sourcePath = string.Empty;
            string targetPath = string.Empty;

            string fileName = string.Empty;
            if (opcao == 1)
            {
                sourcePath = "~/App_Data/MailAttachs/Temp/" + filePath_temp;
                targetPath = "~/App_Data/MailAttachs/" + emailFolder;
            }

            if (opcao == 2)
            {
                sourcePath = "~/App_Data/MailAttachs/" + filePath_temp;
                targetPath = "~/App_Data/MailAttachs/Temp/" + emailFolder;
            }

            //emailFolder = "ticket_" + emailFolder;
            bool exists = System.IO.Directory.Exists(HttpContext.Current.Server.MapPath(sourcePath));

            if (exists && !(filePath_temp == "default"))
            {
                exists = System.IO.Directory.Exists(HttpContext.Current.Server.MapPath(targetPath));
                if (!exists)
                    System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath(targetPath));

                string destFile = string.Empty;


                string[] files = System.IO.Directory.GetFiles(HttpContext.Current.Server.MapPath(sourcePath));

                foreach (string s in files)
                {
                    fileName = System.IO.Path.GetFileName(s);
                    destFile = System.IO.Path.Combine(HttpContext.Current.Server.MapPath(targetPath), fileName);
                    Console.WriteLine("" + destFile + "");
                    System.IO.File.Copy(s, destFile, true);
                }

            }
            else
            {
                Console.WriteLine("Source path does not exist!");
            }


        }

        public void Email_FW_FilePath(string emailFolder, string filePath_temp, string fileSelected)
        {
            string sourcePath = string.Empty;
            string targetPath = string.Empty;

            string fileName = string.Empty;
            //if (opcao == 1)
            //{
            //    sourcePath = "~/App_Data/MailAttachs/Temp/" + filePath_temp;
            //    targetPath = "~/App_Data/MailAttachs/" + emailFolder;
            //}

            //if (opcao == 2)
            //{
                sourcePath = "~/App_Data/MailAttachs/" + filePath_temp;
                targetPath = "~/App_Data/MailAttachs/Temp/" + emailFolder;
            //}

            //emailFolder = "ticket_" + emailFolder;
            bool exists = System.IO.Directory.Exists(HttpContext.Current.Server.MapPath(sourcePath));

            if (exists && !(filePath_temp == "default"))
            {
                exists = System.IO.Directory.Exists(HttpContext.Current.Server.MapPath(targetPath));
                if (!exists)
                    System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath(targetPath));

                string destFile = string.Empty;


                string[] files = System.IO.Directory.GetFiles(HttpContext.Current.Server.MapPath(sourcePath));

                foreach (string s in files)
                {
                    fileName = System.IO.Path.GetFileName(s);

                    if (fileName == fileSelected)
                    {
                        destFile = System.IO.Path.Combine(HttpContext.Current.Server.MapPath(targetPath), fileName);
                        System.IO.File.Copy(s, destFile, true);
                    }
                }

            }
            else
            {
                Console.WriteLine("Source path does not exist!");
            }


        }

       
    }
}