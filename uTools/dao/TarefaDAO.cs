﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace uTools.dao
{
    public class TarefaDAO
    {

        string db = "uTasks_DB";

        private string id;
        private string descricao;
        private string resumo;
        private string resolucao;
        private string dataRegisto;
        private string dataFecho;
        private string estadoID;
        private string estado;
        private string departamentoID;
        private string departamento;
        private string tipo;
        private string sla;
        private string diferencaSLA;
        private string clienteID;
        private string clienteNome;
        private string canalContactoID;
        private string canalContacto;
        private string userID;


        private void Update_Detalhe_Tarefa(string id)
        {


            string db = "uTasks_DB";

            string sqlstr = "SELECT Tarefa.ID, Tarefa.Descricao, Tarefa.Resumo, Tarefa.ResolucaoResumo, CONVERT(char(16),Tarefa.DataRegisto,121) DataRegisto, CONVERT(char(16),Tarefa.DataFecho,121) DataFecho, " +
                "Tarefa.EstadoID, Estado.Descricao Estado, Tarefa.DepartamentoID, Departamento.Descricao Departamento, Tipificacao.Descricao Tipo, Tarefa.SLAHoras, (Tarefa.SLAHoras - DATEDIFF(HH, ISNULL(Tarefa.DataFecho, GETDATE()),Tarefa.DataRegisto )) diferenca_SLA, tarefa.clienteID, Cliente.Nome AS ClienteNome, " +
                "Tarefa.CanalContactoID AS Canal_ContactoID, Canal_Contacto.Descricao AS Canal_Contacto, AspUser.UserName AS Usuario, (LEFT(AspUser.Name, 170) + '...') AS Feito_por " +
                "FROM Tarefa " +
                "LEFT JOIN Estado ON Tarefa.EstadoID=Estado.ID " +
                "LEFT JOIN Departamento ON Tarefa.DepartamentoID=Departamento.ID " +
                "LEFT JOIN Tipificacao ON Tarefa.TipoID=Tipificacao.ID " +
                "LEFT JOIN uCRM.dbo.Cliente AS Cliente ON (Tarefa.ClienteID = Cliente.Id) " +
                "LEFT JOIN Canal_Contacto ON (Tarefa.CanalContactoID = Canal_Contacto.ID) " +
                "LEFT JOIN Tipificacao AS Assunto ON (Tarefa.TipificacaoID = Assunto.ID) " +
                "LEFT JOIN uAuth.dbo.AspNetUsers AS AspUser ON (Tarefa.UserIDRegisto = AspUser.Id) " +

                "WHERE Tarefa.ID=" + id;


            string connectionString = ConfigurationManager.ConnectionStrings
                [db].ConnectionString;
            SqlConnection DBConnection = new SqlConnection(connectionString);
            using (DBConnection)
            {
                SqlCommand command = new SqlCommand(sqlstr, DBConnection);
                try
                {
                    DBConnection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        reader.Read();
                        Id = reader["ID"].ToString();
                        Descricao = reader["Descricao"].ToString();
                        Resumo = reader["Resumo"].ToString();
                        Resolucao = reader["ResolucaoResumo"].ToString();
                        DataRegisto = reader["DataRegisto"].ToString();
                        DataFecho = reader["DataFecho"].ToString();
                        EstadoID = reader["EstadoID"].ToString();
                        Estado = reader["Estado"].ToString();
                        DepartamentoID = reader["DepartamentoID"].ToString();
                        Departamento = reader["Departamento"].ToString();
                        Tipo = reader["Tipo"].ToString();
                        Sla = reader["SLAHoras"].ToString() + "(" + reader["diferenca_SLA"].ToString() + ")";
                        DiferencaSLA = reader["diferenca_SLA"].ToString();
                        clienteID = reader["ClienteID"].ToString();
                        ClienteNome = reader["ClienteNome"].ToString();
                        CanalContactoID = reader["Canal_ContactoID"].ToString();
                        CanalContacto = reader["Canal_Contacto"].ToString();
                        UserID = reader["Usuario"].ToString();

                    }

                }
                catch (Exception erro)
                {
                    //label_status = "ERRO : " + erro.Message;

                }
            }
        }



        public string Select_Mounth_Ticket(string task_id)
        {
            
            string sqlstr = "SELECT CONVERT(char(16),Tarefa.DataRegisto,121) DataRegisto " +
                "FROM Tarefa " +
                "WHERE Tarefa.ID = " + task_id;


            string connectionString = ConfigurationManager.ConnectionStrings
                [db].ConnectionString;
            SqlConnection DBConnection = new SqlConnection(connectionString);
            using (DBConnection)
            {
                SqlCommand command = new SqlCommand(sqlstr, DBConnection);
                try
                {
                    DBConnection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        reader.Read();
                        
                        return reader["DataRegisto"].ToString();
                        
                    }
                    reader.Close();
                }
                catch (Exception erro)
                {
                    //label_status = "ERRO : " + erro.Message;

                }
            }

            return null;
        }


        public string Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Descricao
        {
            get { return descricao; }
            set { descricao = value; }
        }

        public string Resumo
        {
            get { return resumo; }
            set { resumo = value; }
        }


        public string Resolucao
        {
            get { return resolucao; }
            set { resolucao = value; }
        }
        public string DataRegisto
        {
            get { return dataRegisto; }
            set { dataRegisto = value; }
        }

        public string DataFecho
        {
            get { return dataFecho; }
            set { dataFecho = value; }
        }


        public string EstadoID
        {
            get { return estadoID; }
            set { estadoID = value; }
        }
        public string Estado
        {
            get { return estado; }
            set { estado = value; }
        }

        public string DepartamentoID
        {
            get { return departamentoID; }
            set { departamentoID = value; }
        }

        public string Departamento
        {
            get { return departamento; }
            set { departamento = value; }
        }

        public string Tipo
        {
            get { return tipo; }
            set { tipo = value; }
        }

        public string Sla
        {
            get { return sla; }
            set { sla = value; }
        }

        public string DiferencaSLA
        {
            get { return diferencaSLA; }
            set { diferencaSLA = value; }
        }

        public string ClienteID
        {
            get { return clienteID; }
            set { clienteID = value; }
        }

        public string ClienteNome
        {
            get { return clienteNome; }
            set { clienteNome = value; }
        }

        public string CanalContactoID
        {
            get { return canalContactoID; }
            set { canalContactoID = value; }
        }

        public string CanalContacto
        {
            get { return canalContacto; }
            set { canalContacto = value; }
        }

        public string UserID
        {
            get { return userID; }
            set { userID = value; }
        }


    }
}