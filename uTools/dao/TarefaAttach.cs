﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace uTools.dao
{
    public class TarefaAttach
    {
        private string tarefaID = string.Empty;
        private string filename = string.Empty;
        private string dataRegisto = string.Empty;
        private string userID = string.Empty;

        public TarefaAttach(string tarefaID, string filename, string userID)
        {
            TarefaID = tarefaID;
            Filename = filename;
            UserID = userID;
        }

        public string UserID
        {
            get { return userID; }
            set { userID = value; }
        }

        public string DataRegisto
        {
            get { return dataRegisto; }
            set { dataRegisto = value; }
        }

        public string Filename
        {
            get { return filename; }
            set { filename = value; }
        }

        public string TarefaID
        {
            get { return tarefaID; }
            set { tarefaID = value; }
        }
        
        public Boolean Update_TarefaAttachList(string file)
        {
            string sqlstr = "INSERT INTO Tarefa_Attach (TarefaID, Filename, DataRegisto, UserId) VALUES (" + TarefaID + ", '" + file + "', GETDATE(), '" + UserID + "')";

            string sqlstr_id = "SELECT MAX(ID) AS ID FROM Tarefa_Attach";

            string connectionString = ConfigurationManager.ConnectionStrings["uTasks_DB"].ConnectionString;
            SqlConnection DBConnection = new SqlConnection(connectionString);
            using (DBConnection)
            {
                SqlCommand command = new SqlCommand(sqlstr, DBConnection);
                try
                {
                    DBConnection.Open();

                    command.ExecuteNonQuery();

                    command = new SqlCommand(sqlstr_id, DBConnection);
                    //SqlDataReader reader = command.ExecuteReader();
                    //if (reader.HasRows)
                    //{
                    //    reader.Read();
                    //    return reader["ID"].ToString();
                    //}
                    return true;
                }
                catch (Exception e)
                {

                }

                return false;

            }
        }
    }
}