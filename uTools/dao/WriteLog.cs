﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace uTools.dao
{
    public class WriteLog
    {

        //Variaveis de logs
        private static int id = 0;
        private static string author = string.Empty;
        private static int entity = 0;
        private static int action = 0;
        private static string field_1 = null;
        private static string field_2 = null;


        //Variaveis de que indicam a entidade que do log
        public static int USER = 1;

        // Enumeração para Entidade do Log
        public enum Log_Entidade
        {
            USER = 1,
            USER_SESSION = 5,
            USER_PASSWORD = 6,

            CLIENT = 2,
            TICKET = 3,
            TICKET_COMMENT = 4,
            TICKET_STATUS = 5,
            TICKET_REFERENCIA = 7
        }

        // Enumeração para Acção da Entidade do Log
        public enum Log_Accao
        {
            LOGIN = 1,
            LOG_OUT = 2,
            LOG_OUT_FORCED = 9,
            CHANGE = 3,
            CREATE = 4,
            UPDATE = 5,
            DELETE = 6,
            SEND = 7
        }


        public static void Write(string author, Log_Entidade entity, Log_Accao action, string field_1, string field_2)
        {
            Author = author;
            Entity = (int)entity;
            Action = (int)action;
            Field_1 = field_1;
            Field_2 = field_2;

            write();
        }

        public static int ID
        {
            get { return id; }
            set { id = value; }
        }

        public static string Author
        {
            get { return author; }
            set { author = value; }
        }

        public static int Entity
        {
            get { return entity; }
            set { entity = value; }
        }

        public static int Action
        {
            get { return action; }
            set { action = value; }
        }

        public static string Field_1
        {
            get { return field_1; }
            set { field_1 = value; }
        }

        public static string Field_2
        {
            get { return field_2; }
            set { field_2 = value; }
        }


        public static void write()
        {

            string db = "DefaultConnection";

            string connectionString = ConfigurationManager.ConnectionStrings[db].ConnectionString;

            SqlConnection DBConnection = new SqlConnection(connectionString);


            string sqlstr = "INSERT INTO AspNetLogs (Author, Register_Date, Entity, [Action], Field_1, Field_2) VALUES ('" + Author + "', GETDATE(), " + Entity + ", " + Action + ", '" + field_1 + "', '" + Field_2 + "')";

            using (DBConnection)
            {
                SqlCommand command = new SqlCommand(sqlstr, DBConnection);


                try
                {
                    DBConnection.Open();

                    command.CommandType = CommandType.Text;
                    command.CommandText = sqlstr;

                    command.ExecuteNonQuery();

                }
                catch (Exception e)
                { }

                DBConnection.Close();

            }
        }

    }
}