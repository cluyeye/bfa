﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserChangePwd.aspx.cs" Inherits="uTools.UserChangedPwd" %>



<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>

    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta http-equiv="content-language" content="pt-br" />

    <meta name="description" content="Ferramenta de Ticketing, que vai melhorar a
        Experiência de Cliente dando resposta às situações colocadas nos vários canais
        de interacção da sua organização (agências, pontos de venda, contact center, etc).
        A partir de qualquer área da sua empresa, a resposta é dada dentro do prazo expectável,
        aumentando a Qualidade na relação com o Cliente. Os pedidos de informação, as reclamações
        ou sugestões são direccionadas a quem decide e resolvem-se com maior rapidez, eficiência
        e controlo da interacção" />

    <meta name="locale" content="AO"/>
    <meta name="language" content="pt"/>
    <meta name="country" content="Angola"/>

    <link rel="shortcut icon" href="images/image_icon.png" />

    <title>uTools | Nova Palavra-passe</title>

    <link rel="stylesheet" type="text/css" href="css/style_userchangepwd.css" />
    <link rel="stylesheet" type="text/css" href="css/geral.css" />

</head>

<body>
    <form id="form1" runat="server">
        <asp:PlaceHolder runat="server" ID="LoginForm">

            <div id="container_changepwd" align="center">

                <div>
                    <%--<img title="" id="img_changepwd" src="~/images/userchange_img.png" />--%>

                    <div id="content_user_changepwd" align="justify">

                        <p>
                            <asp:Literal runat="server" ID="StatusMessage" />
                        </p>
                        <div style="margin-bottom: 10px">
                            <asp:Label runat="server" AssociatedControlID="UserName" class="lb_changepwd">Nome do Usuario*</asp:Label>
                            <div>
                                <asp:TextBox ID="UserName" CssClass="txtInput" Font-Size="X-Large" runat="server" Enabled="false" />
                            </div>
                        </div>
                        <div style="margin-bottom: 10px">
                            <asp:Label runat="server" AssociatedControlID="CurrentPwd" class="lb_changepwd">Palavra-Passe Atual*</asp:Label>
                            <div>
                                <asp:TextBox CssClass="txtInput" runat="server" ID="CurrentPwd" TextMode="Password" />
                            </div>
                        </div>
                        <div style="margin-bottom: 10px">
                            <asp:Label runat="server" AssociatedControlID="Password" class="lb_changepwd">Palavra-Passe Nova*</asp:Label>
                            <div>
                                <asp:TextBox CssClass="txtInput" runat="server" ID="Password" TextMode="Password" />
                            </div>
                        </div>
                        <div style="margin-bottom: 10px">
                            <asp:Label runat="server" AssociatedControlID="ConfirmPassword" class="lb_changepwd">Confirmar Palavra-Passe*</asp:Label>
                            <div>
                                <asp:TextBox CssClass="txtInput" runat="server" ID="ConfirmPassword" TextMode="Password" />
                            </div>
                        </div>
                        <div>
                            <div>
                                <asp:ImageButton ID="btn_alterar" runat="server" ImageUrl="~/images/ok.png" OnClick="CreateUser_Click" CssClass="img_bt" title="Alterar Palavra-passe" />
                                <%--<asp:Button runat="server" OnClick="CreateUser_Click" Text="Register" />--%>
                            </div>
                        </div>


                    </div>

                </div>


            </div>

        </asp:PlaceHolder>
</form>
</body>

</html>


