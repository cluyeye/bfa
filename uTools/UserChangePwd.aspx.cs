﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;

namespace uTools
{
    public partial class UserChangedPwd : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!User.Identity.IsAuthenticated)
            {
                Response.Redirect("~/Login.aspx?urlredirect=UserChangedPwd.aspx");
            }

            UserName.Text = "" + Context.User.Identity.GetUserName();
        }


        protected void CreateUser_Click(object sender, EventArgs e)
        {
            // Default UserStore constructor uses the default connection string named: DefaultConnection

            var userStore = new UserStore<IdentityUser>();
            var userManager = new UserManager<IdentityUser>(userStore);
            var user = userManager.Find(UserName.Text, CurrentPwd.Text);


            if (user != null)
            {
                //var authenticationManager = HttpContext.Current.GetOwinContext().Authentication;
                //var userIdentity = userManager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);
                //authenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = false }, userIdentity);



                IdentityResult result = userManager.ChangePassword(user.Id, CurrentPwd.Text, Password.Text);

                if (result.Succeeded)
                {
                    //manager.AddToRoles(user.Id, "user", "admin");

                    StatusMessage.Text = "User : " + user.UserName + " pwd alterada";

                    registar_Log();

                    Logout();


                }
                else
                {
                    StatusMessage.Text = result.Errors.FirstOrDefault();
                }

            }
            else
            {
                StatusMessage.Text = "Invalid username or password.";
            }

        }

        
        private void Logout()
        {
            var authenticationManager = HttpContext.Current.GetOwinContext().Authentication;

            authenticationManager.SignOut();

            Response.ClearContent();
            Response.Redirect("~/Login.aspx?urlredirect=utools_main.aspx");

        }



        private void registar_Log()
        {

            String[] log = new String[15];

            log[0] = "" + Context.User.Identity.GetUserId();
            log[1] = "" + DateTime.Now.ToString();
            log[2] = "uAuth.dbo.AspNetUsers";
            log[3] = "" + 6;
            log[4] = "" + 5;
            //log[5] = UserName.Text;
            //log[6] = CurrentPwd.Text;
            //log[7] = ConfirmPassword.Text;

            Tools.registar_Log(log);

        }
    }
}