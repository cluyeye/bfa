﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;


namespace uTools
{
    public partial class utools_main : System.Web.UI.Page
    {

        string id;
        string name_user;
        
        protected void Page_Load(object sender, EventArgs e)
        {

            string id = Context.User.Identity.GetUserId();
            string name_user = Tools.Load_AspNetUser(id);

            //op_manage.Visible = true;

            if (!User.Identity.IsAuthenticated)
            {
                Response.Redirect("~/Login.aspx?urlredirect=utools_main.aspx");
            }


            lbl_info_agent.Text = "" + name_user + "" + role_agent();

            //lbl_job_role_agent.Text = ":::::Info Job Role Agent:::::";

            if (lbl_info_agent.Text == string.Empty)
            {
                agent_info.Visible = false;
            }

            if (lbl_job_role_agent.Text == string.Empty)
            {
                job_role_info.Visible = false;
            }
                        
            
            uTasksView1.OnUpdateDetalheTarefa += new uTools.Controls.uTasksView.OnUpdateDetalheTarefa_event(uTasksView_OnUpdateDetalheTarefa);

            Get_Username_Agent();
        }


        private string role_agent()
        {
            string role = string.Empty;

            if (User.IsInRole("Admin"))
                return " | ADMINISTRADOR(A)";

            if (User.IsInRole("Director DMK"))
                return " | DIRECTOR(A) DMK";

            if (User.IsInRole("Coordenador DMK"))
                return " | COORDENADOR(A) BO DMK";

            if (User.IsInRole("Assistente DMK"))
                return " | ASSISTENTE BO DMK";

            if (User.IsInRole("Coordenador UCALL"))
                return " | COORDENADOR(A) DE CONTACT CENTER";

            if (User.IsInRole("Supervisao UCALL"))
                return " | SUPERVISOR DE CONTACT CENTER";

            if (User.IsInRole("Asssistente UCALL"))
                return " | ASSISTENTE DE CONTACT CENTER";

            return role;
        }


        public bool uTasksView_OnUpdateDetalheTarefa(string id)
        {                        
            uCRMView1.Update_client(id);
            return true;
        }

        public string Get_Selected_Cliente()
        {
            return uCRMView1.ClienteID;
        }


        public string Get_Password_Agent()
        {
            if (this.Page.GetType().FullName == "ASP.Login_aspx")
                return ((Login)this.Page).Password_Agent;
            else
                return string.Empty;
        }


        public void Update_CRM(string id)
        { 
        }

        private string Get_Username_Agent()
        {
            if (this.Page.GetType().FullName == "ASP.Login_aspx")
                return ((Login)this.Page).Username_Agent;
            else
                return string.Empty;
        }

        private void Logout()
        {
            var authenticationManager = HttpContext.Current.GetOwinContext().Authentication;

            Load_user_info(id, name_user);
            Tools.change_session_value_OUT(Context.User.Identity.GetUserId(), 0);

            authenticationManager.SignOut();

            Response.ClearContent();
            Response.Redirect("~/Login.aspx?urlredirect=utools_main.aspx");

        }

        private void Load_user_info(string id, string name_user)
        {
            id = Context.User.Identity.GetUserId();
            name_user = Tools.Load_AspNetUser(id);
        }

        private void registar_Log()
        {

            String[] log = new String[15];

            log[0] = "" + Context.User.Identity.GetUserId();
            log[2] = "uAuth.dbo.AspNetUsers";
            log[3] = "" + 5;
            log[4] = "" + 2;
            log[5] = "" + Context.User.Identity.GetUserName();

            Tools.registar_Log(log);
        }

        protected void btn_pl_admin_Click(object sender, EventArgs e)
        {
            Boolean user = User.IsInRole("Admin");


            if (!user)
            {
                
                //modal_accao.Show();
                //pnl_accao_modal.Visible = true;
            
                //header_modal.InnerText = "Acesso Negado";

                //body_modal.InnerText = "Apenas pode aceder o Painel de Administração o Usuário que tenha privilégios de um Administrador.";
            }
            else
                Response.Redirect("~/10.11.1.68/utools_admin/utools_admin.aspx");


        }

        protected void btn_accao_cancel_Click(object sender, EventArgs e)
        {
            Boolean user = User.IsInRole("Admin");


            if (user)
            {

                //modal_accao.Show();
                //pnl_accao_modal.Visible = true;

                //header_modal.InnerText = "Acesso Negado";

                //body_modal.InnerText = "Apenas pode aceder o Painel de Administração o Usuário que tenha privilégios de um Administrador.";
            }
            //else
            //    Response.Redirect("http://10.11.1.68/utools_admin/utools_admin.aspx");
        }

        protected void btn_logout_Click(object sender, EventArgs e)
        {
            registar_Log();

            Logout();
        }

    }
}