﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace uTools
{
    public class Cliente
    {
        private int cliente_id;
        private string nome=string.Empty;
        private string email = string.Empty;
        private string telefone1 = string.Empty;
        private string telefone2 = string.Empty;
        private string numero_conta = string.Empty;
        private int agencia_id;
        private int tipo_id;

        public int Cliente_id 
        {
            get
            {
                return cliente_id;
            }
            set{
                cliente_id = value;
            }
        }

        public string Nome
        {
            get
            {
                return nome;
            }
            set
            {
                nome = value;
            }
        }

        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                email = value;
            }
        }

        public string Telefone1
        {
            get
            {
                return telefone1;
            }
            set
            {
                telefone1 = value;
            }
        }

        public string Telefone2
        {
            get
            {
                return telefone2;
            }
            set
            {
                telefone2 = value;
            }
        }

        public string Numero_conta
        {
            get
            {
                return numero_conta;
            }
            set
            {
                numero_conta = value;
            }
        }

        public int Agencia_id
        {
            get
            {
                return agencia_id;
            }
            set
            {
                agencia_id = value;
            }
        }

        public int Tipo_id
        {
            get
            {
                return tipo_id;
            }
            set
            {
                tipo_id = value;
            }
        }

        public bool SelectCliente_byContacto(string contacto, string db)
        {
            string sqlstr = "SELECT TOP 20 [Id],[Nome],[Email],[Telefone1],[Telefone2],[Conta],[AgenciaID],[TipoID] " +
                "FROM [uCRM].[dbo].[Cliente] WHERE (Telefone1='" + contacto + "' OR telefone2='" + contacto +
                "' OR Email='" + contacto + "')";

            string connectionString = ConfigurationManager.ConnectionStrings
                [db].ConnectionString;
            SqlConnection DBConnection = new SqlConnection(connectionString);
            using (DBConnection)
            {
                SqlCommand command = new SqlCommand(sqlstr, DBConnection);

                try
                {
                    DBConnection.Open();
                    SqlDataReader reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        reader.Read();
                        cliente_id = reader.GetInt32(0);
                        if (!reader.IsDBNull(1)) nome = reader.GetString(1);
                        if (!reader.IsDBNull(2)) email = reader.GetString(2);
                        if (!reader.IsDBNull(3)) telefone1 = reader.GetString(3);
                        if (!reader.IsDBNull(4)) telefone2 = reader.GetString(4);
                        if (!reader.IsDBNull(5)) numero_conta = reader.GetString(5);
                        if (!reader.IsDBNull(6)) agencia_id = reader.GetInt16(6);
                        if (!reader.IsDBNull(7)) tipo_id = reader.GetInt16(7);
                    }
                    else
                    {
                        //message_status.Text = "ERRO : Cliente não existe";
                        return false;
                    }
                    reader.Close();
                    return true;
                }
                catch (Exception e)
                {
                    //message_status.Text = "ERRO : " + e.Message;
                    return false;
                }
            }
        }

        public bool SelectCliente_byID(int id, string db)
        {
            string sqlstr = "SELECT [Id],[Nome],[Email],[Telefone1],[Telefone2],[Conta],[AgenciaID],[TipoID] FROM [uCRM].[dbo].[Cliente] WHERE id=" + id.ToString();
            string connectionString = ConfigurationManager.ConnectionStrings
                [db].ConnectionString;
            SqlConnection DBConnection = new SqlConnection(connectionString);
            using (DBConnection)
            {
                SqlCommand command = new SqlCommand(sqlstr, DBConnection);

                try
                {
                    DBConnection.Open();
                    SqlDataReader reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        reader.Read();
                        cliente_id = reader.GetInt32(0);
                        if (!reader.IsDBNull(1)) nome = reader.GetString(1);
                        else nome = string.Empty;
                        if (!reader.IsDBNull(2)) email = reader.GetString(2);
                        else email = string.Empty;
                        if (!reader.IsDBNull(3)) telefone1 = reader.GetString(3);
                        else telefone1 = string.Empty;
                        if (!reader.IsDBNull(4)) telefone2 = reader.GetString(4);
                        else telefone2 = string.Empty;
                        if (!reader.IsDBNull(5)) numero_conta = reader.GetString(5);
                        else numero_conta = string.Empty;
                        if (!reader.IsDBNull(6)) agencia_id = reader.GetInt32(6);
                        else agencia_id = 0;
                        if (!reader.IsDBNull(7)) tipo_id = reader.GetInt32(7);
                        else tipo_id = 0;
                    }
                    else
                    {
                        //message_status.Text = "ERRO : Cliente não existe";
                        clear_cliente();
                        return false;
                    }
                    reader.Close();
                    return true;
                }
                catch (Exception e)
                {
                    //message_status.Text = "ERRO : " + e.Message;
                    clear_cliente();
                    return false;
                }
            }
        }

        public bool update_cliente(string db)
        {
            string sqlstr = "UPDATE [dbo].[Cliente] " + "SET [Nome] = '" + nome + "',[Email] ='" + email + "',[Telefone1] ='" + telefone1 +
                    "',[Telefone2] ='" + telefone2 + "',[Conta] = '" + numero_conta + "',[AgenciaID] = " + agencia_id.ToString() +
                    ",[TipoID] = " + tipo_id + " WHERE ID=" + cliente_id;

            string connectionString = ConfigurationManager.ConnectionStrings[db].ConnectionString;
            SqlConnection DBConnection = new SqlConnection(connectionString);
            using (DBConnection)
            {
                SqlCommand command = new SqlCommand(sqlstr, DBConnection);
                try
                {
                    DBConnection.Open();
                    int affected_rows = command.ExecuteNonQuery();
                    if (affected_rows == 1) return true;
                        else return false;
                } catch(Exception e)
                {
                    return false;
                }
            }

        }
        public int insert_cliente(string db)
        {
            string sqlstr = "[dbo].[SP_Inserir_Cliente]";
                
                /*"EXECUTE [dbo].[SP_Inserir_Cliente] '" + nome + "','" + email + "','" + telefone1 + 
                    "','" + telefone2 + "','" + numero_conta + "'," + agencia_id.ToString() + "," + tipo_id;*/

            string connectionString = ConfigurationManager.ConnectionStrings[db].ConnectionString;
            SqlConnection DBConnection = new SqlConnection(connectionString);
            using (DBConnection)
            {
                SqlCommand command = new SqlCommand(sqlstr, DBConnection);
                try
                {
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter param_id = command.Parameters.Add("@id", SqlDbType.Int);
                    param_id.Direction = ParameterDirection.ReturnValue;
                    command.Parameters.Add(new SqlParameter("@nome", nome));
                    command.Parameters.Add(new SqlParameter("@email", email));
                    command.Parameters.Add(new SqlParameter("@telefone1", telefone1));
                    command.Parameters.Add(new SqlParameter("@telefone2", telefone2));
                    command.Parameters.Add(new SqlParameter("@conta", numero_conta));
                    command.Parameters.Add(new SqlParameter("@agenciaid", agencia_id));
                    command.Parameters.Add(new SqlParameter("@tipoid", tipo_id));
                    
                    DBConnection.Open();
                    
                    command.ExecuteNonQuery();                    
                    return int.Parse(param_id.Value.ToString());                   
                }
                catch (Exception e)
                {
                    return -1;
                }
            }

        }

        public DataTable select_clients()
        {
            string db = "uCRM_DB";
            
            string where="";
            if (nome.Length > 0) where = where + "nome LIKE '%" + nome + "%' OR ";
            if (email.Length > 0) where = where + "email LIKE '%" + email + "%' OR ";
            if (telefone1.Length > 0) where = where + "telefone1 LIKE '%" + telefone1 + "%' OR ";
            if (telefone2.Length > 0) where = where + "telefone2 LIKE '%" + telefone2 + "%' OR ";
            if (numero_conta.Length > 0) where = where + "conta LIKE '%" + numero_conta + "%' OR ";

            string sqlstr;

            if (where.Length>3) sqlstr = "SELECT TOP 20 id, nome, email, telefone1, telefone2, conta FROM cliente WHERE " + where.Substring(0,where.Length-3);
            else sqlstr = "SELECT TOP 20 id, nome, email, telefone1, telefone2, conta FROM cliente";
         
            string connectionString = ConfigurationManager.ConnectionStrings
                [db].ConnectionString;
            SqlConnection DBConnection = new SqlConnection(connectionString);

            using (DBConnection)
            {
                SqlCommand command = new SqlCommand(sqlstr, DBConnection);

                try
                {
                    DBConnection.Open();
                    SqlDataAdapter sda = new SqlDataAdapter(command);                                       
                    DataTable dt = new DataTable();
                    sda.Fill(dt);                    
                    return dt;
                }
                catch (Exception e)
                {
                    return null;
                }
            }
        }

        private void clear_cliente()
        {
            cliente_id = 0;
            nome = string.Empty;
            email = string.Empty;
            telefone1 = string.Empty;
            telefone2 = string.Empty;
            numero_conta = string.Empty;
            agencia_id = 0;
            tipo_id = 0;
        }

    }


}
