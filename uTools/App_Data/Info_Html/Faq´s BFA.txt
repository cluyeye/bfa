<html>
<head>
<meta charset="UTF-8">


<script language="JavaScript" type="text/javascript">
<!-- Copyright 2005, Sandeep Gangadharan -->
<!-- For more free scripts go to http://www.sivamdesign.com/scripts/ -->
<!--
if (document.getElementById) {
document.writeln('<style type="text/css"><!--')
document.writeln('.texter {display:none} @media print {.texter {display:block;}}')
document.writeln('//--></style>') }

function openClose(theID) {
if (document.getElementById(theID).style.display == "block") { document.getElementById(theID).style.display = "none" }
else { document.getElementById(theID).style.display = "block" } }
// -->
</script>
</head>



<body>
<h1>Perguntas frequentes do Cart�o multicaixa Empresa</h1>
<div onClick="openClose('a1')" style="cursor:hand; cursor:pointer"><b>1. O que � o cart�o de D�bito Multicaixa Empresa?</b></div>
<div id="a1" class="texter">
� um cart�o de d�bito personalizado ou n�o personalizado AKZ, utiliz�vel em caixas autom�ticas e TPA/POS na rede Multicaixa para levantamentos, consultas, transfer�ncias e pagamentos, destinado ao segmento Empresas.
</div>

<div onClick="openClose('a2')" style="cursor:hand; cursor:pointer"><b>2. Qual a diferen�a entre o Cart�o Multicaixa e o Cart�o Multicaixa Empresa?</b></div>
<div id="a2" class="texter">
A diferen�a est� exclusivamente no processo de ades�o. No caso do Cart�o Multicaixa Empresa o impresso de ades�o dever� ser assinado pelos representantes legais da empresa enquanto no cart�o Multicaixa deve ser assinado somente pelo titular que solicita o cart�o. As funcionalidades dos dois cart�es s�o exactamente as mesmas. 
</div>

<div onClick="openClose('a3')" style="cursor:hand; cursor:pointer"><b>3.Posso solicitar o cart�o em qualquer balc�o BFA?</b></div>
<div id="a3" class="texter">
Sim. Caso j� tenha conta no BFA, pode solicitar em qualquer balc�o BFA.
</div>

<div onClick="openClose('a4')" style="cursor:hand; cursor:pointer"><b>4. Qual o tempo de entrega do cart�o ao Cliente?</b></div>
<div id="a4" class="texter">
O cart�o n�o personalizado � de entrega imediata e o Cart�o Multicaixa Empresa personalizado tem um prazo m�dio de entrega de 15 dias.
</div>

<div onClick="openClose('a4')" style="cursor:hand; cursor:pointer"><b>4. Qual o tempo de entrega do cart�o ao Cliente?</b></div>
<div id="a4" class="texter">
O cart�o n�o personalizado � de entrega imediata e o Cart�o Multicaixa Empresa personalizado tem um prazo m�dio de entrega de 15 dias.
</div>

<div onClick="openClose('a5')" style="cursor:hand; cursor:pointer"><b>5.Pode um Cliente ter mais de um Cart�o Multicaixa Empresa?</b></div>
<div id="a5" class="texter">
Sim. Pode ter um cart�o multicaixa empresa, por cada conta � ordem/natureza.
</div>

<div onClick="openClose('a6')" style="cursor:hand; cursor:pointer"><b>6.Pode empresa com conta conjunta, solicitar o cart�o Multicaixa empresa?</b></div>
<div id="a6" class="texter">
N�o. Por se tratar de tipo de conta que requer mais de uma assinatura, n�o � permitida a emiss�o de um Cart�o Multicaixa Empresa.
</div>

<div onClick="openClose('a7')" style="cursor:hand; cursor:pointer"><b>7.O cart�o Multicaixa Empresa tem algum custo?</b></div>
<div id="a7" class="texter">
N�o. A ades�o e anuidade s�o gratuitas.
</div>

<div onClick="openClose('a8')" style="cursor:hand; cursor:pointer"><b>8.Posso alterar o c�digo pin do cart�o?</b></div>
<div id="a8" class="texter">
O c�digo pin do cart�o multicaixa pode ser alterado nas Caixas Autom�ticas (ATM).
</div>

<h1>Perguntas sobre altera��es de Divisas</h1>
<div onClick="openClose('b1')" style="cursor:hand; cursor:pointer"><b>1. Quest�es sobre inibi��o de pagamentos antecipados e Refor�o do Cart�o de Cr�dito</b></div>
<div id="b1" class="texter">
Neste momento n�o � poss�vel efectuar pagamentos antecipados ou de refor�o do Cart�o de Cr�dito. 
Estas medidas devem-se ao actual quadro cambial apresentado pelo mercado, nomeadamente devido � redu��o significativa do pre�o do barril de petr�leo que est� a conduzir a um cen�rio de redu��o significativa da disponibilidade de divisas recebidas no pais e, em consequ�ncia, das divisas que o BNA disponibiliza aos bancos comerciais. 
Estas medidas s�o tempor�rias e a situa��o dever� ser reposta logo que as condi��es do mercado cambial o permitam.

</div>


<div onClick="openClose('b2')" style="cursor:hand; cursor:pointer"><b>2. Quest�es sobre a redu��o do limite mensal de carregamento do cart�o pr�-pago Kandandu</b></div>
<div id="b2" class="texter">
O montante m�ximo de carregamento do cart�o Kandandu foi alterado passando para 400.000 Akz/m�s.
Estas medidas devem-se ao actual quadro cambial apresentado pelo mercado, nomeadamente devido � redu��o significativa do pre�o do barril de petr�leo que est� a conduzir a um cen�rio de redu��o significativa da disponibilidade de divisas recebidas no pais e, em consequ�ncia, das divisas que o BNA disponibiliza aos bancos comerciais. 
Estas medidas s�o tempor�rias e a situa��o dever� ser reposta logo que as condi��es do mercado cambial o permitam.

</div>


<div onClick="openClose('b3')" style="cursor:hand; cursor:pointer"><b>3. Quest�es sobre a inibi��o do carregamento do cart�o pr�-pago Kandandu nos ATM�s e no BFA Net</b></div>
<div id="b3" class="texter">
Por quest�es t�cnicas os carregamentos nos ATM�s e no BFA Net est�o temporariamente indispon�veis. Para proceder ao carregamento do cart�o dever� dirigir-se a qualquer balc�o BFA.<br>
Estas medidas devem-se ao actual quadro cambial apresentado pelo mercado, nomeadamente devido � redu��o significativa do pre�o do barril de petr�leo que est� a conduzir a um cen�rio de redu��o significativa da disponibilidade de divisas recebidas no pais e, em consequ�ncia, das divisas que o BNA disponibiliza aos bancos comerciais. 
Estas medidas s�o tempor�rias e a situa��o dever� ser reposta logo que as condi��es do mercado cambial o permitam.
</div>


<div onClick="openClose('b4')" style="cursor:hand; cursor:pointer"><b>4. Quest�es relacionadas com os atrasos nas transfer�ncias para o estrangeiro � OPE�s </b></div>
<div id="a8" class="texter">
As transfer�ncias para o estrangeiro encontram-se condicionadas pela reduzida disponibilidade de divisas no mercado cambial o que poder� resultar num atraso na realiza��o das opera��es.<br>
Estas medidas devem-se ao actual quadro cambial apresentado pelo mercado, nomeadamente devido � redu��o significativa do pre�o do barril de petr�leo que est� a conduzir a um cen�rio de redu��o significativa da disponibilidade de divisas recebidas no pa�s e, em consequencia, das divisas que o BNA disponibiliza aos bancos comerciais.Estas medidas s�o tempor�rias e a situa��o dever� ser reposta logo que as condi��es do mercado cambial o permitam.
</div>


<div onClick="openClose('b5')" style="cursor:hand; cursor:pointer"><b>5. Quest�es relacionadas com Cr�ditos Document�rios � Importa��o (CDI�s) </b></div>
<div id="b5" class="texter">
A emiss�o pontual de cr�ditos document�rios � importa��o (CDI) foi restringida, sendo apenas poss�vel na situa��es em que:<br>
i. A opera��o estiver coberta por fundos pr�prios na respectiva moeda de pagamento; ou<br>
ii. Se associada a um contrato de Forward Cambial. <br>
Estas medidas devem-se ao actual quadro cambial apresentado pelo mercado, nomeadamente devido � redu��o significativa do pre�o do barril de petr�leo que est� a conduzir a um cen�rio de redu��o significativa da disponibilidade de divisas recebidas no pais e, em consequ�ncia, das divisas que o BNA disponibiliza aos bancos comerciais. <br>
Estas medidas s�o tempor�rias e a situa��o dever� ser reposta logo que as condi��es do mercado cambial o permitam.

</div>


<div onClick="openClose('b6')" style="cursor:hand; cursor:pointer"><b>6. Quest�es relacionadas com Western Union</b></div>
<div id="b6" class="texter">
As opera��es Western Union de Angola para o estrangeiro est�o suspensas. As opera��es de recebimento n�o sofrem altera��o permanecendo dispon�veis.<br>
Estas medidas devem-se ao actual quadro cambial apresentado pelo mercado, nomeadamente devido � redu��o significativa do pre�o do barril de petr�leo que est� a conduzir a um cen�rio de redu��o significativa da disponibilidade de divisas recebidas no pais e, em consequ�ncia, das divisas que o BNA disponibiliza aos bancos comerciais. <br>
Estas medidas s�o tempor�rias e a situa��o dever� ser reposta logo que as condi��es do mercado cambial o permitam.<br>
</div>


</body>
</html>