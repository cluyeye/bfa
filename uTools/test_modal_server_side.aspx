﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="test_modal_server_side.aspx.cs" Inherits="uTools.test_modal_server_side" %>

<!DOCTYPE html>
<style>
    .fundo_modal {
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable auto if needed */
        background-color: rgba(0, 0, 0, .4); /* Black w/ opacity */
    }

    .clickto_modal {
        /*display: none;*/ /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    }

    .clickto_modal-content {
        /*position: relative;*/
        margin: auto;
        color: white;
        padding: 0;
        border: 1px solid #888;
        width: 500px;
        height: 350px;
        border-radius: 10px;
        box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
        -webkit-animation-name: animatetop;
        -webkit-animation-duration: 0.4s;
        animation-name: animatetop;
        animation-duration: 0.4s;
    }

    #content_modal {
        margin: 0;
        padding: 0;
        width: 100%;
        height: 100%;
        /*border: 2px solid green;*/
        margin: auto;
        background-image: url('../images/back_modal.png');
        background-repeat: no-repeat;
        border-radius: 10px;
    }

    #content_top {
        padding: 5px;
        padding-bottom: 0px;
        border: 2px solid yellow;
        height: 30px;
    }

    #content_center {
        width: 80%;
        height: 240px;
        border: 2px solid red;
        position: relative;
        top: 20px;
    }

    #content_button {
        position: relative;
        top: 20px;
        border: 2px solid green;
        padding-left: 40px;
    }
</style>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>


    <link rel="stylesheet" type="text/css" href="css/geral.css" />
    <%--<link rel="stylesheet" type="text/css" href="css/style_modal.css" />--%>
</head>


<body>
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
            <%--<asp:Button ID="btn_accao_ok" runat="server" Text="Client Call Modal" OnClick="btn_accao_ok_Click"/>
        
        <asp:Panel ID="pnl_modal" runat="server" CssClass="clickto_modal" Visible="false" >
            <asp:Panel ID="pnl_modal_content" runat="server" CssClass="clickto_modal-content" >
            TESTE   asp:Panel         
            <asp:Button ID="btn_accao_cancel" runat="server" Text="Cancel" OnClick="btn_accao_cancel_Click" />
            </asp:Panel>
        </asp:Panel>--%>


            <asp:Button ID="btn_accao_ok" runat="server" Text="client Call Modal" />

            <asp:Button ID="Button1" runat="server" Text="server Call Modal" OnClick="btn_accao_ok_Click" />

            <ajaxToolkit:ModalPopupExtender ID="modal_accao" runat="server"
                TargetControlID="btn_accao_ok" PopupControlID="pnl_accao_modal" CancelControlID="btn_accao_cancel"
                BackgroundCssClass="fundo_modal" DropShadow="true">
            </ajaxToolkit:ModalPopupExtender>

            <asp:Panel ID="pnl_accao_modal" runat="server" CssClass="clickto_modal-content">
                <div id="content_modal" align="center">
                    <div id="content_top" align="right">
                        <label style="font-size: 18px; text-shadow: 10px 10px 5px #000000;">teste</label>
                    </div>

                    <div id="content_center">
                        <label></label>
                    </div>

                    <div id="content_button" align="justify">
                        <asp:ImageButton ID="btn_accao_cancel" runat="server" CssClass="img_bt" ImageUrl="~/images/ok.png" title="Ok" OnClick="btn_accao_cancel_Click" />
                    </div>
                </div>

            </asp:Panel>

        </div>
    </form>
</body>
</html>
