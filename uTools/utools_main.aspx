﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General.Master" AutoEventWireup="true" CodeBehind="utools_main.aspx.cs" Inherits="uTools.utools_main" %>


<%@ Register Src="Controls/uTasksView.ascx" TagName="uTasksView" TagPrefix="utv" %>
<%@ Register Src="Controls/uKnowledgeView.ascx" TagName="uKnowledgeView" TagPrefix="ukv" %>
<%@ Register Src="Controls/uCRMView.ascx" TagName="uCRMView" TagPrefix="ucrm" %>
<%@ Register Src="~/Controls/Servicos.ascx" TagName="Servicos" TagPrefix="svc" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <%--<asp:Panel ID="pnl_accao_modal" runat="server" CssClass="clickto_modal-content">
        <div id="content_modal" align="center">
            <div id="content_top" align="right">
                <label id="header_modal" runat="server" style="font-size: 18px; text-shadow: 10px 10px 5px #000000;">
                    teste</label>
            </div>
            <div id="content_center">
                <label id="body_modal" runat="server">
                </label>
            </div>
            <div id="content_button" align="justify">
                <asp:ImageButton ID="btn_accao_cancel" runat="server" CssClass="img_bt" ImageUrl="~/images/ok.png" title="Ok" OnClick="btn_accao_cancel_Click" />
            </div>
        </div>
    </asp:Panel>--%>

    <div class="container_default">
        <%--<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" EnablePageMethods="true" />--%>
        <header>

            <img src="images/teste_04_1200px.png" title="BFA UCALL" style="width: 100%;" />

            <div id="container_manage" align="justify">
                <span id="agent_info" class="i_manage" runat="server">
                    <asp:Label runat="server" ID="lbl_info_agent"></asp:Label>
                </span><span id="op_manage" class="i_manage" style="background-color: #8c2034;" runat="server">

                    <div class="dropdown">
                        <span class="dropbtn">&#9776;</span>
                        <div class="dropdown-content" align="justify">
                            <%--<asp:Button ID="btn_accao_ok" class="btn_logout" runat="server" Text="client Call Modal" />--%>
                            <asp:Button ID="btn_pl_admin" class="btn_logout" runat="server" OnClick="btn_pl_admin_Click" Text="Painel Administrativo" />
                            <%--<a href="#" onclick="">Painel Administrativo</a>--%>
                            <a href="UserChangePwd.aspx">Mudar Palavra-Passe</a>
                            <hr />
                            <asp:Button ID="btn_logout" class="btn_logout" runat="server" OnClick="btn_logout_Click" Text="Terminar Sessão" />
                        </div>
                    </div>
                </span><span id="job_role_info" class="i_manage" style="background-color: #58595b;" runat="server">
                    <asp:Label runat="server" ID="lbl_job_role_agent"></asp:Label>
                </span>
            </div>

        </header>

        <article id="content_main">

            <div class="tabbed">
                <input name="tabbed" id="tabbed1" type="radio" checked>

                <section>
                    <h1>
                        <label for="tabbed1" class="txt_Titulo">uKNOWLEDGEBASE</label>
                    </h1>
                    <div>
                        <ukv:uKnowledgeView ID="uKnowledgeView1" runat="server"></ukv:uKnowledgeView>

                    </div>
                </section>

                <input name="tabbed" id="tabbed2" type="radio">

                <section>
                    <h1>
                        <label for="tabbed2" class="txt_Titulo">uTASKS</label>
                    </h1>
                    <div style="background-color: #e6e7e8;">
                        <utv:uTasksView ID="uTasksView1" runat="server"></utv:uTasksView>

                    </div>
                </section>

                <input name="tabbed" id="tabbed3" type="radio">

                <section>
                    <h1>
                        <label for="tabbed3" class="txt_Titulo">SERVIÇOS GERAIS</label>
                    </h1>
                    <div>
                        <svc:Servicos ID="Suporte" runat="server"></svc:Servicos>

                    </div>
                </section>

            </div>

        </article>
        <nav id="content_crm">

            <ucrm:uCRMView ID="uCRMView1" runat="server" />

        </nav>
        <footer id="rodape">
            <img src="images/rodape_02.png" title="Rodapé" style="width: 100%; height: 28px;" />
        </footer>


        <script language="javascript" type="text/javascript">
            //<![CDATA[</span />

            var clicked = false;
            function CheckBrowser() {
                if (clicked == false) {
                    //Browser closed
                    PageMethods.Logout();
                }
                else {
                    //redirected 
                    clicked = false;
                }
            }

            function bodyUnload() {

                if (clicked == false)//browser is closed
                {
                    var request = GetRequest();

                    request.open("POST", "../LogOut.aspx", false);
                    request.send();
                    alert('This is close');
                }
            }
            function GetRequest() {
                var xmlhttp;
                if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }

                alert('This is close. Request');

                return xmlhttp;
            }
        </script>
    </div>
</asp:Content>


