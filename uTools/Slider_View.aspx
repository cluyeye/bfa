﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Slider_View.aspx.cs" Inherits="uTools.Controls.Slider_View" %>

<!DOCTYPE html>

<html>
<head>

    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <title>Amazing Slider</title>

    <!-- Insert to your webpage before the </head> -->
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Slider, Hi Slider, Simple HTML5 Slideshow, Free js slider" />
    <meta name="description" content="Slider created with Hi Slider which enable you to publish responsive jQuery image slideshows, seamless WordPress slider plugins, amazing website sliding banners and fancy JavaScript slideshow presentation" />
    <script type="text/javascript" src="sliderengine/jquery.js"></script>
    <script type="text/javascript" src="sliderengine/jquery.hislider.js"></script>

    <!-- End of head section HTML codes -->

    <style type="text/css">
        body, html {
            margin: 0;
            padding: 0;
        }
    </style>

</head>
<body>

    <div id='hislider1' style="max-width: 100%; max-height: 600px; margin: 0 auto; border: none;">
        <ul style="display: none; overflow: hidden; height: 0; visibility: hidden; opacity: 0;">
            <li>
                <div>
                    <img data-src="images/BFA.png" data-thumb-src="images/BFA-th.png" title="" alt="" data-content-type="image" data-content="" data-interval="-1" />
                    <div data-type="effect" data-effect-type="Random"></div>
                </div>
            </li>
            <li>
                <div>
                    <img data-src="images/Imagem1.png" data-thumb-src="images/Imagem1-th.png" title="" alt="" data-content-type="image" data-content="" data-interval="-1" />
                    <div data-type="effect" data-effect-type="Random"></div>
                </div>
            </li>
            <li>
                <div>
                    <img data-src="images/image_344_0_1403722127.jpeg" data-thumb-src="images/image_344_0_1403722127-th.jpeg" title="" alt="" data-content-type="image" data-content="" data-interval="-1" />
                    <div data-type="effect" data-effect-type="Random"></div>
                </div>
            </li>
            <li>
                <div>
                    <img data-src="images/maxresdefault.jpg" data-thumb-src="images/maxresdefault-th.jpg" title="" alt="" data-content-type="image" data-content="" data-interval="-1" />
                    <div data-type="effect" data-effect-type="Random"></div>
                </div>
            </li>
            <li>
                <div>
                    <img data-src="images/BFA20anos.png" data-thumb-src="images/BFA20anos-th.png" title="" alt="" data-content-type="image" data-content="" data-interval="-1" />
                    <div data-type="effect" data-effect-type="Random"></div>
                </div>
            </li>
            <li>
                <div>
                    <img data-src="images/{929ba0e1-e277-43b8-956e-b004fa4b8cc5}.png" data-thumb-src="images/{929ba0e1-e277-43b8-956e-b004fa4b8cc5}-th.png" title="" alt="" data-content-type="image" data-content="" data-interval="-1" />
                    <div data-type="effect" data-effect-type="Random"></div>
                </div>
            </li>
            <li>
                <div>
                    <img data-src="images/{6c69290c-dff6-43c3-ba9c-cc3516d9be80}.png" data-thumb-src="images/{6c69290c-dff6-43c3-ba9c-cc3516d9be80}-th.png" title="" alt="" data-content-type="image" data-content="" data-interval="-1" />
                    <div data-type="effect" data-effect-type="Random"></div>
                </div>
            </li>
            <li>
                <div>
                    <img data-src="images/{26dcd836-a32e-416d-b4a2-11c4b584524c}.PNG" data-thumb-src="images/{26dcd836-a32e-416d-b4a2-11c4b584524c}-th.PNG" title="" alt="" data-content-type="image" data-content="" data-interval="-1" />
                    <div data-type="effect" data-effect-type="Random"></div>
                </div>
            </li>
            <li>
                <div>
                    <img data-src="images/{69da40cb-739f-4c05-9321-f82db24bd3f8} (1).png" data-thumb-src="images/{69da40cb-739f-4c05-9321-f82db24bd3f8} (1)-th.png" title="" alt="" data-content-type="image" data-content="" data-interval="-1" />
                    <div data-type="effect" data-effect-type="Random"></div>
                </div>
            </li>
            <li>
                <div>
                    <img data-src="images/{dc52431a-6969-49b3-8a3a-c992104926ef}.png" data-thumb-src="images/{dc52431a-6969-49b3-8a3a-c992104926ef}-th.png" title="" alt="" data-content-type="image" data-content="" data-interval="-1" />
                    <div data-type="effect" data-effect-type="Random"></div>
                </div>
            </li>
            <li>
                <div>
                    <img data-src="images/2a9dc030-6d58-40be-8989-c7affc12b0b4.JPG" data-thumb-src="images/2a9dc030-6d58-40be-8989-c7affc12b0b4-th.JPG" title="" alt="" data-content-type="image" data-content="" data-interval="-1" />
                    <div data-type="effect" data-effect-type="Random"></div>
                </div>
            </li>
            <li>
                <div>
                    <img data-src="images/newsBFA-1-1280x850.jpg" data-thumb-src="images/newsBFA-1-1280x850-th.jpg" title="" alt="" data-content-type="image" data-content="" data-interval="-1" />
                    <div data-type="effect" data-effect-type="Random"></div>
                </div>
            </li>
            <li>
                <div>
                    <img data-src="images/6c68d2b5-7bfe-4df2-9568-b2044ebaecb4-large.png" data-thumb-src="images/6c68d2b5-7bfe-4df2-9568-b2044ebaecb4-large-th.png" title="" alt="" data-content-type="image" data-content="" data-interval="-1" />
                    <div data-type="effect" data-effect-type="Random"></div>
                </div>
            </li>
            <li>
                <div>
                    <img data-src="images/9c28d490-0a87-4fdb-9e12-8f049e7fb6a6-large.png" data-thumb-src="images/9c28d490-0a87-4fdb-9e12-8f049e7fb6a6-large-th.png" title="" alt="" data-content-type="image" data-content="" data-interval="-1" />
                    <div data-type="effect" data-effect-type="Random"></div>
                </div>
            </li>
            <li>
                <div>
                    <img data-src="images/large-1086876-2.jpg" data-thumb-src="images/large-1086876-2-th.jpg" title="" alt="" data-content-type="image" data-content="" data-interval="-1" />
                    <div data-type="effect" data-effect-type="Random"></div>
                </div>
            </li>
        </ul>

        <noscript>The slider is powered by <strong>Hi Slider</strong>, a easy jQuery image slider from <a target="_blank" href="hislider.com"></a>. Please enable JavaScript to view.</noscript>
        <div class="hi-about-text" style="display: none">This jQuery slider was created with the free <a style="color: #FFF;" href="http://hislider.com" target="_blank">Hi Slider</a> for WordPress plugin,Joomla & Drupal Module from hislider.com.<br />
            <br />
            <a style="color: #FFF;" href="#" class="hi-about-ok">OK</a></div>
        <div class="hi-slider-watermark" style="display: none"><a href="http://hislider.com" target="_blank"></a></div>
    </div>

</body>
</html>
