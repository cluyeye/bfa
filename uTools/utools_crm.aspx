﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="utools_crm.aspx.cs" Inherits="uTools.utools_crm" %>

<%@ Register Src="~/Controls/uCRMView.ascx" TagName="uCRMView" TagPrefix="ucrm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" >
<head runat="server">

    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>

    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta http-equiv="content-language" content="pt-br" />

    <meta name="description" content="Ferramenta de Ticketing, que vai melhorar a
        Experiência de Cliente dando resposta às situações colocadas nos vários canais
        de interacção da sua organização (agências, pontos de venda, contact center, etc).
        A partir de qualquer área da sua empresa, a resposta é dada dentro do prazo expectável,
        aumentando a Qualidade na relação com o Cliente. Os pedidos de informação, as reclamações
        ou sugestões são direccionadas a quem decide e resolvem-se com maior rapidez, eficiência
        e controlo da interacção" />

    <meta name="locale" content="AO"/>
    <meta name="language" content="pt"/>
    <meta name="country" content="Angola"/>

    <link rel="shortcut icon" href="images/image_icon.png" />


    <title>uCRM</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <style>
        @charset 'utf-8';


        @font-face {
            font-family: ephismere;
            src: url('../fontes/Styling W00 Regular.ttf');
        }


        body
        {
            background-color: #e6e7e8;
            font-family: 'ephismere';
            font-size: 12px;
            color: #58595b;
            max-height: none;
        }


        #container_crm {
            max-height: none;
        }

        #container_interaccoes {
            min-height: 150px;
            max-height: 350px;
            overflow-y: auto;
        }

            #container_interaccoes gridview_interaccoes {
                width: 100px;
                max-height: none;
            }

        #container_crm .txtInput {
            border: 1px solid #e6e7e8;
            width: 250px;
            border-radius: 4px;
            height: 25px;
        }


        #container_crm .selectInput {
            border: 1px solid #e6e7e8;
            width: 250px;
            border-radius: 4px;
            height: 25px;
        }

        #container_button_edit {
            float: left;
        }

        .lb_crm {
            font-size: 14px;
        }


        .img_bt {
            width: 40px;
            padding: 0;
            background-color: transparent;
        }
    </style>

    <link rel="stylesheet" type="text/css" href="css/style_upd_progress.css">

</head>

<body>
    <div class="container">
        <form id="form1" runat="server">
            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
            <nav id="content_crm">

                <ucrm:uCRMView ID="uCRMView1" runat="server" />

            </nav>
        </form>
    </div>
    
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <div id="blur" />
            <div id="progress">
                <img src="images/loading-small.gif" width="50px" height="50px" />
            </div>
        </ProgressTemplate>


    </asp:UpdateProgress></body>
</html>
