﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using System.Web.Services;

namespace uTools
{
    public partial class General : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void Logout()
        {
            var authenticationManager = HttpContext.Current.GetOwinContext().Authentication;

            //Load_user_info(id, name_user);
            Tools.change_session_value_OUT(Context.User.Identity.GetUserId(), 0);

            authenticationManager.SignOut();

            Response.ClearContent();
            Response.Redirect("~/Login.aspx?urlredirect=utools_main.aspx");

        }

        protected void btn_accao_cancel_Click(object sender, EventArgs e)
        {
            //pnl_modal.Visible = false;
        }


    }
}