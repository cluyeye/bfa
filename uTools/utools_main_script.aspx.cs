﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;



namespace uTools
{
    public partial class utools_main_script : System.Web.UI.Page
    {
        public String AgenteID
        {
            get
            {
                object o = ViewState["AgenteID"];
                return (o == null) ? String.Empty : (string)o;
            }

            set
            {
                ViewState["AgenteID"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                AgenteID = Request.QueryString["user_id"];
                if (!Login()) Response.Redirect("error_page.html");
            }
        }

        private bool Login()
        {
            if (AgenteID != "")
            {
                string username = string.Empty;

                string db = "uTasks_DB";

                string sqlstr = "SELECT UserName FROM [uAuth].[dbo].[AspNetUsers] where UserName='" + AgenteID + "'";

                string connectionString = ConfigurationManager.ConnectionStrings[db].ConnectionString;
                SqlConnection DBConnection = new SqlConnection(connectionString);
                using (DBConnection)
                {
                    SqlCommand command = new SqlCommand(sqlstr, DBConnection);
                    try
                    {
                        DBConnection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        if (reader.HasRows)
                        {
                            reader.Read();
                            username = reader["UserName"].ToString();
                        }
                        else
                        {
                            reader.Close();
                            return false;
                        }
                        reader.Close();
                    }
                    catch (Exception erro)
                    {
                        return false;
                    }
                }

                var userStore = new UserStore<IdentityUser>();
                var userManager = new UserManager<IdentityUser>(userStore);
                var user = userManager.Find(username, "utools_agent");

                if (user != null)
                {
                    var authenticationManager = HttpContext.Current.GetOwinContext().Authentication;
                    var userIdentity = userManager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);

                    authenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = false }, userIdentity);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;
        }


    }
}